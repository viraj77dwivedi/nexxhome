﻿simpalTek.lib = simpalTek || {};

simpalTek.lib.doorControlAnimation = function () {
    var upImages = new Array();
    var downImages = new Array();
    var baseImgPath = "img/garage-control-button";
    var unknownStateImage = "/Garage-state-unknown.png";
    var animationUpInterval = null;
    var animationDownInterval = null;
    var isAnimationRunning = false;
    var scope;

    //var $scope = angular.element(document.body).injector().get('$scope');

    var preloadDownImage = [
        "/down/GarageDown-Complete.png",
        "/down/GarageGoingDown-Frame1.png",
        "/down/GarageGoingDown-Frame2.png",
        "/down/GarageGoingDown-Frame3.png",
        "/down/GarageGoingDown-Frame4.png",
        "/down/GarageGoingDown-Frame5.png",
        "/down/GarageGoingDown-Frame6.png",
        "/down/GarageGoingDown-Frame7.png",
        "/down/GarageGoingDown-Frame8.png",
        "/down/GarageGoingDown-Frame9.png",
        "/down/GarageGoingDown-Frame10.png"
    ];

    var preloadUpImage = [
        "/up/GarageUp-Complete.png",
        "/up/GarageGoingUp-Frame1.png",
        "/up/GarageGoingUp-Frame2.png",
        "/up/GarageGoingUp-Frame3.png",
        "/up/GarageGoingUp-Frame4.png",
        "/up/GarageGoingUp-Frame5.png",
        "/up/GarageGoingUp-Frame6.png",
        "/up/GarageGoingUp-Frame7.png",
        "/up/GarageGoingUp-Frame8.png"
    ];

    function preload() {
        for (i = 0; i < preloadUpImage.length; i++) {
            upImages[i] = new Image();
            upImages[i].src = baseImgPath + preloadUpImage[i];
        }

        for (i = 0; i < preloadDownImage.length; i++) {
            downImages[i] = new Image();
            downImages[i].src = baseImgPath + preloadDownImage[i];
        }
    }

    preload();

    this.scope = scope;

    this.currentState = function (state) {

        if (state == "Offline") {
            this.scope.buttonImage = baseImgPath + unknownStateImage;
        }
        else if (!isAnimationRunning) {
            if (state == "Open")
                this.scope.buttonImage = upImages[0].src;
            else 
                this.scope.buttonImage = downImages[0].src;
        }
    }

    this.stopAnimation = function () {
        isAnimationRunning = false;
        clearInterval(animationDownInterval);
        clearInterval(animationUpInterval);
    }

    this.isRunning = function () { return isAnimationRunning };

    this.startUp = function (callBack) {
        //var $scope = angular.element(document.body).injector().get('$scope');
        var counter = 1;

        clearInterval(animationDownInterval);

        if (animationUpInterval != null)
            clearInterval(animationUpInterval);

        animationUpInterval = setInterval(function () {

            if (counter >= preloadUpImage.length) {
                clearInterval(animationUpInterval);
                animationUpInterval = null;

                counter = 1;

                //$scope.garageActivityStatus = "";

                isAnimationRunning = false;

                currentState("Open");

                //console.log("callBack",callBack)
                callBack();
            }
            else {
                this.scope.buttonImage = upImages[counter].src;
                isAnimationRunning = true;
                this.scope.$apply();
                //console.log("image up", upImages[counter].src)
            }

            counter++;

        }, 1000);
    }

    this.startDown = function (callBack) {
        //var $scope = angular.element(document.body).injector().get('$scope');
        var counter = 1;

        clearInterval(animationUpInterval);

        if (animationDownInterval != null)
            clearInterval(animationDownInterval);

        animationDownInterval = setInterval(function () {

            if (counter >= preloadDownImage.length) {
                clearInterval(animationDownInterval);
                animationDownInterval = null;

                counter = 1;

                //$scope.garageActivityStatus = "";


                isAnimationRunning = false;

                setTimeout(function () {
                    currentState("Close");
                }, 2000);

                callBack();
            }
            else {
                this.scope.buttonImage = downImages[counter].src;
                this.scope.$apply();
                isAnimationRunning = true;
            }

            counter++;

        }, 1000);

        //alert(0)
    }



    return this;
}();