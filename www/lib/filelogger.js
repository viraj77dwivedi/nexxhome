/*!
 * fileLogger
 * Copyright 2016 Peter Bakondy https://github.com/pbakondy
 * See LICENSE in this repository for license information
 */
(function(){
/* global angular, console, cordova */
/* eslint no-console:0 */

// install    : cordova plugin add cordova-plugin-file
// date format: https://docs.angularjs.org/api/ng/filter/date

angular.module('fileLogger', ['ngCordova.plugins.file'])

  .factory('$fileLogger', ['$q', '$window', '$cordovaFile', '$timeout', '$filter',
    function ($q, $window, $cordovaFile, $timeout, $filter) {

    'use strict';


    var queue = [];
    var ongoing = false;
    var levels = ['DEBUG', 'INFO', 'WARN', 'ERROR'];

    var storageFilename = 'messages.log';

    var dateFormat;
    var dateTimezone;

    // detecting Ripple Emulator
    // https://gist.github.com/triceam/4658021
    function isRipple() {
      return $window.parent && $window.parent.ripple;
    }

    function isBrowser() {
      return (!$window.cordova && !$window.PhoneGap && !$window.phonegap) || isRipple();
    }


    function log(level) {
      if (angular.isString(level)) {
        level = level.toUpperCase();

        if (levels.indexOf(level) === -1) {
          level = 'INFO';
        }
      } else {
        level = 'INFO';
      }

      var now = new Date();
      var timestamp = dateFormat ?
        $filter('date')(now, dateFormat, dateTimezone) : now.toJSON();

      var messages = Array.prototype.slice.call(arguments, 1);
      var message = [ timestamp, level ];
      var text;

      for (var i = 0; i < messages.length; i++ ) {
        if (angular.isArray(messages[i])) {
          text = '[Array]';
          try {
            // avoid "TypeError: Converting circular structure to JSON"
            text = JSON.stringify(messages[i]);
          } catch(e) {
            // do nothing
          }
          message.push(text);
        }
        else if (angular.isObject(messages[i])) {
          text = '[Object]';
          try {
            // avoid "TypeError: Converting circular structure to JSON"
            text = JSON.stringify(messages[i]);
          } catch(e) {
            // do nothing
          }
          message.push(text);
        }
        else {
          message.push(messages[i]);
        }
      }

      if (isBrowser()) {
        // log to browser console

        messages.unshift(timestamp);

        if (angular.isObject(console) && angular.isFunction(console.log)) {
          switch (level) {
            case 'DEBUG':
              if (angular.isFunction(console.debug)) {
                console.debug.apply(console, messages);
              } else {
                console.log.apply(console, messages);
              }
              break;
            case 'INFO':
              if (angular.isFunction(console.debug)) {
                console.info.apply(console, messages);
              } else {
                console.log.apply(console, messages);
              }
              break;
            case 'WARN':
              if (angular.isFunction(console.debug)) {
                console.warn.apply(console, messages);
              } else {
                console.log.apply(console, messages);
              }
              break;
            case 'ERROR':
              if (angular.isFunction(console.debug)) {
                console.error.apply(console, messages);
              } else {
                console.log.apply(console, messages);
              }
              break;
            default:
              console.log.apply(console, messages);
          }
        }

      } else {
        // log to logcat
        console.log(message.join(' '));
      }

      queue.push({ message: message.join(' ') + '\n' });

      if (!ongoing) {
        process();
      }
    }


    function process() {

      if (!queue.length) {
        ongoing = false;
        return;
      }

      ongoing = true;
      var m = queue.shift();

      writeLog(m.message).then(
        function() {
          $timeout(function() {
            process();
          });
        },
        function() {
          $timeout(function() {
            process();
          });
        }
      );

    }


    function writeLog(message) {
      var q = $q.defer();

      if (isBrowser()) {
        // running in browser with 'ionic serve'

        if (!$window.localStorage[storageFilename]) {
          $window.localStorage[storageFilename] = '';
        }

        $window.localStorage[storageFilename] += message;
        q.resolve();

      } else {

        if (!$window.cordova || !$window.cordova.file || !$window.cordova.file.dataDirectory) {
          q.reject('cordova.file.dataDirectory is not available');
          return q.promise;
        }

        //window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
        //    directoryEntry.getFile(storageFilename, { create: false },
        //        function () {
        //            console.log("file exist")
        //            directoryEntry.getFile(storageFilename, { create: false }, function (fileEntry) {
        //                //console.log("fileEntry", fileEntry);
        //                fileEntry.createWriter(function (fileWriter) {
        //                    fileWriter.onwriteend = function (e) {
        //                        // for real-world usage, you might consider passing a success callback
        //                        console.log('Write of file "' + storageFilename + '"" completed.', e);

        //                    };
        //                    fileWriter.onerror = function (e) {
        //                        // you could hook this up with our global error handler, or pass in an error callback
        //                        console.log('Write failed', e);
        //                    };

        //                    //fileWriter.seek(fileWriter.length);

        //                    var blob = new Blob([message], { type: 'text/plain' });

        //                    fileWriter.write(blob);

        //                    //console.log("blob", blob);

        //                }, errorHandler.bind(null, storageFilename));
        //            }, errorHandler.bind(null, storageFilename));
        //        },
        //        function () {
        //            console.log("file DOES NOT exist")
        //            directoryEntry.getFile(storageFilename, { create: true }, function (fileEntry) {
        //                //console.log("fileEntry", fileEntry);
        //                fileEntry.createWriter(function (fileWriter) {
        //                    fileWriter.onwriteend = function (e) {
        //                        // for real-world usage, you might consider passing a success callback
        //                        console.log('Write of file "' + storageFilename + '"" completed.', e);

        //                    };
        //                    fileWriter.onerror = function (e) {
        //                        // you could hook this up with our global error handler, or pass in an error callback
        //                        console.log('Write failed', e);
        //                    };

        //                    //fileWriter.seek(fileWriter.length);

        //                    var blob = new Blob([message], { type: 'text/plain' });

        //                    fileWriter.write(blob);

        //                    //console.log("blob", blob);

        //                }, errorHandler.bind(null, storageFilename));
        //            }, errorHandler.bind(null, storageFilename));
        //        }
        //    , errorHandler.bind(null, storageFilename));
        //}, errorHandler.bind(null, storageFilename));

        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
            //console.log("directoryEntry", directoryEntry);

            directoryEntry.getFile(storageFilename, { create: true }, function (fileEntry) {
                //console.log("fileEntry", fileEntry);
                fileEntry.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (e) {
                        // for real-world usage, you might consider passing a success callback
                        console.log('Write of file "' + storageFilename + '"" completed.', e);

                    };
                    fileWriter.onerror = function (e) {
                        // you could hook this up with our global error handler, or pass in an error callback
                        console.log('Write failed', e);
                    };

                    //fileWriter.seek(fileWriter.length);

                    var blob = new Blob([message], { type: 'text/plain' });

                    fileWriter.write(blob);

                    //console.log("blob", blob);

                }, errorHandler.bind(null, storageFilename));
            }, errorHandler.bind(null, storageFilename));
        }, errorHandler.bind(null, storageFilename));

        //$cordovaFile.checkFile(cordova.file.dataDirectory, storageFilename).then(
        //  function() {
        //    // writeExistingFile(path, fileName, text)
        //    $cordovaFile.writeExistingFile(cordova.file.dataDirectory, storageFilename, message).then(
        //      function() {
        //        q.resolve();
        //      },
        //      function (error) {
        //          console.log("writeExistingFile error", error, cordova.file.dataDirectory, storageFilename)
        //        q.reject(error);
        //      }
        //    );
        //  },
        //  function() {
        //    // writeFile(path, fileName, text, replaceBool)
        //    $cordovaFile.writeFile(cordova.file.dataDirectory, storageFilename, message, true).then(
        //      function() {
        //        q.resolve();
        //      },
        //      function (error) {
        //          console.log("writeFile error", error, cordova.file.dataDirectory, storageFilename)
        //        q.reject(error);
        //      }
        //    );

              
        //  }
        //);

      }

      return q.promise;
    }

    var errorHandler = function (fileName, e) {
        var msg = '';
        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'Storage quota exceeded';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'File not found';
                break;
            case FileError.SECURITY_ERR:
                msg = 'Security error';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'Invalid modification';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'Invalid state';
                break;
            default:
                msg = 'Unknown error';
                break;
        };
        console.log('Error (' + fileName + '): ' + msg);
    }

    function getLogfile() {
      var q = $q.defer();

      console.log('getLogfile');

      if (isBrowser()) {
          console.log('getLogfile isBrowser');
        q.resolve($window.localStorage[storageFilename]);
      } else {

          console.log('getLogfile local');

        if (!$window.cordova || !$window.cordova.file || !$window.cordova.file.dataDirectory) {
            q.reject('cordova.file.dataDirectory is not available');
            console.log('cordova.file.dataDirectory is not available');
          return q.promise;
        }

        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
            console.log("getLogfile filesystem", directoryEntry)
            directoryEntry.getFile(storageFilename, { create: false },
                          function (fileEntry) {

                              fileEntry.file(function (file) {
                                  var reader = new FileReader();

                                  reader.onloadend = function () {
                                      console.log("getLogfile reader result", this.result)
                                      q.resolve(this.result);
                                  };

                                  reader.readAsText(file);

                              });
                          }
                      );
        });

        //$cordovaFile.readAsText(cordova.file.dataDirectory, storageFilename).then(
        //  function (result) {
        //      console.log('getLogfile result', result);
        //    q.resolve(result);
        //  },
        //  function (error) {
        //      console.log('getLogfile error', error);

        //    q.reject(error);
        //  }
        //);
      }

      return q.promise;
    }


    function deleteLogfile() {
      var q = $q.defer();

      if (isBrowser()) {
        $window.localStorage.removeItem(storageFilename);
        q.resolve();
      } else {

        if (!$window.cordova || !$window.cordova.file || !$window.cordova.file.dataDirectory) {
          q.reject('cordova.file.dataDirectory is not available');
          return q.promise;
        }

        $cordovaFile.removeFile(cordova.file.dataDirectory, storageFilename).then(
          function(result) {
            q.resolve(result);
          },
          function(error) {
            q.reject(error);
          }
        );
      }

      return q.promise;
    }


    function setStorageFilename(filename) {
      if (angular.isString(filename) && filename.length > 0) {
        storageFilename = filename;
        return true;
      } else {
        return false;
      }
    }


    function setTimestampFormat(format, timezone) {
      if (!(angular.isUndefined(format) || angular.isString(format))) {
        throw new TypeError('format parameter must be a string or undefined');
      }
      if (!(angular.isUndefined(timezone) || angular.isString(timezone))) {
        throw new TypeError('timezone parameter must be a string or undefined');
      }

      dateFormat = format;
      dateTimezone = timezone;
    }


    function checkFile() {
      var q = $q.defer();

      if (isBrowser()) {

        q.resolve({
          'name': storageFilename,
          'localURL': 'localStorage://localhost/' + storageFilename,
          'type': 'text/plain',
          'size': ($window.localStorage[storageFilename] ? $window.localStorage[storageFilename].length : 0)
        });

      } else {

        if (!$window.cordova || !$window.cordova.file || !$window.cordova.file.dataDirectory) {
            q.reject('cordova.file.dataDirectory is not available');
            console.log('cordova.file.dataDirectory is not available')
          return q.promise;
        }

        $cordovaFile.checkFile(cordova.file.dataDirectory, storageFilename).then(function(fileEntry) {
          fileEntry.file(q.resolve, q.reject);
        }, q.reject);

      }

      return q.promise;
    }

    function debug() {
      var args = Array.prototype.slice.call(arguments, 0);
      args.unshift('DEBUG');
      log.apply(undefined, args);
    }


    function info() {
      var args = Array.prototype.slice.call(arguments, 0);
      args.unshift('INFO');
      log.apply(undefined, args);
    }


    function warn() {
      var args = Array.prototype.slice.call(arguments, 0);
      args.unshift('WARN');
      log.apply(undefined, args);
    }


    function error() {
      var args = Array.prototype.slice.call(arguments, 0);
      args.unshift('ERROR');
      log.apply(undefined, args);
    }


    return {
      log: log,
      getLogfile: getLogfile,
      deleteLogfile: deleteLogfile,
      setStorageFilename: setStorageFilename,
      setTimestampFormat: setTimestampFormat,
      checkFile: checkFile,
      debug: debug,
      info: info,
      warn: warn,
      error: error,
      writeLog: writeLog
    };

  }]);

})();