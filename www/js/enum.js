﻿var TransitionType = {
    ENTER: 1,
    EXIT: 2,
    BOTH: 3
}

simpalTek.Enum = simpalTek || {};

simpalTek.Enum.messageType = Object.freeze({
    info: { value: 1, name: "info" },
    warning: { value: 2, name: "warning" },
    danger: { value: 3, name: "danger" },
    success: { value: 4, name: "success" },
});

simpalTek.Enum.messageStatus = Object.freeze({
    unseen: { value: 1, name: "unseen" },
    shown: { value: 2, name: "shown" },
    next: { value: 3, name: "next" },
    permanent: { value: 4, name: "permanent" },
});

simpalTek.Enum.errorSeverity = Object.freeze({
    critical: { value: 1, name: "Critical" },
    error: { value: 2, name: "Error" },
    info: { value: 3, name: "Informational" },
    logAlways: { value: 4, name: "LogAlways" },
    verbose: { value: 5, name: "Verbose" },
    warning: { value: 6, name: "Warning" },
    debug: { value: 7, name: "Debug" },
});