﻿'use strict';
services.factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', '$timeout', function ($q, $injector, $location, localStorageService, $timeout) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};
            
            
        var authData = simpalTek.global.localStorage.get('authorizationData');

        if (config.url.indexOf(apiDeviceBaseUri) < 0) {
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }
        }

        return config;
    }

        var _responseError = function (rejection) {
        
        //if (rejection.config.url.indexOf(apiDeviceBaseUri) < 0)
            simpalTek.helper.showLoader(false);

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Response error [" + JSON.stringify(rejection) + "]");
                    
        if (rejection.status === 0) {
            if (rejection.config.url.indexOf(apiDeviceBaseUri) < 0)
                simpalTek.helper.messageCenterService(simpalTek.const.NO_INTERNET_MSG, simpalTek.Enum.messageType.warning, simpalTek.Enum.messageStatus.shown);
        }
                    
        if (rejection.status === 401) {
            var authService = $injector.get('authService');
            var authData = simpalTek.global.localStorage.get('authorizationData');

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Service rejected.  User unauthorize.");

            if (authData) {
                if (authData.useRefreshTokens) {
                    $location.path('/refresh');
                    return $q.reject(rejection);
                }
            }

            if (rejection.config.url.indexOf(apiDeviceBaseUri) < 0){
                authService.logOut();

                $timeout(function () {
                    $location.path('/signin');
                    //$state.go("signin");
                }, 500);

                //$location.path('/login');
            }
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);