﻿'use strict';
services.factory('authService', ['$http', '$q', 'localStorageService', 'ngAuthSettings', '$rootScope', function ($http, $q, localStorageService, ngAuthSettings, $rootScope) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        useRefreshTokens: false,
        userFullName: "",
        accessToken: ""
    };

    var _externalAuthData = {
        provider: "",
        userName: "",
        externalAccessToken: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + '/api/UserAccount', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        //if (loginData.useRefreshTokens) {
            data = data + "&client_id=" + ngAuthSettings.clientId;
        //}

        var deferred = $q.defer();

        $http.post(serviceBase + '/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } }).success(function (response) {
                console.log("response", response)

            if (loginData.useRefreshTokens) {
                simpalTek.global.localStorage.set('authorizationData', { token: response.access_token, userName: loginData.userName, userFullName: response.userFullName, refreshToken: response.refresh_token, useRefreshTokens: true });
            }
            else {
                simpalTek.global.localStorage.set('authorizationData', { token: response.access_token, userName: loginData.userName, userFullName: response.userFullName, refreshToken: "", useRefreshTokens: false });
            }
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.useRefreshTokens = loginData.useRefreshTokens;
            _authentication.userFullName = response.userFullName;
            _authentication.accessToken = response.access_token;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        simpalTek.global.localStorage.remove('authorizationData');
        simpalTek.global.localStorage.remove("currentNexxGarage")
        simpalTek.global.localStorage.remove("UserDb")

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.useRefreshTokens = false;

        $rootScope.$broadcast('sessionStop');
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "App Log Out");

    };

    var _fillAuthData = function () {

        var authData = simpalTek.global.localStorage.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.userFullName = authData.userFullName;
            _authentication.useRefreshTokens = authData.useRefreshTokens;
            _authentication.accessToken = authData.token;
        }

    };

    var _refreshToken = function () {
        var deferred = $q.defer();

        var authData = simpalTek.global.localStorage.get('authorizationData');

        if (authData) {

            if (authData.useRefreshTokens) {

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;

                simpalTek.global.localStorage.remove('authorizationData');

                $http.post(serviceBase + '/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    simpalTek.global.localStorage.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
            }
        }

        return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

        var deferred = $q.defer();

        $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

            simpalTek.global.localStorage.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _registerExternal = function (registerExternalData) {

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {

            simpalTek.global.localStorage.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _forgotPassword = function (data) {
        var deferred = $q.defer();

        var data = "username=" + data.userName;

        $http.post(serviceBase + '/api/PasswordForgot', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            
            deferred.resolve(response);

        }).error(function (err, status) {

            deferred.reject(err);
        });
           

        return deferred.promise;
    };

    var _resetPassword = function (data) {
        var deferred = $q.defer();

        var data = "username=" + data.userName + "&password=" + data.Password + "&resetCode=" + data.ResetCode;

        $http.put(serviceBase + '/api/PasswordReset', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {


            deferred.resolve(response);

        }).error(function (err, status) {

            deferred.reject(err);
        });


        return deferred.promise;
    };

    authServiceFactory.resetPassword = _resetPassword;
    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.refreshToken = _refreshToken;
    authServiceFactory.forgotPassword = _forgotPassword;

    authServiceFactory.obtainAccessToken = _obtainAccessToken;
    authServiceFactory.externalAuthData = _externalAuthData;
    authServiceFactory.registerExternal = _registerExternal;

    return authServiceFactory;
}]);