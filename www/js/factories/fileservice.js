﻿'use strict';
//Filesystem (checkDir, createDir, checkFile, creatFile, removeFile, writeFile, readeFile)
services.factory('FileService', function ($q) {
    
    return {
        checkDir: function (dir) {
            var deferred = $q.defer();

            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getDirectory(dir, {create: false},
                        function() {
                            //Dir exist
                            deferred.resolve();
                        },
                        function() {
                            //Dir dont exist
                            deferred.reject();
                        }
                    );
                }
            );
                    
            return deferred.promise;
        },
   
        createDir: function (dir) {
            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getDirectory(dir, {create: true});
                }
            );
        },
        
        checkFile: function (dir, file) {
            var deferred = $q.defer();
            
            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getFile('/'+ dir +'/'+ file, {create: false}, 
                        function() {
                            //File exist
                            deferred.resolve();
                        },
                        function() {
                            //File dont exist
                            deferred.reject();
                        }
                    );
                }
            );
                    
            return deferred.promise;
        },
        
        createFile: function (dir, file) {
            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getFile('/'+ dir +'/'+ file, {create: true});
                }
            );
        },
        
        removeFile: function (dir, file) {
            var deferred = $q.defer();

            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getFile('/'+ dir +'/'+ file, {create: false}, function(fileEntry){
                        fileEntry.remove(function() {
                            deferred.resolve();
                        });
                    });
                }
            );
            
            return deferred.promise;
        },
        
        writeFile: function (dir, file, data, replace) {
            var deferred = $q.defer();
            
            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getFile('/' + dir + '/' + file, { create: replace },
                        function(fileEntry) {
                            fileEntry.createWriter(function(fileWriter) {
                                fileWriter.onwriteend = function (e) {
                                    console.log('Write completed.');
                                };

                                fileWriter.onerror = function (e) {
                                    console.log('Write failed: ' + e.toString());
                                };

                                var blob = new Blob([data], { type: 'text/plain' });
                                fileWriter.write(blob);

                                deferred.resolve(fileWriter);
                            });
                        }
                    );
                }
            );
                    
            return deferred.promise;
        },
        
        readeFile: function (dir, file) {
            var deferred = $q.defer();
              
            getFilesystem().then(
                function(filesystem) {
                    filesystem.root.getFile('/'+ dir +'/'+ file, {create: false},
                        function(fileEntry) {

                            fileEntry.file(function(file) {
                                var reader = new FileReader();

                                reader.onloadend = function() {
                                    deferred.resolve(this.result);
                                };

                                reader.readAsText(file);

                            });
                        }
                    );
                }
            );
            
            return deferred.promise;
        }
        
    };
    
    function getFilesystem() {
        var deferred = $q.defer();
        
        window.requestFileSystem(window.PERSISTENT, 1024*1024, function(filesystem) {
            deferred.resolve(filesystem);
        });
        
        return deferred.promise;
    }
});