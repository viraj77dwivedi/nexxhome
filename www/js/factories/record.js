/**
 * Record service
 * @module record
 
 * @class simpalGate
 */
 services.factory('$record', [

  '$rootScope',
  '$socket',
  '$account',

  function($rootScope, $socket, $account) {

    var enumerator = 0;
	var iOSLocalPath = 'cdvfile://localhost/temporary/';
    var recordName = 'record-'+enumerator+'.wav';
    var mediaRec = null;
    var OnCallback = null;
    var OnAppendData = {};
	
    /**
    * Start a record
    *
    * @method startRecord
    */
    function startRecord(){
      enumerator++;
      recordName = 'record-'+enumerator+'.wav';
	  console.log("recordName", recordName);
	  
	  mediaRec = new Media(recordName,
          function() {
			  //mediaRec.release();
          },
          function(err) {
          });
		 
	  
      mediaRec.startRecord();
    }

    /**
    * Stop record
    *
    * @method stopRecord
    */
    function stopRecord(){
      mediaRec.stopRecord();
	
	  /*var mediaFile = new Media(recordName,
          function() {
            console.log("playAudio():Audio Success");
          },
          function(err) {
            console.log("playAudio():Audio Error: ", err);
          }
      );*/
	  //mediaRec.play();
	  
	  //console.log("mediaFile", mediaFile);
	  //socket.emit('audioData', { data: mediaFile });
    }

    /**
    * Stop record
    *
    * @method stopRecord
    */
    function playRecord(){
		
      mediaRec.play();
    }

    /**
    * Get the name of the record
    *
    * @method getRecord
    */
    function getRecord(){
      return recordName;
    }

    /**
    * Save the recorded file to the server
    *
    * @method save
    */
    function save(callback,appendData){
	  console.log("save called");	
	  
      OnCallback = callback;
      OnAppendData = appendData;
      //window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, OnFileSystem, fail);
	  window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, OnFileSystem, fail);
	  
    }

    /**
    * Callback for setting the file system to persistent.
    *
    * @method OnFileSystem
    */
    function OnFileSystem(fileSystem){
		console.log("OnFileSystem called");
		console.log("recordName", recordName);
      fileSystem.root.getFile(recordName, {create: true, exclusive: false}, OnGetFile, fail);
    }

    /**
    * Callback for geting the file for disk
    *
    * @method OnGetFile
    */
    function OnGetFile(fileEntry){
		console.log("OnGetFile called");
		console.log("File created at " + fileEntry.fullPath);
      fileEntry.file(OnFileEntry, fail);
    }

    /**
    * Callback for file entry, this get the file.
    *
    * @method OnFileEntry
    */
    function OnFileEntry(file){
		console.log("OnFileEntry called");
		console.log("file", file);
      var reader = new FileReader();
      reader.onloadend = function(evt) {
			console.log("reader.onloadend called");
			console.log("evt", evt);
          var binaryAudio = evt.target.result;
          //var base64Data  =   image.replace(/^data:audio\/mpeg;base64,/, "");
		  //var base64Data  =   image.replace(/^data:audio\/wav;base64,/, "");
          //base64Data  +=  base64Data.replace('+', ' ');

          //$socket.emit('playlists:file',{file:base64Data,name:recordName, token: $account.token(), info:OnAppendData},OnCallback);
		  console.log("start audio file emit");
		  //console.log("audio data", binaryAudio);
		  
		  /*var blob = null;
            var builder = new WebKitBlobBuilder();
            for(var i = 0; i < evt.target.result.length; i++){
                builder.append(evt.target.result[i]);
            }
            blob = builder.getBlob("audio/wav");
			*/
			
		//	var arr = new Uint8Array(binaryAudio.byteLength);
        //for (var i = 0; i < arr.length; i++) {
        //    arr[i] = binaryAudio.charCodeAt(i)  & 0xff;
        //}
        //var bl = new window.BlobBuilder();
        //bl.append(arr.buffer);
		//bl.append(binaryAudio);
        //blob = bl.getBlob(type);
		
		var base64 = binaryAudio.split(',')[1];
		
		//console.log("blob", base64);
		  $socket.emit('audioData',{file:base64,name:recordName, info:OnAppendData});
		  //socket.emit('send-file', file.name, buffer);
		  console.log("end audio file emit");
      };
      reader.readAsDataURL(file);
	  //reader.readAsBinaryString(file);
	  //reader.readAsArrayBuffer(file);

    }

    /**
    * When any process of saving file fail, this console the error.
    *
    * @method OnFileEntry
    */
    function fail(err){
      console.log('Error');
      console.log(err);
    }

    /**
    * Play record
    *
    * @method playRecord
    */
    function playRecord(){
		console.log("recordName", recordName);
      var mediaFile = new Media(recordName,
          function() {
			  mediaFile.release();
            console.log("playAudio():Audio Success");
          },
          function(err) {
            console.log("playAudio():Audio Error: ", err);
          }
      );
      // Play audio
      mediaFile.play();
	  
	  console.log("mediaRec", mediaFile);
    }

  return {
    start: startRecord,
    stop: stopRecord,
    play:playRecord,
    name:getRecord,
    save:save
  };
}]);



function exportMonoWAV(type){
	
type = type || "audio/wav";
  var bufferL = mergeBuffers(recBuffersL, recLength);
  
  var downsampledBuffer = downsampleBuffer(bufferL);
  
  var dataview = encodeWAV(downsampledBuffer, true);
  var audioBlob = new Blob([dataview], { type: type });

  this.postMessage(audioBlob);
}

function mergeBuffers(recBuffers, recLength){
  var result = new Float32Array(recLength);
  var offset = 0;
  for (var i = 0; i < recBuffers.length; i++){
    result.set(recBuffers[i], offset);
    offset += recBuffers[i].length;
  }
  return result;
}

function downsampleBuffer(buffer, rate) {
    //rate = 16000;
	rate = rate || 16000;
	console.log("sampleRate", sampleRate);
    if (rate == sampleRate) {
        return buffer;
    }
    if (rate > sampleRate) {
        throw "downsampling rate show be smaller than original sample rate";
    }
    var sampleRateRatio = sampleRate / rate;
    var newLength = Math.round(buffer.length / sampleRateRatio);
    var result = new Float32Array(newLength);
    var offsetResult = 0;
    var offsetBuffer = 0;
    while (offsetResult < result.length) {
        var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
        var accum = 0, count = 0;
        for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
            accum += buffer[i];
            count++;
        }
        result[offsetResult] = accum / count;
        offsetResult++;
        offsetBuffer = nextOffsetBuffer;
    }
    return result;
}

function encodeWAV(samples){
var buffer = new ArrayBuffer(44 + samples.length * 2);
var view = new DataView(buffer);
 
/* RIFF identifier */
writeString(view, 0, 'RIFF');
/* file length */
view.setUint32(4, 32 + samples.length * 2, true);
/* RIFF type */
writeString(view, 8, 'WAVE');
/* format chunk identifier */
writeString(view, 12, 'fmt ');
/* format chunk length */
view.setUint32(16, 16, true);
/* sample format (raw) */
view.setUint16(20, 1, true);
/* channel count */
//view.setUint16(22, 2, true); /*STEREO*/
view.setUint16(22, 1, true); /*MONO*/
/* sample rate */
view.setUint32(24, sampleRate, true);
/* byte rate (sample rate * block align) */
//view.setUint32(28, sampleRate * 4, true); /*STEREO*/
view.setUint32(28, sampleRate * 2, true); /*MONO*/
/* block align (channel count * bytes per sample) */
//view.setUint16(32, 4, true); /*STEREO*/
view.setUint16(32, 2, true); /*MONO*/
/* bits per sample */
view.setUint16(34, 16, true);
/* data chunk identifier */
writeString(view, 36, 'data');
/* data chunk length */
view.setUint32(40, samples.length * 2, true);
 
floatTo16BitPCM(view, 44, samples);
 
return view;
}

function floatTo16BitPCM(output, offset, input){
  for (var i = 0; i < input.length; i++, offset+=2){
    var s = Math.max(-1, Math.min(1, input[i]));
    output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
  }
}

function writeString(view, offset, string){
  for (var i = 0; i < string.length; i++){
    view.setUint8(offset + i, string.charCodeAt(i));
  }
}