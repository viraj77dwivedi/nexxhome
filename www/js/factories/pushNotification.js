'use strict';
services.factory('$pushNotification', ['$q', '$ionicUser', '$ionicPush', function ($q, $ionicUser, $ionicPush) {
        
        var pushNotificationService = {};
        
        var identifyUser = function () {
            var user = $ionicUser.get();
            if (!user.user_id) {
                // Set your user_id here, or generate a random one.
                user.user_id = $ionicUser.generateGUID();
            };
            
            // Add some metadata to your user object.
            angular.extend(user, {
                name: 'Technews',
                bio: 'Hardcoded for now'
            });
            
            // Identify your user with the Ionic User Service
            $ionicUser.identify(user).then(function () {
                //alert('Identified user ' + user.name + '\n ID ' + user.user_id);
                pushRegister();
                return true;
            });
        }
            
        var pushRegister = function () {
            // Register with the Ionic Push service.  All parameters are optional.
            $ionicPush.register({
                canShowAlert: true, //Can pushes show an alert on your screen?
                canSetBadge: true, //Can pushes update app icon badges?
                canPlaySound: true, //Can notifications play a sound?
                canRunActionsOnWake: true, //Can run actions outside the app,
                onNotification: function (notification) {
                    // Handle new push notifications here
                    // console.log(notification);
                    alert(notification);
                    return true;
                }
            });
        }
        
        pushNotificationService.identifyUser = identifyUser;
                
        return pushNotificationService;
    }]);

//services.factory('PushNotificationService', ['$q', '$ionicUser', '$ionicPush',
//    function ($q, $ionicUser, $ionicPush) {
        
//    var PushNotificationService = {};
    
//    var identifyUser = function () {
//        var user = $ionicUser.get();
//        if (!user.user_id) {
//            // Set your user_id here, or generate a random one.
//            user.user_id = $ionicUser.generateGUID();
//        };
        
//        // Add some metadata to your user object.
//        angular.extend(user, {
//            name: 'Technews',
//            bio: 'Hardcoded for now'
//        });
        
//        // Identify your user with the Ionic User Service
//        $ionicUser.identify(user).then(function () {
//            //alert('Identified user ' + user.name + '\n ID ' + user.user_id);
//            pushRegister();
//            return true;
//        });
//    }

//       var pushRegister = function () {
//        // Register with the Ionic Push service.  All parameters are optional.
//        $ionicPush.register({
//            canShowAlert: true, //Can pushes show an alert on your screen?
//            canSetBadge: true, //Can pushes update app icon badges?
//            canPlaySound: true, //Can notifications play a sound?
//            canRunActionsOnWake: true, //Can run actions outside the app,
//            onNotification: function (notification) {
//                // Handle new push notifications here
//                // console.log(notification);
//                alert(notification);
//                return true;
//            }
//        });
        
                 
//        return {identifyUser: identifyUser};
//    }
//}]);