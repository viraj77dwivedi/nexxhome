services.factory("Geofence", function (
    $rootScope,
    $window,
    $q,
    $log,
    $ionicLoading,
    $ionicPopup,
    Settings,
    $http,
    ngAuthSettings,
    authService,
    $interval,
    $timeout
) {

    // Convenient, private reference to BackgroundGeolocation API
    var bgGeo, map, bgGeoState;
    var alreadyNotified = false;
    var locCurrentSpeed = 0;
    var currentPositionCache;
    var addOrUpdateCache;
    var lastgeoFenceTriggerDateTime = "";
    var lastgeoFenceTriggerDateTimeTrue = null;
    var lastInVehicleActivityDate = null;
    var lastActivity = "";
    var lastProximityLocationDesc = "";
    var latLogDelta;
    var secSinceGeofenceEventTriggered = 0;
    var last2LatLogDelta = [];
    var geofenceTrigger = {};
    var enterSignalTrigger = false;
    var userDb = null;
    var signalData = {};
    var userDeviceAccess = null;
    var currentNexxGarage;
    var isMovingCloser = false;
    var currentActivity = "";
    var geoLocDataStore = simpalTek.global.localStorage.get('GeoLocDataStore');
    var geoFenceExitDistance = 0;
    var aggressiveMonitoringActive = false;
    var userDevicePreferOpenDistance = 250;  //default to 250 ft

    geofenceTrigger.latitude = 0;
    geofenceTrigger.longitude = 0;
    //var $rootScope = null;
    //var Settings = null;

    document.addEventListener('deviceready', function () {
        bgGeo = window.BackgroundGeolocation;

        if (!geoLocDataStore) {
            geoLocDataStore = {};
            geoLocDataStore.exitDistance = -1;                        
        }

    }, false);

    /**
 * BackgroundGeolocation Location callback
 * @param {Object} location
 * @param {Integer} taskId
 */
    function onLocation(location, taskId) {

        var proximityLocationDesc = "";
        var config = simpalTek.global.bgGeoConfig;
                
            try {

                BackgroundGeolocation.getCount(function (count) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Location count: " +count);
                });

                if (location.sample !== true) {

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'onLocation triggered ' + JSON.stringify(location));

                    if (geofenceTrigger.latitude != 0) {
                        latLogDelta = simpalTek.helper.geo().distance(geofenceTrigger.latitude, geofenceTrigger.longitude, location.coords.latitude, location.coords.longitude, 'F');

                        last2LatLogDelta.push(latLogDelta);

                        if (last2LatLogDelta.length >= 3)
                            last2LatLogDelta.shift();

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'latLogDelta - ' + latLogDelta);
                        if (last2LatLogDelta.length > 1) {
                            if (last2LatLogDelta[0] > last2LatLogDelta[1])
                                isMovingCloser = true;
                            else
                                isMovingCloser = false;
                        }
                        else
                            isMovingCloser = true;

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Open Condition ' + userDevicePreferOpenDistance + "ft|enterSignalTrigger:" + enterSignalTrigger + "|currentActivity:" + currentActivity + "|isMovingCloser:" + isMovingCloser + "|location.accuracy:" + location.coords.accuracy + "|location.activity.confidence:" + location.activity.confidence);

                        //if ((latLogDelta > 100 && latLogDelta < 250) && enterSignalTrigger) {
                        //if (latLogDelta < (ionic.Platform.isAndroid() ? 150 : 250) && enterSignalTrigger && (currentActivity != 'on_foot' && currentActivity != 'still') && isMovingCloser) {
                        //if (latLogDelta < (ionic.Platform.isAndroid() ? 200 : 250) && enterSignalTrigger && geoLocDataStore.exitDistance > 400) {
                        if (latLogDelta <= userDevicePreferOpenDistance && enterSignalTrigger) {

                            location.exitDistance = geoLocDataStore.exitDistance;
                            location.latLogDelta = latLogDelta;

                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'GEO FENCE OPEN TRIGGER ' + JSON.stringify(location));

                            //reset the geo fence exit distance
                            geoLocDataStore.exitDistance = 0;
                            simpalTek.global.localStorage.set('GeoLocDataStore', geoLocDataStore);

                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Signal sent to open");
                            enterSignalTrigger = false;
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Disable aggressive GPS monitoring.  Engages stationary-mode.');
                            bgGeo.changePace(false); // <-- Disable aggressive GPS monitoring.  Engages stationary-mode.
                            aggressiveMonitoringActive = false;
                            geofenceTrigger.longitude = 0;

                            geofenceService.restart();

                            signalData.signalType = 'auto';

                            signalData.locationData = JSON.stringify(location);

                            if (userDeviceAccess) {
                                signalData.SmartGateDeviceID = userDeviceAccess[0].vendorDeviceId; //userDb.registeredDevices[0].deviceName;
                                signalData.deviceId = userDeviceAccess[0].deviceId;

                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device open ' + JSON.stringify(signalData));

                            }
                            else {
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "auto open userDeviceAccess is null");
                            }

                            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "signalData " + JSON.stringify(signalData));

                            simpalTek.helper.sendDoorSignal(signalData,
                                function (response) {
                                    userDb.gateActivities = response.result;
                                    simpalTek.global.localStorage.set('UserDb', userDb);
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Enter - Garage Signal successfully sent " + signalData.SmartGateDeviceID);
                                },
                                function (err, status) {
                                    console.log("Garage signal error", err, status)
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Garage signal error [" + JSON.stringify(err) + "]");
                                });

                            userDeviceAccess = null;
                            signalData = {};

                        }
                    }

                }

                lastProximityLocationDesc = proximityLocationDesc;
            }
            catch (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'onLocation proximity checked failed - ' + JSON.stringify(e) + " | userDeviceAccess:" + JSON(userDeviceAccess) + "| userdbdevicesAccess: " + JSON(simpalTek.global.localStorage.get('UserDb').devicesAccess));
            }
        
            config = null;

    }
    /**
    * Background Geolocation error callback
    * @param {Integer} code
    */
    function onLocationError(error) {
        console.error('[js] Location error: ', error);
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(error));
    }
    /**
    * Background Geolocation HTTP callback
    */
    function onHttpSuccess(response) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(response));
    }
    /**
    * BackgroundGeolocation HTTP error
    */
    function onHttpError(error) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(error));
    };
    /**
    * Background Geolocation motionchange callback
    */
    function onMotionChange(isMoving, location, taskId) {
        //console.log('[js] onMotionChange: ', isMoving, location);


        if (isMoving) {
            //console.log('Device has just started MOVING', location);
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device has just started MOVING');
        } else {
            //console.log('Device is in STATIONARY MODE', location);
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device is in STATIONARY MODE');
        }

        
        bgGeo.finish(taskId);
    }

    function onError(error) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence error : " + error.type + " error: " + error.code);
    }

    function onGeofencesChange(event) {
        var on = event.on;   //<-- new geofences activiated.
        var off = event.off; //<-- geofences that were de-activated.
        var activeGeofence;
                       
        for (var n = 0, len = on.length; n < len; n++) {
            activeGeofence = on[n];

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence activiated : " + JSON.stringify(activeGeofence))
        }

        // de-activated 
        for (var n = 0, len = off.length; n < len; n++) {
            var identifier = off[n];
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence de-activated : " + JSON.stringify(identifier))
        }
    }

    function onActivityChange(activityName) {

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] onActivityChange: ' + activityName);
        currentActivity = activityName;

    }

    function onProviderChange(provider) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] onProviderChange: ' + JSON.stringify(provider));
    }

    /**
    * BackgroundGeolocation heartbeat event handler
    */
    function onHeartbeat(params) {
        var shakes = params.shakes;
        var location = params.location;
        console.log('- heartbeat: ', params);
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- heartbeat: ' + JSON.stringify(params));
        /**
        * OPTIONAL.  retrieve current position during heartbeat callback
        *
        bgGeo.getCurrentPosition(function(location, taskId) {
          console.log("- location: ", location);
          bgGeo.finish(taskId);      
        });
        *
        *
        */
    }
    /**
    * BackgroundGeolocation schedule event-handler
    */
    function onSchedule(state) {
        console.info('- Schedule event: ', state.enabled, state);

        //$scope.$apply(function () {
        //    $scope.state.enabled = state.enabled;
        //    $scope.state.isMoving = false;
        //});
    }

    /**
    * BackgroundGeolocation geofence callback
    */
    function onGeofence(params, taskId) {
        console.log('- onGeofence: ', JSON.stringify(params, null, 2));

        var garageSignalData = {};
        var authService = null;
        var currentSpeed = 0;
        var identifier = "";
        var userFirstName = "";
        var debug = "";

        var geoFenceTriggerDateTime = "";
        var geoFenceLatLogDelta = 0;

        
        if (ionic.Platform.isAndroid())
        currentActivity = params.location.activity.type;


        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence data: " + JSON.stringify(params, null, 2));
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'lastProximityLocationDesc ' + lastProximityLocationDesc);

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'last2LatLogDelta ' + JSON.stringify(last2LatLogDelta));
        
        document.addEventListener('deviceready', function () {
            currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
            var autoCloseMsg = "";

            if (lastgeoFenceTriggerDateTimeTrue != null)
                secSinceGeofenceEventTriggered = (new Date() - lastgeoFenceTriggerDateTimeTrue) / 1000;
            else if (secSinceGeofenceEventTriggered == 0 && (lastProximityLocationDesc != "at_home" && lastProximityLocationDesc != "in_neighborhood")) {
                secSinceGeofenceEventTriggered = 99;
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "App restart outside home & neighborhood area");
            }

            geoFenceTriggerDateTime = new Date().toString();
            lastgeoFenceTriggerDateTimeTrue = new Date();

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Second from last Geofence Event Triggered " + secSinceGeofenceEventTriggered);

            try {
                userDb = simpalTek.global.localStorage.get('UserDb');
                authService = angular.element(document.body).injector().get('authService');
                identifier = params.identifier;
                
                if (userDb.preference.geofences) {
                    var userDeviceGeoFenceConfig = userDb.preference.geofences.filter(function (x) { return x.identifier == identifier });

                    if(userDeviceGeoFenceConfig.length > 0)
                    {
                        userDevicePreferOpenDistance = userDeviceGeoFenceConfig[0].radius * 3.28084;
                    }
                }

                //garageSignalData.signalType = 'auto';

                //if (userDb.registeredDevices.length > 0)
                //    garageSignalData.SmartGateDeviceID = userDb.registeredDevices[0].deviceName;
                //else
                //    garageSignalData.SmartGateDeviceID = currentNexxGarage.vendorDeviceId; //simpalTek.global.localStorage.get("currentVendorDeviceId");

                if (userDb.devicesAccess.length > 0) {

                    if (identifier.indexOf("-monitor") > 0) {
                        identifier = identifier.split("-")[0];
                    }

                    userDeviceAccess = userDb.devicesAccess.filter(function (item) { return item.deviceId == identifier })

                    if (userDeviceAccess.length > 0) {
                        currentNexxGarage.vendorDeviceId = userDeviceAccess[0].vendorDeviceId;
                        currentNexxGarage.deviceId = userDeviceAccess[0].deviceId;

                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)
                    }
                }
                //alert(JSON.stringify(params, null, 2))
            } catch (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "On Geofence preset: " + e.toString())
            }

            if (params.action == "EXIT") {
                geofenceTrigger.latitude = 0;
                geofenceTrigger.longitude = 0;
                enterSignalTrigger = false;

                bgGeo.getGeofences(function (geofences) {

                    try {
                        for (var i = 0; i < geofences.length; i++) {

                            if (params.identifier == geofences[i].identifier) {
                                geoLocDataStore.exitDistance = simpalTek.helper.geo().distance(geofences[i].latitude, geofences[i].longitude, params.location.coords.latitude, params.location.coords.longitude, 'F');
                               
                                simpalTek.global.localStorage.set('GeoLocDataStore', geoLocDataStore);

                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'geoFenceLatLogDelta Exit - ' + geoLocDataStore.exitDistance);

                            }
                        }
                    }
                    catch (e) { simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Geo Exit Error: " + e.toString()) }

                    if (params.identifier.indexOf("monitor") > 0) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Monitor zone EXIT.');
                        geofenceService.restart();
                    }
                    else {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Start location tracking on EXIT.');
                        BackgroundGeolocation.start();
                    }

                    bgGeo.finish(taskId);
                });

               
            }
            else if (params.action == "ENTER") {


                if (lastgeoFenceTriggerDateTime == geoFenceTriggerDateTime) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence double event called.  Call terminated.");
                }

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "lastgeoFenceTriggerDateTime " + lastgeoFenceTriggerDateTime);
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geoFenceTriggerDateTime " + geoFenceTriggerDateTime);

                //if (lastgeoFenceTriggerDateTime != geoFenceTriggerDateTime) {

                //    lastgeoFenceTriggerDateTime = geoFenceTriggerDateTime;
                          
                    bgGeo.getGeofences(function (geofences) {
                        try {
                           
                            for (var i = 0; i < geofences.length; i++) {

                                if (params.identifier == geofences[i].identifier) {
                                    geoFenceLatLogDelta = simpalTek.helper.geo().distance(geofences[i].latitude, geofences[i].longitude, params.location.coords.latitude, params.location.coords.longitude, 'F');
                                    geofenceTrigger.latitude = geofences[i].latitude;
                                    geofenceTrigger.longitude = geofences[i].longitude;

                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'geoFenceLatLogDelta - ' + geoFenceLatLogDelta);

                                    if (params.identifier.indexOf("monitor") > 0) {
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Monitor zone start tracking.');
                                        BackgroundGeolocation.start();
                                    }
                                    else {
                                        enterSignalTrigger = true;
                                        aggressiveMonitoringActive = true;

                                        setTimeout(function () {
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Aggressive GPS monitoring immediately engaged.');
                                            BackgroundGeolocation.changePace(true);  // <-- Aggressive GPS monitoring immediately engaged.
                                        }, 1000);

                                        //stop the auto open to trigger after 2.9 minutes
                                        BackgroundGeolocation.startBackgroundTask(function (taskId) {
                                            setTimeout(function () {
                                                if (enterSignalTrigger) {
                                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "auto open stop after time period has lapse");

                                                    geofenceTrigger.longitude = 0;
                                                    enterSignalTrigger = false;
                                                    aggressiveMonitoringActive = false;
                                                    geofenceService.restart();
                                                }

                                                BackgroundGeolocation.finish(taskId);

                                            }, 174000)
                                        })
                                    }
                            
                                } 
                            }
                        }
                        catch (e) { simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Geo Enter Error: " + e.toString()) }

                        bgGeo.finish(taskId);

                    });

                //}

            }
            else if (params.action == "DWELL") {

                bgGeo.finish(taskId);
                
            }

        }, false);

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geofence bgtask finished");

    }

    /**
  * Configure BackgroundGeolocation plugin
  */
    function configureBackgroundGeolocation() {
        //var config = Settings.getConfig();
        var config = simpalTek.global.bgGeoConfig;

        // NOTE:  Optionally generate a schedule here.  @see /www/js/tests.js
        //  1: how many schedules?
        //  2: delay (minutes) from now to start generating schedules
        //  3: schedule duration (minutes)
        //  4: time between (minutes) generated schedule ON events
        //
        // UNCOMMENT TO AUTO-GENERATE A SERIES OF SCHEDULE EVENTS BASED UPON CURRENT TIME:
        //config.schedule = Tests.generateSchedule(24, 1, 1, 1);
        //
        //config.url = 'http://192.168.11.120:8080/locations';
        config.params = {};

        // Attach Device info to BackgroundGeolocation params.device    
        config.params.device = ionic.Platform.device();

        //bgGeo = window.BackgroundGeolocation;

        //bgGeo.on('location', onLocation, onLocationError);
        //bgGeo.on('motionchange', onMotionChange);
        //bgGeo.on('geofence', onGeofence);
        //bgGeo.on('http', onHttpSuccess, onHttpError);
        //bgGeo.on('heartbeat', onHeartbeat);
        //bgGeo.on('schedule', onSchedule);
        //bgGeo.on('providerchange', onProviderChange);
        //bgGeo.on('activitychange', onActivityChange);
        //bgGeo.on('error', onError);
        //bgGeo.on('geofenceschange', onGeofencesChange);

        //// Ok, now #configure it!
        //bgGeo.configure(config, function (state) {
        //    console.info('- configure success: ', JSON.stringify(state));
        //    simpalTek.global.bgGeoConfigured = true;

        //    bgGeoState = state;

        //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- configure success: ' + JSON.stringify(state));

        //    // If configured with a Schedule, start the Scheduler now.
        //    //if (state.schedule) {
        //    //    bgGeo.startSchedule(function () {
        //    //        console.log('[js] Start schedule success');
        //    //    }, function (error) {
        //    //        console.warn('- FAILED TO START SCHEDULE: ', error);
        //    //    });
        //    //}

        //    //bgGeo.getCurrentPosition(function (location, taskId) {

        //    //        bgGeo.getGeofences(function (geofences) {

        //    //            try {
        //    //                for (var i = 0; i < geofences.length; i++) {

        //    //                    //if (activeGeofence.identifier == geofences[i].identifier) {
        //    //                    if (geoLocDataStore.exitDistance <= 250) {
        //    //                        geoLocDataStore.exitDistance = simpalTek.helper.geo().distance(geofences[i].latitude, geofences[i].longitude, location.coords.latitude, location.coords.longitude, 'F');

        //    //                        simpalTek.global.localStorage.set('GeoLocDataStore', geoLocDataStore);

        //    //                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'geoFenceLatLogDelta On Config Start - ' + geoLocDataStore.exitDistance);
        //    //                    }
        //    //                    //}
        //    //                }
        //    //            }
        //    //            catch (e) { simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "On Config Start Geofence Error: " + e.toString()) }


        //    //            bgGeo.finish(taskId);
        //    //        });

        //    //        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence activiated : " + JSON.stringify(activeGeofence))

        //    //    bgGeo.finish(taskId);
        //    //});

        //    //if (!state.enabled) {

        //    $rootScope.$broadcast("setUserGeoLoc");

        //    //bgGeo.start(function () {
        //    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation started');
        //    //});

        //    //bgGeo.startGeofences(function () {
        //    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- Geofence-only monitoring started');
        //    //})
        //    //}

        //    //$scope.$apply(function () {
        //    //    $scope.state.enabled = state.enabled;
        //    //    $scope.state.isMoving = state.isMoving;
        //    //});
        //}, function (error) {
        //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, '- configure failed: ' + JSON.stringify(error));
        //});

        // If BackgroundGeolocation is monitoring geofences, fetch them and add map-markers
        //bgGeo.getGeofences(function (rs) {
        //    //for (var n = 0, len = rs.length; n < len; n++) {
        //    //    createGeofenceMarker(rs[n]);
        //    //}
        //});

        //bgGeo.onGeofence(function (geofence, taskId) {
        //    try {
        //        var identifier = geofence.identifier;
        //        var action = geofence.action;
        //        var location = geofence.location;

        //        console.log("- A Geofence transition occurred");
        //        console.log("  identifier: ", identifier);
        //        console.log("  action: ", action);
        //        console.log("  location: ", JSON.stringify(location));

        //        window.cordova.plugins.notification.local.schedule({
        //            id: 1,
        //            title: 'NexxGarage Notification',
        //            text: 'Hi, Would you like to open your garage?',
        //            sound: "file://alexa-what-time-is-it-16bit-16hz-mono-pcm.wav",
        //            at: new Date(new Date().getTime() + 100),
        //            badge: 1,
        //            data: {
        //                customProperty: 'custom value'
        //            }
        //        })

        //    } catch (e) {
        //        console.error("An error occurred in my code!", e);
        //    }
        //    // Be sure to call #finish!!
        //    bgGeo.finish(taskId);
        //});



        //bgGeo.start(function () {
        //    console.log('[js] BackgroundGeolocation started');
        //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation started');

        //    // If BackgroundGeolocation is monitoring geofences, fetch them and add map-markers
        //    //bgGeo.getGeofences(function (rs) {
        //        //for (var n = 0, len = rs.length; n < len; n++) {
        //        //    createGeofenceMarker(rs[n]);
        //        //}
        //    //});

        //    //bgGeo.changePace(false, function () {
        //    //    //$scope.state.isMoving = willStart;
        //    //    //$scope.$apply(function () {
        //    //    //    $scope.state.startButtonIcon = (willStart) ? PAUSE_BUTTON_CLASS : PLAY_BUTTON_CLASS;
        //    //    //});
        //    //});
        //});


    }

    function configureBackgroundFetch() {
        //var config = Settings.getConfig();
        var config = simpalTek.global.bgGeoConfig;

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'configureBackgroundFetch');

        var Fetcher = window.BackgroundFetch;
        // Your background-fetch handler.
        var fetchCallback = function () {
            console.log('[js] BackgroundFetch initiated');
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundFetch INITIATED');

            Fetcher.finish();
        }

        var failureCallback = function () {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- BackgroundFetch failed');
        };

        Fetcher.configure(fetchCallback, failureCallback, {
            stopOnTerminate: config.stopOnTerminate
        });
    }
    /**
    * Platform is ready.  Boot the Home screen
    */
    function onPlatformReady() {

        //// Configure BackgroundGeolocation
        //if (!window.BackgroundGeolocation) {
        //    console.warn('Could not detect BackgroundGeolocation API');
        //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.warning, 'Could not detect BackgroundGeolocation API');
        //    return;
        //}
        //if (window.BackgroundFetch) {
        //    configureBackgroundFetch();
        //}
        //configureBackgroundGeolocation();

    }

    //var getCurrentPosition = function () {
    //    if (!bgGeo) { return; }
    //    //bgGeo.getCurrentPosition(function (location, taskId) {
    //    //    console.info('[js] getCurrentPosition: ', JSON.stringify(location));
    //    //    //centerOnMe(location);
    //    //    bgGeo.finish(taskId);
    //    //}, function (error) {
    //    //    console.warn('[js] getCurrentPosition error: ', error);
    //    //}, {
    //    //    timeout: 10,
    //    //    extras: { name: 'getCurrentPosition' }
    //    //})

    //    if (!currentPositionCache) {
    //        var deffered = $q.defer();

    //        bgGeo.getCurrentPosition(function (position, taskId) {

    //            console.info('[js] getCurrentPosition: ', JSON.stringify(position));

    //            bgGeo.finish(taskId);

    //            deffered.resolve(currentPositionCache = position);
    //            $interval(function () {
    //                currentPositionCache = undefined;
    //            }, 10000, 1);
    //        }, function (error) {
    //            console.warn('[js] getCurrentPosition error: ', error)
    //            deffered.reject(error);
    //        }, { timeout: 10000, enableHighAccuracy: true, name: 'getCurrentPosition' });

    //        return deffered.promise;
    //    }

    //    return $q.when(currentPositionCache);

    //}

    var geofenceService = {
        _geofences: [],
        _geofencesPromise: null,
        _currentSpeed: 0,

        getState: function () { return bgGeoState },

        create: function (attributes) {
            var defaultGeofence = {
                id: UUIDjs.create().toString(),
                latitude: 50,
                longitude: 50,
                radius: 100,
                transitionType: TransitionType.ENTER,
                //transitionType: 1,
                notification: {
                    id: this.getNextNotificationId(),
                    title: "Ionic geofence example",
                    text: "",
                    icon: "res://ic_menu_mylocation",
                    openAppOnClick: true
                }
            };

            return angular.extend(defaultGeofence, attributes);
        },

        init: function () {
            // Add BackgroundGeolocationService event-listeners when Platform is ready.
            ionic.Platform.ready(onPlatformReady);
        },

        start: function () {


            if (window.BackgroundGeolocation) {
                // Add BackgroundGeolocationService event-listeners when Platform is ready.
                ionic.Platform.ready(function () {
                    
                    window.BackgroundGeolocation.destroyLocations(function () { simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "database cleared location"); });

                    BackgroundGeolocation.getCount(function (count) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Location count: " + count);
                    });

                    BackgroundGeolocation.getState(function (state) {

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Geofence plugin enabled : ' + state.enabled);

                        if (state.enabled) {
                            self.config(simpalTek.global.bgGeoConfig);
                        }
                        else {
                            BackgroundGeolocation.startGeofences(function () {
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- Geofence-only monitoring started');

                                BackgroundGeolocation.getGeofences(function (geofences) {
                                    for (var n = 0, len = geofences.length; n < len; n++) {
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence: " + geofences[n].identifier + " | " + geofences[n].radius + " | " + geofences[n].latitude + " | " + geofences[n].longitude);
                                    }
                                }, function (error) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Failed to fetch geofences from server");
                                });

                            })
                        }
                    });

                                      


                });
            }

            //bgGeo.start(function () {
            //    console.log('[js] BackgroundGeolocation started');

            //    // If BackgroundGeolocation is monitoring geofences, fetch them and add map-markers
            //    //bgGeo.getGeofences(function (rs) {
            //    //    for (var n = 0, len = rs.length; n < len; n++) {
            //    //        createGeofenceMarker(rs[n]);
            //    //    }
            //    //});
            //});
        },

        stop: function () {

            if (bgGeo) {
                bgGeo.stop(function () {
                    console.info('[js] BackgroundGeolocation stopped');
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation stopped');
                    //bgGeo.changePace(false, function () {
                    //    //$scope.state.isMoving = willStart;
                    //    //$scope.$apply(function () {
                    //    //    $scope.state.startButtonIcon = (willStart) ? PAUSE_BUTTON_CLASS : PLAY_BUTTON_CLASS;
                    //    //});
                    //});

                });
            }
        },

        config: function (config) {

            //ionic.Platform.ready(function () {
            //    if (bgGeo) {
            //        bgGeo.on('location', onLocation, onLocationError);
            //        bgGeo.on('motionchange', onMotionChange);
            //        bgGeo.on('geofence', onGeofence);
            //        bgGeo.on('http', onHttpSuccess, onHttpError);
            //        bgGeo.on('heartbeat', onHeartbeat);
            //        bgGeo.on('schedule', onSchedule);
            //        bgGeo.on('providerchange', onProviderChange);
            //        bgGeo.on('activitychange', onActivityChange);
            //        bgGeo.on('error', onError);
            //        bgGeo.on('geofenceschange', onGeofencesChange);

            //        bgGeo.configure(config, function (state) {
            //            console.info('- configure success: ', JSON.stringify(state));
            //            simpalTek.global.bgGeoConfigured = true;

            //            bgGeoState = state;

            //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- configure success: ' + JSON.stringify(state));

            //        });
            //    }
                
            //});
        },

        restart: function () {
            var self = this;
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation restarted');

            if (bgGeo) {
                bgGeo.stop(function () {
                    //console.info('[js] BackgroundGeolocation stopped');
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation stopped');

                    window.setTimeout(function () {
                        //self.init();
                        self.start();
                    }, 3000)
                    

                });
            }
        },

        resetOdometer: function () {
            // Reset the odometer.
            bgGeo.resetOdometer(function () {
                //$scope.$apply(function () {
                //    $scope.odometer = 0;
                //});
            });
        },

        loadFromLocalStorage: function () {
            var result = localStorage["geofences"];
            var geofences = [];

            if (result) {
                try {
                    geofences = angular.fromJson(result);
                } catch (ex) {

                }
            }
            this._geofences = geofences;

            return $q.when(this._geofences);
        },

        saveToLocalStorage: function () {
            localStorage["geofences"] = angular.toJson(this._geofences);
        },

        loadFromDevice: function () {
            var self = this;

            if (bgGeo) {
                bgGeo.getGeofences(function (rs) {
                    for (var n = 0, len = rs.length; n < len; n++) {
                        //createGeofenceMarker(rs[n]);
                        self._geofences = angular.fromJson(rs[n]);
                        return self._geofences;
                    }
                });
            }

            return this.loadFromLocalStorage();
        },

        getAll: function () {
            var self = this;

            if (!self._geofencesPromise) {
                self._geofencesPromise = $q.defer();
                //self.loadFromDevice().then(function (geofences) {
                self.loadFromLocalStorage().then(function (geofences) {
                    self._geofences = geofences;
                    self._geofencesPromise.resolve(geofences);
                }, function (reason) {
                    $log.error("Error fetching geofences", reason);
                    self._geofencesPromise.reject(reason);
                });
            }

            return self._geofencesPromise.promise;
        },

        addOrUpdate: function (geofence) {
            var self = this;

            //return $window.geofence.addOrUpdate(geofence).then(function () {
            var searched = self.findById(geofence.identifier);

            if (!searched) {
                self._geofences.push(geofence);

                if (bgGeo) {

                    if (!addOrUpdateCache) {
                        var deffered = $q.defer();

                        bgGeo.addGeofence(geofence, function () {
                            //bgGeo.playSound(Settings.getSoundId('ADD_GEOFENCE'));

                            //self.stop();

                            //restart in 1 sec
                            //setTimeout(function () { self.start(); }, 1000);

                            deffered.resolve(addOrUpdateCache = geofence);
                            $interval(function () {
                                addOrUpdateCache = undefined;
                            }, 10000, 1);

                            //alert("GeoFence Added " + JSON.stringify(geofence))
                            //createGeofenceMarker($scope.geofenceRecord);
                        }, function (error) {
                            console.error(error);
                            //alert("Failed to add geofence: " + error);
                            deffered.reject(error);
                        });

                        return deffered.promise;
                    }
                }

            } else {

                var index = self._geofences.indexOf(searched);

                self._geofences[index] = geofence;

                if (bgGeo) {

                    if (!addOrUpdateCache) {
                        var deffered = $q.defer();

                        bgGeo.removeGeofence(geofence.identifier, function () {
                            //alert("Geofence removed " + geofence.identifier)
                            console.log("Successfully removed geofence");

                            bgGeo.addGeofence(geofence, function () {
                                //bgGeo.playSound(Settings.getSoundId('ADD_GEOFENCE'));

                                self.stop();

                                //restart in 1 sec
                                setTimeout(function () { self.start(); }, 1000);

                                deffered.resolve(addOrUpdateCache = geofence);
                                $interval(function () {
                                    addOrUpdateCache = undefined;
                                }, 10000, 1);

                                //alert("GeoFence Added " + JSON.stringify(geofence))
                                //createGeofenceMarker($scope.geofenceRecord);
                            }, function (error) {
                                console.error(error);
                                //alert("Failed to add geofence: " + error);
                                deffered.reject(error);
                            });

                        }, function (error) {
                            //alert("Error removing Geofence")
                            console.warn("Failed to remove geofence", error);
                            deffered.reject(error);
                        });

                        return deffered.promise;
                    }


                }
            }

            //if (bgGeo) {
            //    bgGeo.addGeofence(geofence, function () {
            //        bgGeo.playSound(Settings.getSoundId('ADD_GEOFENCE'));

            //        self.stop();

            //        //restart in 1 sec
            //        setTimeout(function () { self.start(); }, 1000);


            //        alert("GeoFence Added " + JSON.stringify(geofence))
            //        //createGeofenceMarker($scope.geofenceRecord);
            //    }, function (error) {
            //        console.error(error);
            //        alert("Failed to add geofence: " + error);
            //    });
            //}

            self.saveToLocalStorage();

            return $q.when(addOrUpdateCache);
            //});
        },

        findById: function (id) {
            var geoFences = this._geofences.filter(function (g) {
                return g.id === id;
            });

            if (geoFences.length > 0) {
                return geoFences[0];
            }

            return undefined;
        },

        remove: function (geofence) {
            var self = this;

            $ionicLoading.show({
                template: "Removing geofence..."
            });

            if (bgGeo) {
                //bgGeo.removeGeofence(geofence.identifier);

                bgGeo.removeGeofence(geofence.identifier, function () {
                    console.log("Successfully removed geofence");
                }, function (error) {
                    console.warn("Failed to remove geofence", error);
                });

            }

            //$window.geofence.remove(geofence.id).then(function () {
            $ionicLoading.hide();
            self._geofences.splice(self._geofences.indexOf(geofence), 1);
            self.saveToLocalStorage();
            //}, function (reason) {
            //    $log.error("Error while removing geofence", reason);
            //    $ionicLoading.show({
            //        template: "Error while removing geofence",
            //        duration: 1500
            //    });
            //});
        },

        removeAll: function () {
            if (bgGeo) {
                bgGeo.getGeofences(function (rs) {
                    for (var n = 0, len = rs.length; n < len; n++) {

                        bgGeo.removeGeofence(rs[n].identifier, function () {
                            console.log("Successfully removed geofence");
                        }, function (error) {
                            console.warn("Failed to remove geofence", error);
                        });
                    }
                });
            }
        },

        getNextNotificationId: function () {
            var max = 0;

            this._geofences.forEach(function (gf) {
                if (gf.notification && gf.notification.id) {
                    if (gf.notification.id > max) {
                        max = gf.notification.id;
                    }
                }
            });

            return max + 1;
        },

        getCurrentPosition: function () {
            if (bgGeo) {
                if (!currentPositionCache) {
                    var deffered = $q.defer();

                    bgGeo.getCurrentPosition(function (position, taskId) {

                        console.info('[js] getCurrentPosition: ', JSON.stringify(position));

                        bgGeo.finish(taskId);

                        deffered.resolve(currentPositionCache = position);
                        $interval(function () {
                            currentPositionCache = undefined;
                        }, 10000, 1);

                    }, function (error) {
                        console.warn('[js] getCurrentPosition error: ', error)
                        deffered.reject(error);
                    }, { timeout: 10000, enableHighAccuracy: true, name: 'getCurrentPosition', desiredAccuracy: 0, samples: 5, persist: false });

                    return deffered.promise;
                }
            }
            else {
                if (!currentPositionCache) {
                    var deffered = $q.defer();

                    navigator.geolocation.getCurrentPosition(function (position) {
                        deffered.resolve(currentPositionCache = position);
                        $interval(function () {
                            currentPositionCache = undefined;
                        }, 10000, 1);
                    }, function (error) {
                        deffered.reject(error);
                    }, { timeout: 10000, enableHighAccuracy: true });

                    return deffered.promise;
                }

                //return $q.when(currentPositionCache);
            }

            return $q.when(currentPositionCache);

        }
    };


    return geofenceService;
});
