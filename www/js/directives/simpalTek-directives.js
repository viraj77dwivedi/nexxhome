﻿
var moduleName = "simpalGate.directives";

angular.module(moduleName, [])

.directive('fadeBar', function ($timeout) {
    return {
        restrict: 'E',
        template: '<div class="fade-bar"></div>',
        replace: true,
        link: function ($scope, $element, $attr) {

            $timeout(function () {
                $scope.$watch('sideMenuController.getOpenRatio()', function (ratio) {
                    $element[0].style.opacity = Math.abs(ratio);
                });
            });
        }
    }
})

.directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
})
.directive('maintenanceMessage', function ($timeout) {

    var maint = TMKmobile.Global.localStorage.get('maintenanceData');

    if (maint != null) {
        var directiveTemplate = '<div class="maint-msg">';
        directiveTemplate += '   <div class="alert alert-warning">';
        directiveTemplate += '            <a class="close" ng-click="closeMaintenanceMessage()" data-dismiss="alert" aria-hidden="true">&times;</a>'
        directiveTemplate += maint.Message;
        directiveTemplate += '        </div>';
        directiveTemplate += '    </div>';
    }

    return {
        restrict: 'E',
        template: directiveTemplate,
        replace: true,
        controller: function ($scope) {
            $scope.closeMaintenanceMessage = function () {
                $(".maint-msg").hide();
                TMKmobile.Global.localStorage.remove('maintenanceData');
                maint = null;
            }

            if (maint != null) {
                $timeout(function () {
                    $(".maint-msg").hide("slow");
                    TMKmobile.Global.localStorage.remove('maintenanceData');
                }, maint.expiresIn)
            } else {
                $(".maint-msg").hide();
            }
        },
        link: function ($scope, $element, $attr) {


        }
    }
})

.directive('compareTo', function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };
            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    }
})

.directive('authorize', ['authService', 'data', function (authService, data) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs, ngModelCtrl) {

            if (!authService.authentication.isAuth) {
                authService.logOut();
            }

        }
    };
}])


.directive('formvalidator', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        require: ['formvalidator', '?form'],
        controller: ['$scope', function ($scope) {
            this.attempted = false;

            var formController = null;

            this.setAttempted = function () {
                this.attempted = true;
            };

            this.setFormController = function (controller) {
                formController = controller;
            };


            //this.needsAttention = function (fieldModelController) {
            //    if (!formController) return false;

            //    if (fieldModelController) {
            //        return fieldModelController.$invalid && (fieldModelController.$dirty || this.attempted);
            //    } else {
            //        return formController && formController.$invalid && (formController.$dirty || this.attempted);
            //    }
            //};
        }],
        scope: {
            formvalidatorconfig: '=',
            formvalidatorapi: '=',
            formaction: '='
        },
        link: function (scope, elem, attr, ctrl) {
            scope.$watch('formvalidatorconfig', function (value) {
                if (value) {
                    $(elem).validate(value);
                    if (value.validateOnInit)
                        scope.formvalidatorapi.validate();
                }
            });

            scope.formvalidatorapi = {
                validate: function () {
                    $(elem).validate().form();
                },
                valid: function () {
                    alert(0)
                    //return $(elem).valid();
                }
            };
        },
        compile: function (cElement, cAttributes, transclude) {
            return {
                pre: function (scope, formElement, attributes, controllers) {
                    var submitController = controllers[0];
                    var formController = (controllers.length > 1) ? controllers[1] : null;

                    submitController.setFormController(formController);

                    scope.rc = scope.rc || {};
                    scope.rc[attributes.name] = submitController;
                },
                post: function (scope, formElement, attributes, controllers) {

                    var submitController = controllers[0];
                    var formController = (controllers.length > 1) ? controllers[1] : null;
                    var fn = $parse(attributes.formvalidator);

                    $(formElement).bind('submit', function (event) {
                        //submitController.setAttempted();
                        if (!scope.$$phase) scope.$apply();

                        scope.$apply(function () {
                            fn(scope, { $event: event });

                        });
                    });
                }
            };
        }
    };

}])

//.directive('backButton', TMKmobile.Bootstrap.backButton)
//.directive('backHomeButton', TMKmobile.Bootstrap.backHomeButton)
//.directive("submitButton", TMKmobile.Bootstrap.submitButton)
//.directive("listviewPolicy", TMKmobile.Bootstrap.listViewPolicy)
//.directive("listPayments", TMKmobile.Bootstrap.listPayments)
//.directive("pageFooter", TMKmobile.Bootstrap.pageFooter)

.directive('parentDirective', function () {
    return {
        controller: function ($scope) {
            this.scope = $scope;
        }
    };
})

.directive('pageHeader', function () {
    return {
        restrict: 'E',
        controller: function ($window) {
            $window.scrollTo(0, 0)
        }
    };
})

.directive('scrollTopView', function ($rootScope, $ionicScrollDelegate, $timeout) {
    return {
        restrict: 'A',
        //controller: function ($ionicScrollDelegate, $timeout) {
        //    angular.element(document).ready(function () {
        //        setTimeout(function () {
        //            $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
        //        }, 500);

        //    });
        //},
        link: function (scope, elem, attrs, ctrl) {
            angular.element(document).ready(function () {
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                }, 500);
            });
        }
    };
})

.directive('forceblur', function () {
    return {
        restrict: "A",
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (ctrl && ctrl.$viewChangeListeners) {
                ctrl.$viewChangeListeners.push(function () {
                    $(elem).blur();
                });
            }
        }
    };
})


;