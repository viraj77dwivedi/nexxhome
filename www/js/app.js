/**
 * Initialization of simpalGate app
 * @module simpalGate

 * @class simpalGate
 */

'use strict';

var simpalTek    = simpalTek || {};
simpalTek.global = simpalTek || {};
simpalTek.const  = simpalTek || {};

simpalTek.global.appName = "com.simpalTek.nexxGarage";
simpalTek.global.environment = 'stage'; //dev,stage,prod
simpalTek.global.deviceInfo = {};
simpalTek.global.bgGeo = null;
simpalTek.global.tempDeviceInfo = {};
simpalTek.global.appSettings = {};

simpalTek.global.appSettings.faqUrl = "http://nexxgarage.com/faq/";

simpalTek.global.bgGeoConfigured = false;
simpalTek.global.logger = {};
simpalTek.global.logger.log = [];

simpalTek.global.doorSignalSentAcknowledged = false;
simpalTek.global.doorStateChanged = false;
simpalTek.global.firmwareUpdating = false;
simpalTek.global.pushNotification = null;
simpalTek.global.isDataRefreshed = false;
simpalTek.global.gettingPreloadData = false;
simpalTek.global.userTimeZone = 3;
simpalTek.global.appReStarted = false;

simpalTek.global.bgGeoConfig = {};
simpalTek.global.bgGeoConfig.desiredAccuracy = 0;
simpalTek.global.bgGeoConfig.distanceFilter = 20;
simpalTek.global.bgGeoConfig.stopTimeout = 1; //5;
simpalTek.global.bgGeoConfig.stationaryRadius = 25;
simpalTek.global.bgGeoConfig.activityType = "AutomotiveNavigation";
simpalTek.global.bgGeoConfig.stopOnTerminate = false;
simpalTek.global.bgGeoConfig.startOnBoot = true;
simpalTek.global.bgGeoConfig.autoSync = false;
simpalTek.global.bgGeoConfig.stopDetectionDelay = 0;
simpalTek.global.bgGeoConfig.disableMotionActivityUpdates = false;
//simpalTek.global.bgGeoConfig.stopAfterElapsedMinutes = 5;
simpalTek.global.bgGeoConfig.activityRecognitionInterval = 10000;
simpalTek.global.bgGeoConfig.maxDaysToPersist = 1;
simpalTek.global.bgGeoConfig.maxRecordsToPersist = 1;
simpalTek.global.bgGeoConfig.logMaxDays = 1;
simpalTek.global.bgGeoConfig.geofenceInitialTriggerEntry = false;
simpalTek.global.bgGeoConfig.minimumActivityRecognitionConfidence = 90;
simpalTek.global.bgGeoConfig.geofenceProximityRadius = 50000;  //about 30 miles

simpalTek.global.bgGeoConfig.foregroundService = true;
simpalTek.global.bgGeoConfig.notificationTitle = "Nexx Garage";
simpalTek.global.bgGeoConfig.notificationText = "Auto Open Activated";
//simpalTek.global.bgGeoConfig.forceReloadOnMotionChange = true;
//simpalTek.global.bgGeoConfig.forceReloadOnGeofence = true;
//simpalTek.global.bgGeoConfig.forceReloadOnLocationChange = true;
simpalTek.global.bgGeoConfig.locationUpdateInterval = 1000;
simpalTek.global.bgGeoConfig.fastestLocationUpdateInterval = 500;
simpalTek.global.bgGeoConfig.triggerActivities = 'in_vehicle, on_bicycle, running, walking, on_foot';
//simpalTek.global.bgGeoConfig.locationAuthorizationAlert = {
//        titleWhenNotEnabled: "Oops, Your location-services not enabled",
//        titleWhenOff: "Oops, Your location-services OFF",
//        instructions: "You must enable 'Always' in location-services.",
//        cancelButton: "Cancel",
//        settingsButton: "Settings"
//}

//simpalTek.global.deviceInfo.platform = "";
//simpalTek.global.deviceInfo.version = "";
//simpalTek.global.deviceInfo.uuid = "";
//simpalTek.global.deviceInfo.cordovaVersion = "";
//simpalTek.global.deviceInfo.model = "";

simpalTek.const.NO_INTERNET_MSG = "Uh-oh, We could not connect to the internet.  Pull the screen down to fresh the data.";
simpalTek.const.CURRENT_DATA_BEING_RETRIEVED_MSG = "Your current status is being retreived.  You can also pull down the screen to get the status immediately if this is taking some time.";
simpalTek.const.PERLOAD_RETRIEVE_ERROR_MSG = "There was a problem trying to update your current information.  Pull the screen down to retry freshing your information.";
simpalTek.const.ACCESS_EXPIRE_MSG = "Your access has expired, please re-login.";
simpalTek.const.REGISTERED_DEVICE_OFFLINE_MSG = "Alert!  Your Nexx Garage device is currently offline.";

simpalTek.global.localStorageAlternative = function () {

    var structLocalStorage = {};

    var setItem = function (key, value) {
        structLocalStorage[key] = value;
    }

    var getItem = function (key) {
        if (typeof structLocalStorage[key] != 'undefined'){
            return structLocalStorage[key];
        }else{
            return null;
        }
    }

    var removeItem = function (key) {
        structLocalStorage[key] = undefined;
    }

    structLocalStorage.setItem = setItem;
    structLocalStorage.getItem = getItem;
    structLocalStorage.removeItem = removeItem;

    return structLocalStorage;
}();

simpalTek.global.localStorageCookie = function () {

    var cookieLocalStorage = {};

    var setItem = function (key, value) {
        //console.log("set - key:" + key + " value:" + value);
        //console.log(JSON.stringify(value));

        if (typeof (value) == "object")
            $.cookie(key, JSON.stringify(value));
        else
            $.cookie(key, value);
    }

    var getItem = function (key) {
        //console.log("get - key:" + key + " value:" + $.cookie(key));
        //console.log(JSON.stringify($.cookie(key)));
        if (typeof $.cookie(key) == 'undefined')
            return null;
        else
            return $.cookie(key);
    }

    var removeItem = function (key) {
        $.removeCookie(key);
    }

    cookieLocalStorage.setItem = setItem;
    cookieLocalStorage.getItem = getItem;
    cookieLocalStorage.removeItem = removeItem;

    return cookieLocalStorage;
}();

simpalTek.global.localStorage = function () {

    var localStorageFactory = {};
    var storageImpl = null;
    var localStorageAllowed = true;

    try {
        localStorage.setItem("test_localStorage", "");
        localStorage.removeItem("test_localStorage");

        storageImpl = localStorage;
    }
    catch (e) {

        var cookieEnable = navigator.cookieEnabled;

        if (typeof navigator.cookieEnabled === 'undefined' && !cookieEnable) {
            document.cookie = 'cookie-test';
            cookieEnable = (document.cookie.indexOf('cookie-test') !== -1);
        }

        if (cookieEnable) {
            localStorageAllowed = true;
            storageImpl = simpalTek.global.localStorageCookie;
        }else{
            localStorageAllowed = false;
            storageImpl = simpalTek.global.localStorageAlternative;
        }

    }

    localStorageFactory.set = function (key, value) {
        if (localStorageAllowed) {

            if (typeof (value) == "object")
                storageImpl.setItem(key, JSON.stringify(value));
            else
                storageImpl.setItem(key, value);
            }
            else
                storageImpl.setItem(key, value);
    }


    localStorageFactory.get = function (key) {
        var value = storageImpl.getItem(key);

        if (localStorageAllowed)
            return value && JSON.parse(value);
        else
            return value;
    }

    localStorageFactory.remove = function (key) {
        storageImpl.removeItem(key);
    }

    return localStorageFactory;
}()

/**
 * Angular app, and dependencies.
 * @property app
 */
var app = angular.module('simpalGate', ['ionic', 'ngCordova', 'simpalGate.controllers', 'simpalGate.services', 'simpalGate.directives', 'ngTagsInput', 'Scope.safeApply', 'ionic-modal-select', 'LocalStorageModule', 'MessageCenterModule', 'leaflet-directive', 'ionic.service.core', 'ionic.service.push', 'angular.filter', 'angular-carousel', 'circle.countdown', 'fileLogger', 'mgo-angular-wizard']);

/**
 * Module of services.
 * @property services
 */
var services = angular.module('simpalGate.services', []);

/**
 * Intial run of the aplication.
 * @method run
 */
app.run(['$ionicPlatform', 'authService', '$rootScope', '$interval',
    'localStorageService', '$window', '$document', '$ionicLoading',
    '$state', '$log', '$cordovaPush', 'ngAuthSettings', '$http', 'Geofence','$fileLogger',

    function ($ionicPlatform, authService, $rootScope, $interval,
        localStorageService, $window, $document, $ionicLoading,
        $state, $log, $cordovaPush, ngAuthSettings, $http, Geofence, $fileLogger
    ) {



          authService.fillAuthData();

          $rootScope.$on('$locationChangeStart', function (event, next, current) {
              $rootScope.navBgColor = '';
          });

          $ionicPlatform.ready(function() {
              var homeGpsLoc = null;
              var currentUpdate, lastUpdate;
              var iosUnregisterOption = {};

              $fileLogger.setStorageFilename('nexxGarageAppLog.txt');

              //var store = cordova.file.dataDirectory;
              //var path = cordova.file.applicationDirectory + "www/female_hello.mp3";

              //window.resolveLocalFileSystemURL(path,
              //function gotFile(fileEntry) {

              //    window.resolveLocalFileSystemURL(store,
              //        function onSuccess(dirEntry) {
              //            //alert(JSON.stringify(dirEntry));

              //            fileEntry.copyTo(dirEntry, 'female_hello.mp3',
              //                function () {
              //                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Copy file to " + dirEntry);
              //                },
              //                function () {
              //                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Copy file");
              //                });
              //        }, null);
              //}, null);


            //var deviceInfo = ionic.Platform.device();

            //angular.element(document).ready(function () {
            //    $("#installFirmwareLink").click(function () {
            //        var data = {};
            //        data.vendorDeviceId = simpalTek.global.localStorage.get("currentNexxGarage").vendorDeviceId;



            //        $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceFirmware', data).success(function (response) {

            //            simpalTek.helper.messageCenterService("The new firmware is in the process of being installed onto your device.  Check the <a href='#/app/aboutDevice'>About Device</a> page for the status.", simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown, 5000);
            //            simpalTek.global.firmwareUpdating = true;
            //            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Edit device user successful");

            //        }).error(function (err, status) {


            //        });

            //        if (window.cordova) {
            //            navigator.notification.confirm(
            //                'You are about to install an update to your Nexx Garage device.  Tap on  the "Install" button to continue.', // message
            //                 (function (buttonIndex) {

            //                     var data = {};

            //                     if (buttonIndex == 2) {
            //                         data.vendorDeviceId = simpalTek.global.localStorage.get("currentNexxGarage").vendorDeviceId;

            //                         $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceFirmware', data).success(function (response) {

            //                             simpalTek.helper.messageCenterService("The new firmware is in the process of being installed onto your device.  Check the <a href='#/app/aboutDevice'>About Device</a> page for the status.", simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown, 5000);
            //                             simpalTek.global.firmwareUpdating = true;

            //                         }).error(function (err, status) {


            //                         });
            //                     }

            //                 }),            // callback to invoke with index of button pressed
            //                'Install New Firmware',           // title
            //                ['Cancel', 'Install']     // buttonLabels
            //            );
            //        }
            //        return false;
            //    })
            //});

            //if (simpalTek.global.localStorage.get('authorizationData')) {
            //    Geofence.init();
            //}

            if (window.cordova) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (ionic.Platform.isAndroid()) {
                    window.addEventListener("native.hidekeyboard", function () {
                        //show stuff on keyboard hide
                        StatusBar.hide();
                        window.AndroidFullScreen.immersiveMode(false, false);
                    });
                }

                if (ionic.Platform.isAndroid()) {
                    ionic.Platform.fullScreen();
                    if (window.StatusBar) {

                        ionic.Platform.showStatusBar(false)
                    }
                }
                else {
                    window.StatusBar.styleDefault();
                    ionic.Platform.showStatusBar(true)
                }
            }

            //if (window.StatusBar) {
            //    StatusBar.hide();
            //    ionic.Platform.fullScreen();
            //}

            if (window.cordova) {

                if (screen)
                    screen.lockOrientation('portrait');

                 console.log(AppVersion.version); // e.g. "1.2.3"
                 console.log(AppVersion.build); // e.g. 1234

                $rootScope.appVersion = AppVersion.version;

                //navigator.notification.alert("hello", function () { }, "Alert Test", "Ok");

                //Geofence.start();

                //window.cordova.plugins.notification.local.registerPermission(function (granted) {
                //});

                // Get UUID

                window.plugins.uniqueDeviceID.get(function(uuid) {

                    if (uuid == "")
                        simpalTek.global.deviceInfo.uuid = ionic.Platform.device().uuid
                    else
                        simpalTek.global.deviceInfo.uuid = uuid;

                }, function () {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "uniqueDeviceID failed.");
                    simpalTek.global.deviceInfo.uuid = ionic.Platform.device().uuid;
                });

            }

        });

}])

.filter('trustAsResourceUrl', ['$sce', function($sce) {
  return function(val) {
      return $sce.trustAsResourceUrl(val);
  };
}])

.filter('utcToLocal', ['$filter', function ($filter) {
    return function (utcDateString, format) {
        // return if input date is null or undefined
        if (!utcDateString) {
            return;
        }

        // append 'Z' to the date string to indicate UTC time if the timezone isn't already specified
        if (utcDateString.indexOf('Z') === -1 && utcDateString.indexOf('+') === -1) {
            utcDateString += 'Z';
        }

        // convert and format date using the built in angularjs date filter
        return $filter('date')(utcDateString, format);
    };
}])
/**
 * Configurations of routes, uis, and controllers dependecies.
 * @method config
 */
.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, $ionicAppProvider) {

    //$ionicAppProvider.identify({
    //    app_id: '793f84e2',
    //    api_key: 'b14c2240ebdbb38c53de967c73dec43753a7833a5a888a2a',
    //    dev_push: true
    //});

    $httpProvider.interceptors.push('authInterceptorService');

    $provide.decorator('$exceptionHandler', ['$log', '$delegate', '$injector',
        function ($log, $delegate, $injector) {
            return function (exception, cause) {

                var httpSvc = $injector.get('$http');
                //var localStorageSvc = $injector.get('localStorageService');
                //var appConfigSvc = $injector.get('appConfig');
                var locSvc = $injector.get('$location');
                var $ionicLoading = $injector.get('$ionicLoading');
                var ngAuthSettings = $injector.get('ngAuthSettings');
                var authService = $injector.get('authService');
                var errorModel = {};
                var stackTrace = "";
                var appErrorCount = 0;

                errorModel.Severity = "Error";

                //try {
                //    stackTrace = String(stacktraceService.print({ e: exception }));
                //} catch (e) { }

                try {
                    //var appErrorCount = simpalTek.global.localStorage.get("tempData").appErrorCount;

                    //if (appErrorCount == undefined || appErrorCount == null)
                     //   appErrorCount = 0;




                    //bypass log if bgGeo task error
                    //if (exception.stack.indexOf("1st param, eg: bgGeo.finish(taskId)") < 0) {
                        if (appErrorCount < 3) {
                            errorModel.Message = exception.toString();

                            errorModel.StackTrace = "Page: " + locSvc.url() + " " + exception.stack;
                            //errorModel.AppInfo = angular.toJson(TMKmobile.Config.appInfo);

                            //httpSvc.post(TMKmobile.Config.apiServiceBase + "/api/Logger", errorModel);
                        //TMKmobile.Helper.logger().send(TMKmobile.Enum.errorSeverity.warning.name, JSON.stringify(errorModel));
                            //alert(JSON.stringify(errorModel))
                            $ionicLoading.hide();
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.critical, JSON.stringify(errorModel));

                            //if (authService.authentication.isAuth)
                            //    simpalTek.helper.sendMail2Support(errorModel, "Device Error");

                        }

                        appErrorCount++;

                    //}
                }
                      catch (loggingError) {
                    $log.warn("Error logging failed");
                    $log.log(loggingError);
                }

                //TMKmobile.Common.showLoader(false);

                $delegate(exception, cause);
            };
        }
    ]);

  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: "UserCtrl"
    })

      .state('app.initialSetup', {
          url: "/initialSetup",
          views: {
              'menuContent': {
                  templateUrl: "templates/initialSetup.html",
                  controller: 'InitialSetupCtrl'
              }
          }
      })


    .state('app.registeredDevices', {
        url: "/registeredDevices",
        views: {
            'menuContent' : {
                templateUrl: "templates/registeredDevices.html",
                controller: 'RegisteredDevicesCtrl'
            }
        }
    })
    .state('app.registerDeviceWizard', {
        url: "/registerDeviceWizard",
        views: {
            'menuContent': {
                templateUrl: "templates/registerDeviceWizard.html",
                controller: 'RegisterDeviceWizardCtrl'
            }
        }
    })

    .state('app.activityLog', {
        url: "/activityLog",
        views: {
            'menuContent' : {
                templateUrl: "templates/history.html",
                controller: 'ActivityLogCtrl'
            }
        }
    })

    .state('app.notificationSettings', {
        url: "/notificationSettings",
        views: {
            'menuContent' : {
                templateUrl: "templates/notificationSettings.html",
                controller: 'NotificationSettingsCtrl'
            }
        }
    })

    .state('app.userAccount', {
        url: "/userAccount",
        views: {
            'menuContent' : {
                templateUrl: "templates/userAccount.html",
                controller: 'UserAccountCtrl'
            }
        }
    })

    .state('app.garageDoorControl', {
      url: "/garageDoorControl",
      views: {
        'menuContent' :{
          templateUrl: "templates/garageDoorControl.html",
          controller: "GarageDoorControlCtrl"
        }
      }
    })

	.state('app.register', {
      url: "/register",
      views: {
        'menuContent' :{
          templateUrl: "templates/createAccount.html",
          controller: 'RegisterCtrl'
        }
      }
    })

      .state('signin', {
          url: '/signin',
          templateUrl: 'templates/login.html',
          controller: 'UserCtrl'
      })

      .state('resetPassword', {
          url: '/resetPassword',
          templateUrl: 'templates/resetPassword.html',
          controller: 'UserCtrl'
      })
    //.state('app.signin', {
    //    url: "/signin",
    //    views: {
    //        'menuContent' : {
    //            templateUrl: "templates/login.html",
    //            controller: 'UserCtrl'
    //        }
    //    }
    //})
    .state("app.geofences", {
        url: "/geofences",
        views: {
            'menuContent' : {
                templateUrl: "templates/geofences.html",
                controller: 'GeofencesCtrl'
            }
        }
    })
    .state("app.geofence-new", {
        url: "/geofence/new/:longitude,:latitude",
        views: {
            'menuContent' : {
                templateUrl: "templates/geofence.html",
                controller: 'GeofenceCtrl'
            }
        },

        resolve: {
            geofence: function ($stateParams, Geofence) {
                return Geofence.create({
                    longitude: parseFloat($stateParams.longitude),
                    latitude: parseFloat($stateParams.latitude)
                });
            }
        }
    })
    .state("app.geofence-edit", {
        url: "geofence/:geofenceId",
        views: {
            'menuContent' : {
                templateUrl: "templates/geofence.html",
                controller: 'GeofenceCtrl'
            }
        },

        resolve: {
            geofence: function ($stateParams, Geofence, $q, $rootScope) {
                //var geofence = Geofence.findById($stateParams.geofenceId);

                var geoFences = $rootScope.geofences.filter(function (g) {
                    return g.identifier === $stateParams.geofenceId;
                });

                if (geoFences.length > 0) {
                    return geoFences[0];
                }

                if (geofence) {
                    return $q.when(angular.copy(geofence));
                }

                return $q.reject("Cannot find geofence with id: " + $stateParams.geofenceId);
            }
        }
    })
    .state("app.push", {
        url: "/push",
        views: {
            'menuContent' : {
                templateUrl: "templates/push.html",
                controller: 'PushCtrl'
            }
        }
    })
    .state("app.geoSettings", {
        url: "/geoSettings",
        views: {
            'menuContent': {
                templateUrl: "templates/geosettings.html",
                controller: 'GeoSettingsCtrl'
            }
        }
    })
    .state("app.places", {
        url: "/places",
        views: {
            'menuContent': {
                templateUrl: "templates/places.html",
                controller: 'PlacesCtrl'
            }
        }
    })
    .state("app.deviceUserManager", {
        url: "/deviceUserManager",
        views: {
            'menuContent': {
                templateUrl: "templates/deviceUserManager.html",
                controller: 'DeviceUserManagerCtrl'
            }
        }
    })
    .state("app.aboutDevice", {
        url: "/aboutDevice",
        views: {
            'menuContent': {
                templateUrl: "templates/aboutDevice.html",
                controller: 'AboutDeviceCtrl'
            }
        }
    })
    //.state('getSettings', {
    //  url: '/getSettings',
    //  controller: 'GeoSettingsCtrl'
    //})
    //.state('settings', {
    //  url: '/settings',
    //  templateUrl: 'templates/geosettings/settings.html',
    //  controller: 'GeoSettingsCtrl',
    //  cache: false
    //})
    .state('app.geoSettingsDistanceFilter', {
      url: '/settings/distanceFilter',
      views: {
      'menuContent': {
          templateUrl: "templates/geosettings/radio-list.html",
          controller: 'GeoSettingsCtrl'
      }
  }
    })
    .state('app.geoSettingsStationaryRadius', {
      url: '/settings/stationaryRadius',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsDesiredAccuracy', {
        url: '/settings/desiredAccuracy',
        views: {
            'menuContent': {
                templateUrl: "templates/geosettings/radio-list.html",
                controller: 'GeoSettingsCtrl'
            }
        }
    })
    .state('app.geoSettingsActivityType', {
      url: '/settings/activityType',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsTriggerActivities', {
        url: '/settings/triggerActivities',
        views: {
            'menuContent': {
                templateUrl: "templates/geosettings/trigger-activities.html",
                controller: 'GeoSettingsCtrl'
            }
        }
    })
    .state('app.geoSettingsDisableElasticity', {
      url: '/settings/disableElasticity',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsUrl', {
        url: '/settings/url',
        views: {
            'menuContent': {
                templateUrl: "templates/geosettings/url.html",
                controller: 'GeoSettingsCtrl'
            }
        }
    })
    .state('app.geoSettingsMethod', {
      url: '/settings/method',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsAutoSync', {
      url: '/settings/autoSync',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsBatchSync', {
      url: '/settings/batchSync',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsLocationUpdateInterval', {
      url: '/settings/locationUpdateInterval',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsFastestLocationUpdateInterval', {
      url: '/settings/fastestLocationUpdateInterval',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsActivityRecognitionInterval', {
      url: '/settings/activityRecognitionInterval',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsStopDetectionDelay', {
      url: '/settings/stopDetectionDelay',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsStopTimeout', {
      url: '/settings/stopTimeout',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsStopOnTerminate', {
      url: '/settings/stopOnTerminate',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsForceReloadOnLocationChange', {
      url: '/settings/forceReloadOnLocationChange',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsForceReloadOnMotionChange', {
      url: '/settings/forceReloadOnMotionChange',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsForceReloadOnGeofence', {
      url: '/settings/forceReloadOnGeofence',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsStartOnBoot', {
      url: '/settings/startOnBoot',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsDebug', {
      url: '/settings/debug',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsPreventSuspend', {
      url: '/settings/preventSuspend',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsHeartbeatInterval', {
      url: '/settings/heartbeatInterval',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsForegroundService', {
      url: '/settings/foregroundService',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsDeferTime', {
      url: '/settings/deferTime',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsPausesLocationUpdatesAutomatically', {
      url: '/settings/pausesLocationUpdatesAutomatically',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsUseSignificantChangesOnly', {
      url: '/settings/useSignificantChangesOnly',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    })
    .state('app.geoSettingsDisableMotionActivityUpdates', {
      url: '/settings/disableMotionActivityUpdates',
      views: {
          'menuContent': {
              templateUrl: "templates/geosettings/radio-list.html",
              controller: 'GeoSettingsCtrl'
          }
      }
    });

  //if (ionic.Platform.isAndroid()) {
      //document.addEventListener("deviceready", function () {
          if (simpalTek.global.localStorage.get('authorizationData')) {
              $urlRouterProvider.otherwise('/app/garageDoorControl');
          }
          else
              $urlRouterProvider.otherwise('/signin');
  //    //}, false);

  //}
  //else
  //    $urlRouterProvider.otherwise('/signin');

});

var serviceBase = '';
var domainServiceBase = '';
var publicDomainServiceBase = '';
var apiDeviceBaseUri = 'http://192.168.1.1';

switch (simpalTek.global.environment) {
    case "dev":
        //serviceBase = 'http://localhost:53701';
        serviceBase = 'https://dev-identity-api.simpaltek.com';
        domainServiceBase = 'http://localhost:53693/api/Domain/SmartGATE';
        publicDomainServiceBase = 'http://localhost:53693/api/Public';
        simpalTek.global.bgGeoConfig.debug = true;
        simpalTek.global.bgGeoConfig.logLevel = 5;
        break;
    case "stage":
        serviceBase = 'https://dev-identity-api.simpaltek.com';
        domainServiceBase = 'https://dev-domain-api.simpaltek.com/api/Domain/SmartGATE';
        publicDomainServiceBase = 'https://domain-api.simpaltek.com/api/Public';
        simpalTek.global.bgGeoConfig.debug = false;
        simpalTek.global.bgGeoConfig.logLevel = 5;
        break;
    case "prod":
        serviceBase = 'https://identity-api.simpaltek.com';
        domainServiceBase = 'https://domain-api.simpaltek.com/api/Domain/SmartGATE';
        publicDomainServiceBase = 'https://domain-api.simpaltek.com/api/Public';
        simpalTek.global.bgGeoConfig.debug = false;
        simpalTek.global.bgGeoConfig.logLevel = 0;
        break;
}

//var serviceBase = 'http://localhost:53701';
//var domainServiceBase = 'http://localhost:53693/api/Domain/SmartGATE';
//var publicDomainServiceBase = 'http://localhost:53693/api/Public';

//var serviceBase = 'https://dev-identity-api.simpaltek.com';
//var domainServiceBase = 'https://dev-domain-api.simpaltek.com/api/Domain/SmartGATE';

//var serviceBase = 'https://identity-api.simpaltek.com';
//var domainServiceBase = 'https://domain-api.simpaltek.com/api/Domain/SmartGATE';
//var publicDomainServiceBase = 'https://domain-api.simpaltek.com/api/Public';

app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    apiDomainServiceBaseUri : domainServiceBase,
    apiDeviceBaseUri: apiDeviceBaseUri,
    apiPublicDomainServiceBaseUri: publicDomainServiceBase,
    clientId: '64c7ffd32e0e426b99a5513b40217c6f',
    tokenReminderMeIn : 60, //s
    sessionInterval : 1000,//ms
    errorLogThreshold : 1,
    messageCenterTimeOut : 0, //ms
    deviceSleepLapseIntervalThreshold : 5000, //ms,
});



function fail() {
        console.log("failed to get filesystem");
    }

function gotFS(fileSystem) {
	console.log("got filesystem");

		// save the file system for later access
	console.log(fileSystem.root.fullPath);
	window.rootFS = fileSystem.root;
}
