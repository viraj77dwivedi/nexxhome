﻿angular.module('retry_request', ['ng'])
  .factory('RetryRequest', ['$http', '$q', function ($http, $q) {
      return function (path) {
          var MAX_REQUESTS = 3,
              counter = 1,
              results = $q.defer();

          var request = function () {
              $http({ method: 'GET', url: path })
                .success(function (response) {
                    results.resolve(response)
                })
                .error(function () {
                    if (counter < MAX_REQUESTS) {
                        request();
                        counter++;
                    } else {
                        results.reject("Could not load after multiple tries");
                    }
                });
          };

          request();

          return results.promise;
      }
  }]);