app.controller("GeofencesCtrl", function (
    $scope,
    $ionicActionSheet,
    $timeout,
    $log,
    $state,
    Geolocation,
    Geofence,
    $ionicLoading,
    $location,
    $rootScope
) {
    $ionicLoading.show({
        template: "Getting geofences from device...",
        duration: 3000
    });

    $rootScope.geofences = [];

    document.addEventListener('deviceready', function () {
        if (window.BackgroundGeolocation) {
            window.BackgroundGeolocation.getGeofences(function (rs) {

                $rootScope.proximitySetupCount = rs.length;

                for (var n = 0, len = rs.length; n < len; n++) {
                    $rootScope.geofences.push(rs[n]);
                }
            });
        }
        else {
            Geofence.getAll().then(function (geofences) {
                $ionicLoading.hide();
                $rootScope.geofences = geofences;
            }, function (reason) {
                $ionicLoading.hide();
                $log.error("An Error has occured", reason);
            });
        }
    }, false);

    $scope.createNew = function () {
        $log.log("Obtaining current location...");
        $ionicLoading.show({
            template: "Obtaining current location...",
            hideOnStateChange: true
        });
        Geolocation.getCurrentPosition()
            .then(function (position) {
                $log.info("Current position found", position);

                $state.go("app.geofence-new", {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
                //$location.path('#/app/geofence/new/').search({ latitude: position.coords.latitude, longitude: position.coords.longitude});
                
                    
            }, function (reason) {
                $log.error("Cannot obtain current location", reason);
                $ionicLoading.show({
                    template: "Cannot obtain current location",
                    duration: 1500
                });
            });
    };

    $scope.editGeofence = function (geofence) {
        $state.go("app.geofence-edit", {
            geofenceId: geofence.identifier
        });
    };

    $scope.removeGeofence = function (geofence) {
        //Geofence.remove(geofence);
        
        if (window.BackgroundGeolocation) {

            //Geofence.stop();

            window.BackgroundGeolocation.removeGeofence(geofence.identifier, function () {
                $rootScope.geofences.splice($rootScope.geofences.indexOf(geofence), 1);

                window.BackgroundGeolocation.getGeofences(function (rs) {

                    $rootScope.proximitySetupCount = rs.length;

                });

                console.log("Geofence removed " + geofence.identifier);
       
            }, function (error) {
                //Geofence.start();
                console.warn("Failed to remove geofence", error);
            });

            
        }

    };

    $scope.more = function () {
        // Show the action sheet
        $ionicActionSheet.show({
            titleText: "More options",
            buttons: [
                { text: "<i class='icon ion-checkmark-circled'></i> Test application" }
            ],
            destructiveText: "<i class='icon ion-trash-b'></i> Delete all geofences",
            cancelText: "<i class='icon ion-android-cancel'></i> Cancel",
            destructiveButtonClicked: function () {
                Geofence.removeAll();
                return true;
            },
            buttonClicked: function() {
                window.location.href = "cdvtests/index.html";
            }
        });
    };
});
