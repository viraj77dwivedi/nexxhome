/**
 * Garage Door controller
 *
 * @module GarageDoorCtrl

 * @class simpalGate.controllers
 */

app.controller('GarageDoorControlCtrl', [

    '$scope',
    '$http',
    '$ionicModal',
    'ngAuthSettings',
    'localStorageService',
    '$rootScope',
    '$ionicPlatform',
    '$window',
    '$timeout',
    '$ionicPopup',
    '$location',
    'authService',
    '$filter',
    'Geofence',
    '$ionicSlideBoxDelegate',
    '$fileLogger',
    'messageCenterService',
    function ($scope, $http, $ionicModal, ngAuthSettings, localStorageService, $rootScope, $ionicPlatform, $window, $timeout, $ionicPopup, $location, authService, $filter, Geofence, $ionicSlideBoxDelegate, $fileLogger, messageCenterService) {

        var userDb = simpalTek.global.localStorage.get('UserDb');
        var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
        var data = {};
        var currentDate = new Date();
        var currentGarageDoorState = "";
        var authData = simpalTek.global.localStorage.get('authorizationData');
        var userAccessDevices = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });
        var lastDeviceActivity = [];
        var nameArray;

        $scope.lastUserActivity = "";
        $scope.userFirstName = authService.authentication.userFullName.split(" ")[0];
        simpalTek.lib.doorControlAnimation.scope = $scope;
        $scope.doorControlAnimationIsRunning = simpalTek.lib.doorControlAnimation.isRunning;

        //if (userDb.preference) {
        //    if (userDb.preference.proximityMonitorAuto) {

        //        // Add BackgroundGeolocationService event-listeners when Platform is ready.
        //        if (simpalTek.global.bgGeoConfigured == false)
        //            simpalTek.helper.geoFence();


        //    }
        //}

        //$ionicPlatform.ready(function () {
        //    if (window.cordova) {
        //        WifiWizard.getCurrentSSID(function (ssid) {
        //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);
        //        }, function (e) {
        //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
        //        });

        //        WifiWizard.listNetworks(function (networks) {
        //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WiFi network " + JSON.stringify(networks));
        //        }, function (e) {
        //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard WiFi network error " + JSON.stringify(e));
        //        });

        //    }
        //});

        if (!currentNexxGarage)
            currentNexxGarage = {};

        //if (!socket.connected)
        //    socket.emit('join', { email: authData.userName });

        if (userDb.registeredDevices.length == 0 && currentNexxGarage == null && userDb.devicesAccess.length == 0)
        {
            $scope.accessDevices = [];
            $location.path('/app/initialSetup');
            return;
        }

        if (userAccessDevices.length > 0) {
            if (userDb.gateActivities)
                lastDeviceActivity = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == userAccessDevices[0].vendorDeviceId });
        }

        GetAccessDevices();

        $scope.slideHasChanged = function (index) {

            console.log("slideHasChanged index " + index);

            userDb = simpalTek.global.localStorage.get('UserDb');
            $scope.lastUserActivity = "";

            if (userAccessDevices[index].userType == "O")
                $rootScope.navBgColor = '';
            else
                $rootScope.navBgColor = '#FF9009';

            currentNexxGarage.vendorDeviceId = userAccessDevices[index].vendorDeviceId;
            currentNexxGarage.deviceId = userAccessDevices[index].deviceId;
            currentNexxGarage.garageDoorState = userAccessDevices[index].currentDoorState;

            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);

            lastDeviceActivity = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == userAccessDevices[index].vendorDeviceId });;

            simpalTek.lib.doorControlAnimation.stopAnimation();
            simpalTek.lib.doorControlAnimation.currentState(simpalTek.global.localStorage.get("currentNexxGarage").garageDoorState);

            //$scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
            if (lastDeviceActivity.length > 0) {

                nameArray = lastDeviceActivity[0].userFullName.split("-");

                if (nameArray.length > 1) {
                    if (nameArray[0] == "A")
                        $scope.lastUserActivity = "Last auto " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + nameArray[1];
                    else
                        $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
                }
                else
                    $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
            }

            setCurrentGarageState();

        }

        $scope.doRefresh = function () {
            simpalTek.helper.getUserPreloadData(function () {
                userDb = simpalTek.global.localStorage.get('UserDb');

                $timeout(function () {
                    $rootScope.$broadcast('loadCurrentNexxGarageState');
                });

                //Stop the ion-refresher from spinning
                $rootScope.$broadcast('scroll.refreshComplete');


            },
            function () {

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "getUserPreloadData UserPreloadData Error");

                //Stop the ion-refresher from spinning
                $rootScope.$broadcast('scroll.refreshComplete');

            });
        }

        $scope.repeatDone = function () {

            $timeout(function () {
                $ionicSlideBoxDelegate.update();
            });

        };


        if (userDb != null) {
            if (userDb.gateActivities != null) {
                //if (lastDeviceActivity.length > 0)
                    //$scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;

                if (lastDeviceActivity.length > 0) {

                    nameArray = lastDeviceActivity[0].userFullName.split("-");

                    if (nameArray.length > 1) {
                        if (nameArray[0] == "A")
                            $scope.lastUserActivity = "Last auto " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + nameArray[1];
                        else
                            $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
                    }
                    else
                        $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
                }
            }
        }

        $scope.garageSignalComplete = true;
        //$scope.nexxGarageTitle = '<img class="title-image" src="img/logo.png" />';

        var setCurrentGarageState = function () {
            currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");

            if (currentNexxGarage) {

                var registerDevice = userDb.registeredDevices.filter(function (x) { return x.deviceId == currentNexxGarage.deviceId})

                if (registerDevice.length > 0)
                {
                    if (registerDevice[0].isOnline == false) {
                        simpalTek.helper.messageCenterService(simpalTek.const.REGISTERED_DEVICE_OFFLINE_MSG, simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown);
                        simpalTek.lib.doorControlAnimation.currentState("Offline");
                    }
                    else
                    {
                        if (messageCenterService.mcMessages.length > 0) {
                            if (messageCenterService.mcMessages[0].message == simpalTek.const.REGISTERED_DEVICE_OFFLINE_MSG)
                                messageCenterService.remove();

                        }
                        if (simpalTek.lib.doorControlAnimation.isRunning() == false)
                            simpalTek.lib.doorControlAnimation.currentState((currentNexxGarage.garageDoorState) ? currentNexxGarage.garageDoorState : 'Open');
                    }
                }
                else {
                    if (simpalTek.lib.doorControlAnimation.isRunning() == false)
                        simpalTek.lib.doorControlAnimation.currentState((currentNexxGarage.garageDoorState) ? currentNexxGarage.garageDoorState : 'Open');
                }

                //$scope.currentGarageState = "";
                $scope.doorControlAnimationIsRunning = simpalTek.lib.doorControlAnimation.isRunning();


                switch (currentNexxGarage.garageDoorState) {
                    case "Close":
                        $scope.currentGarageState = "Closed";
                        $scope.controlButtonText = "Open";
                        data.ActivityType = 2;

                        //if (simpalTek.lib.doorControlAnimation.isRunning() == false)
                        //    simpalTek.lib.doorControlAnimation.currentState("Close");

                        break;
                    case "Open":
                        $scope.currentGarageState = "Opened";
                        $scope.controlButtonText = "Close";
                        data.ActivityType = 1;

                        break;
                    default:
                        $scope.currentGarageState = currentNexxGarage.garageDoorState
                        $scope.controlButtonText = "Unknown";
                        data.ActivityType = 3;
                }


                if (currentNexxGarage.garageDoorState == "Close")
                    data.ActivityType = 2;
            }
        }

        //$scope.currentDateTime = currentDate.toDateString() + " @ " + currentDate.toLocaleTimeString();


        //if (userDb == null) {
        //    data.ActivityType = 2; //defalut closed
        //    $scope.currentState = "Close";
        //} else {

            //if (userDb.registeredDevices.length > 0)
            //    data.SmartGateDeviceID = userDb.registeredDevices[0].deviceName;
            //else

            if (currentNexxGarage)
                data.SmartGateDeviceID = currentNexxGarage.vendorDeviceId;

            setCurrentGarageState();

            $scope.$on('loadCurrentNexxGarageState', function (event, args) {
                var userDb = simpalTek.global.localStorage.get('UserDb');
                var $injector = angular.element(document.body).injector();
                var authService = $injector.get('authService');
                authData = simpalTek.global.localStorage.get('authorizationData');

                try{
                    userAccessDevices = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });

                    if (userAccessDevices.length > 0) {
                        try{
                            lastDeviceActivity = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == userAccessDevices[$ionicSlideBoxDelegate.currentIndex()].vendorDeviceId });
                        }
                        catch (e) {
                            simpalTek.helper.sendMail("techlogs@simpaltek.com", e, "Error on loadCurrentNexxGarageState authService :" + JSON.stringify(authService));
                        }
                    }

                    //$scope.accessDevices = userAccessDevices;
                    GetAccessDevices();

                    if (!currentNexxGarage)
                        currentNexxGarage = {};

                    if (userAccessDevices.length > 0) {
                        currentNexxGarage.vendorDeviceId = userAccessDevices[$ionicSlideBoxDelegate.currentIndex()].vendorDeviceId;
                        currentNexxGarage.deviceId = userAccessDevices[$ionicSlideBoxDelegate.currentIndex()].deviceId;
                        currentNexxGarage.garageDoorState = userAccessDevices[$ionicSlideBoxDelegate.currentIndex()].currentDoorState;

                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);

                        lastDeviceActivity = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == userAccessDevices[$ionicSlideBoxDelegate.currentIndex()].vendorDeviceId });;

                        simpalTek.lib.doorControlAnimation.currentState(currentNexxGarage.garageDoorState);

                    }

                    if (lastDeviceActivity.length > 0) {

                        nameArray = lastDeviceActivity[0].userFullName.split("-");

                        if (nameArray.length > 1) {
                            if (nameArray[0] == "A")
                                $scope.lastUserActivity = "Last auto " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + nameArray[1];
                            else
                                $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
                        }
                        else
                            $scope.lastUserActivity = "Last " + ((lastDeviceActivity[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(lastDeviceActivity[0].activityDate, "h:mm:ss a") + " by " + lastDeviceActivity[0].userFullName;
                    }

                    //$scope.lastUserActivity = "Last " + ((userDb.gateActivities.filter(function (item) { return item.deviceId == $scope.accessDevices[$ionicSlideBoxDelegate.currentIndex()].deviceId })[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(userDb.gateActivities[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(userDb.gateActivities[0].activityDate, "h:mm:ss a") + " by " + userDb.gateActivities[0].userFullName;
                    setCurrentGarageState();

                    //if (userDb.gateActivities.length > 0)
                    //    $scope.lastUserActivity = "Last " + ((userDb.gateActivities[0].activityType == "Open") ? "opened" : "closed") + " on " + $filter('utcToLocal')(userDb.gateActivities[0].activityDate, "EEEE MMMM d, y") + " at " + $filter('utcToLocal')(userDb.gateActivities[0].activityDate, "h:mm:ss a") + " by " + userDb.gateActivities[0].userFullName;

                    $timeout(function () {
                        $ionicSlideBoxDelegate.update();

                        $scope.$safeApply();
                    });
                } catch (e) {
                    simpalTek.helper.sendMail("techlogs@simpaltek.com", e, "Error on loadCurrentNexxGarageState");
                }

            });

        //}



        $scope.record = function () {

            cordova.plugins.notification.local.schedule({
                id         : 1,
                title      : 'I will bother you every minute',
                text       : '.. until you cancel all notifications',
                sound      : 'file://alexa-what-time-is-it-16bit-16hz-mono-pcm.wav',
                every      : 'minute',
                autoClear  : true,
                at         : new Date(new Date().getTime() + 10 * 1000),
                ongoing    : true,
                badge      : 1
            });

            //var recognition = new SpeechRecognition();
            //recognition.onresult = function (event) {
            //    if (event.results.length > 0) {
            //        $scope.recognizedText = event.results[0][0].transcript;
            //        $scope.$apply()
            //    }
            //};
            //recognition.start();
        };

        $scope.cancelNotification = function () {
            // cancel a single notification by passing the ID
            cordova.plugins.notification.local.cancel(1,
                function () {
                    alert('ok, ' + ID + ' canceled');
                }
            );

        };
       
        $scope.activateSmartGate = function (deviceId, vendorDeviceId, currentDoorState) {

            //if (!socket.connected) {
            //    socket.connect();
            //    socket.emit('join', { email: authData.userName });
            //}

            currentNexxGarage.vendorDeviceId = vendorDeviceId;
            currentNexxGarage.deviceId = deviceId;
            currentNexxGarage.garageDoorState = currentDoorState;

            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
            //reset the garage status by a timer since there's no way to know when the garage door activity is complete
            //$timeout(function () {
            //    $scope.garageActivityStatus = "";
            //}, 11000);

            //reset the garageSignalComplete flag to true after 3 sec.  this will allow the user to resend the signal incase the device was down
            $timeout(function () {
                $scope.garageSignalComplete = true;
            }, 3000);

            if (!$scope.garageSignalComplete)
                return;

            $scope.garageSignalComplete = false;

            if ($scope.controlButtonText == "Open") {
                $scope.garageActivityStatus = "Garage is opening";
                simpalTek.lib.doorControlAnimation.startUp(function () { $scope.garageActivityStatus = ""; simpalTek.lib.doorControlAnimation.currentState("Open"); $scope.$apply(); });
            }
            else if ($scope.controlButtonText == "Close") {
                $scope.garageActivityStatus = "Garage is closing";
                simpalTek.lib.doorControlAnimation.startDown(function () { $scope.garageActivityStatus = ""; $scope.$apply(); });
            }

            simpalTek.helper.sendDoorSignal(data,
                function (response) {
                    userDb.gateActivities = response.result;

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Garage button event successful");

                    //if (!socket.connected) {
                    //    socket.connect();
                    //    socket.emit('join', { email: authData.userName });

                        //simpalTek.helper.getUserPreloadData(function () {

                        //}, function () {

                        //})

                        //$timeout(function () {

                        //    if (!socket.connected) {
                        //        simpalTek.helper.messageCenterService(simpalTek.const.CURRENT_DATA_BEING_RETRIEVED_MSG, simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown);
                        //    }

                        //    simpalTek.global.isDataRefreshed = false;
                        //}, 3000);
                    //}

                    if (response.status == 1) {
                        //navigator.notification.alert("Garage Door Control Error", function () {
                        //    $ionicLoading.hide();
                        //}, response.result, "Ok");

                        $ionicPopup.alert({
                            title: 'Garage Door Control Error',
                            template: response.result
                        });

                    }

                    //if the door did not acknowledge after 5 sec after a successful sent than try it one more time.
                    //$timeout(function () {
                    //    if (!simpalTek.global.doorSignalSentAcknowledged) {

                    //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Garage button sending signal retry");

                    //        simpalTek.helper.sendDoorSignal(data,
                    //            function (response) {
                    //                userDb.gateActivities = response.result;
                    //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Garage button event successful retry");
                    //            },
                    //            function (err, status) {
                    //                $scope.garageSignalComplete = true;
                    //                console.log("Garage signal error", err, status)

                    //                $ionicPopup.alert({
                    //                    title: 'Garage Door Control Error',
                    //                    template: 'There was an issue sending a signal to your garage door control.'
                    //                });
                    //            }
                    //        );
                    //    }
                    //}, 5000);
                },
                function (err, status) {
                    $scope.garageSignalComplete = true;
                    console.log("Garage signal error", err, status)

                    //navigator.notification.alert("Garage Door Control Error", function () {
                    //    $ionicLoading.hide();
                    //}, "There was an issue sending a signal to your garage door control.", "Ok");

                    $ionicPopup.alert({
                        title: 'Garage Door Control Error',
                        template: 'There was an issue sending a signal to your garage door control.'
                    });
                }
            );

            //$http.post(ngAuthSettings.apiDomainServiceBaseUri + '/GateControlSignal', data).success(function (response) {
            //    //console.log("response", response)
            //    userDb.gateActivities = response.result;
            //    //simpalTek.global.localStorage.remove('UserDb')


            //    //if (data.ActivityType == 1) {
            //    //    data.ActivityType = 2;
            //    //    $scope.currentState = "Close";
            //    //}
            //    //else {
            //    //    data.ActivityType = 1
            //    //    $scope.currentState = "Open";
            //    //}

            //    //userDb.currentGarageState = $scope.currentState;

            //    //simpalTek.global.localStorage.set('UserDb', userDb);

            //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Garage button event successful");

            //}).error(function (err, status) {
            //    $scope.garageSignalComplete = true;
            //    console.log("Garage signal error", err, status)

            //    $ionicPopup.alert({
            //        title: 'Garage Door Control Error',
            //        template: 'There was an issue sending a signal to your garage door control.'
            //    });

            //});


        }

        function GetAccessDevices() {

            for (var i = 0; i < userAccessDevices.length; i++) {
                userAccessDevices[i].number = i + 1;
            }

            $scope.accessDevices = userAccessDevices;
            //default color for 1st garage door
            if (userAccessDevices[0].userType == "O")
                $rootScope.navBgColor = '';
            else
                $rootScope.navBgColor = '#FF9009';

            simpalTek.lib.doorControlAnimation.currentState((currentNexxGarage.garageDoorState) ? currentNexxGarage.garageDoorState : 'Open');

        }
}])

;
