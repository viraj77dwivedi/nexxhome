﻿app.controller("DeviceUserManagerCtrl", function (
    $scope,
    $ionicLoading,
    $state,
    $window,
    $rootScope,
    $timeout,
    authService,
    $ionicModal,
    $http,
    ngAuthSettings,
    $ionicPopup,
    $document
) {
    var userDb = simpalTek.global.localStorage.get('UserDb');
    var authData = simpalTek.global.localStorage.get('authorizationData');
    var selectedVendorDeviceId = "";
    var selectedDeviceId = "";
    var userAccessDevices = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });

    $scope.userData = {};
    $scope.userName = authService.authentication.userName;
    $scope.noDeviceSetup = false;
    $scope.registeredDevices = userDb.registeredDevices.filter(function (x) { return x.isEnable;})

    if (userDb.devicesAccess.length == 0) {
        $scope.noDeviceSetup = true;
        return;
    }

    $scope.onDeviceSelect = function (newValue, oldValue) {
        //alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValue));
        if (newValue) {
            //set this if the user has a register device
            if (userDb.registeredDevices.length > 0) {
                $scope.deviceModel.deviceNickName = newValue.locationNickName
                selectedVendorDeviceId = newValue.deviceName;
                selectedDeviceId = newValue.deviceId;

            }

            $rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authService.authentication.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
            $rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });
        }
    };

    $scope.doRefresh = function () {
        simpalTek.helper.getUserPreloadData(function () {

            //$scope.userData.deviceId = userDb.devicesAccess[0].deviceId;
            //$scope.deviceNickName = userDb.devicesAccess[0].deviceNickName;
            $rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authService.authentication.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
            $rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });

            //Stop the ion-refresher from spinning
            $rootScope.$broadcast('scroll.refreshComplete');
        },
        function () {

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "getUserPreloadData UserPreloadData Error");

            //Stop the ion-refresher from spinning
            $rootScope.$broadcast('scroll.refreshComplete');

        });
    }

    //$scope.userData.deviceId = userDb.devicesAccess[0].deviceId;
    //$scope.deviceNickName = userDb.devicesAccess[0].deviceNickName;
    
    //$rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'O' || item.email.toLowerCase() != authData.userName.toLowerCase()});
    //$rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authData.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
    //$rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authData.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });

    $scope.showUserAddEdit = function (type, userType, email, userDeviceAccessId, grant, grantAutoOpen, grantViewActivityHistory, autoDoorOpen, doorOpenNotification, doorOpenReminderNotification, doorCloseNotification, deviceId) {

        if (type == "ADD") {
            //if ($rootScope.registeredDevicesCount == 0 || $rootScope.registeredDevicesCount == null) {
            if (userDb.registeredDevices.length == 0) {
                $ionicPopup.alert({
                    title: 'No Device Registered',
                    template: 'You do not have a Nexx Garage device registered to your account so you cannot grant access.'
                });

                return;
            }
        }


        $ionicModal.fromTemplateUrl('templates/deviceUserManagerAddEdit.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            
            userDb = simpalTek.global.localStorage.get('UserDb');
            $scope.userAddEditModal = modal;
            $scope.userAddEditModalType = type;
            $scope.userType = userType;
            $scope.userData.userDeviceAccessId = userDeviceAccessId;

            if (deviceId)
                $scope.userData.deviceId = deviceId;//userDb.devicesAccess[0].deviceId;
            else
                $scope.userData.deviceId = selectedDeviceId;

            //$scope.deviceNickName = userDb.devicesAccess[0].deviceNickName;

            $scope.userData.userPreference = [

                            { id: 1, text: "Auto Open Door", checked: (autoDoorOpen == "true") ? true : false, ownerGrantAccess: grantAutoOpen },
                            { id: 2, text: "Door Open Notification", checked: (doorOpenNotification == "true") ? true : false, ownerGrantAccess: true },
                            { id: 4, text: "Door Close Notification", checked: (doorCloseNotification == "true") ? true : false, ownerGrantAccess: true },
                            { id: 3, text: "Door Open Reminder", checked: (doorOpenReminderNotification == "true") ? true : false, ownerGrantAccess: true },
            ];

            switch(type)
            {
                case "EDIT":
                    modal.scope.userData.email = email;

                    if (grant == "true")
                        $scope.userData.grantAccess = true; //Boolean(grant);
                    else
                        $scope.userData.grantAccess = false;

                    if (grantAutoOpen == "true")
                        $scope.userData.grantAutoOpen = true;
                    else
                        $scope.userData.grantAutoOpen = false;

                    if (grantViewActivityHistory == "true")
                        $scope.userData.grantViewActivityHistory = true;
                    else
                        $scope.userData.grantViewActivityHistory = false;

                    break;
                case "ADD":
                    $scope.userData.grantAccess = true;
                    break;
            }
            
            if (deviceId) {
                $rootScope.selectedGeofence = JSON.parse(userDb.devicesAccess.filter(function (item) { return item.deviceId == deviceId })[0].ownerDeviceGPSLoc);
                $rootScope.selectedGeofence.radius = 70;
                $rootScope.selectedGeofence.identifier = deviceId;
                //$rootScope.selectedGeofence.vendorDeviceId = vendorDeviceId;

                if (userDb.preference.geofences) {
                    var userDeviceGeoFenceConfig = userDb.preference.geofences.filter(function (x) { return x.identifier == deviceId });

                    if (userDeviceGeoFenceConfig.length > 0) {
                        $rootScope.selectedGeofence.radius = parseFloat(userDeviceGeoFenceConfig[0].radius);
                    }
                }

                $scope.center = {
                    lat: $rootScope.selectedGeofence.latitude,
                    lng: $rootScope.selectedGeofence.longitude,
                    zoom: 17
                };

                $scope.markers = {
                    marker: {
                        draggable: true,
                        message: $rootScope.selectedGeofence.identifier,
                        lat: $rootScope.selectedGeofence.latitude,
                        lng: $rootScope.selectedGeofence.longitude,
                        icon: {}
                    }
                };
                $scope.paths = {
                    circle: {
                        type: "circle",
                        radius: $rootScope.selectedGeofence.radius,
                        latlngs: $scope.markers.marker,
                        clickable: false
                    }
                };
            }

            $scope.userAddEditModal.show();

        });
    }

    $scope.openDeviceLocationConfig = function () {
        $ionicModal.fromTemplateUrl('templates/geofence.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            //userDb = simpalTek.global.localStorage.get('UserDb');

            $scope.deviceLocationConfigModal = modal;

            //$scope.paths.circle.radius = $rootScope.selectedGeofence.radius;

            

            $scope.deviceLocationConfigModal.show();
        });
    }

    $scope.userGrantPermission = function(grantPermission){
        if (!grantPermission)
            return false;
    }
    $scope.hideUserAddEdit = function () {
        $scope.userAddEditModal.hide();
        $scope.userData = {grantAccess : true};
        $scope.userAddEditModal.remove();
    }

    $scope.saveUser = function () {
        var rules = {};
        var messages = {};

        $.validator.addMethod(
            "emailExist",
            function (value, element) {
                var exist = $rootScope.ownerDevicesAccess.filter(function (item) { return item.email.toLowerCase() == value.trim().toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });

                if (exist.length > 0)
                    return false;
                else
                    return true;
            },
            "Guest username already added"
        );


        rules.email = {
            required: {
                depends: function () {
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            }, email: true
            , emailExist : true
        };
        rules.confirm = {
            required: {
                depends: function () {
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            }, equalTo: email
        };

        messages.email = { required: "Email is required", email: "Invalid email" };
        messages.confirm = { required: "Cofirm Email is required", equalTo: "Email does not match" };

        var validator = new simpalTek.helper.jQueryFormValidator("userAccessAddEdit", rules, messages);
            
        if (validator.validate()) {

            $ionicLoading.show({
                template: 'Saving device.  Please wait.'
            });

            if ($scope.userAddEditModalType == "ADD") {
                $http.post(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceAccess', $scope.userData).success(function (response) {
                    console.log("Add response", response)
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Add device user successful");

                    userDb.devicesAccess = response.result;
                    simpalTek.global.localStorage.set('UserDb', userDb);

                    //$scope.userDevicesAccess = response.result;
                    $rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authService.authentication.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
                    $rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });
                    
                    navigator.notification.alert("The user has been successfully added.  They must have their own account created and the App installed onto their device to gain access.", function () {
                        $scope.hideUserAddEdit();
                        $ionicLoading.hide();
                    }, "User Access Added", "Ok");

                    

                }).error(function (err, status) {

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Add device user  error " + err + " | " + status);
                });
            } else if ($scope.userAddEditModalType == "EDIT") {
                console.log("$scope.userData", $scope.userData)
                $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceAccess', $scope.userData).success(function (response) {
                    console.log("Edit response", response)
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Edit device user successful");

                    userDb.devicesAccess = response.result;
                    simpalTek.global.localStorage.set('UserDb', userDb);

                    //$scope.userDevicesAccess = response.result;
                  
                    $rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authService.authentication.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
                    $rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });

                    simpalTek.global.localStorage.set('UserDb', userDb);
                    //$scope.$apply();

                    $scope.hideUserAddEdit();
                    $ionicLoading.hide();

                }).error(function (err, status) {

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Edit device user  error " + err + " | " + status);
                });
            }

        }
    }


    $scope.deleteUser = function () {


        var onConfirm = function (buttonIndex) {

            if (buttonIndex == 2) {

                $ionicLoading.show({
                    template: 'Removing user\'s access.  Please wait.'
                });

                $http({
                    method: 'DELETE',
                    url: ngAuthSettings.apiDomainServiceBaseUri + '/DeviceAccess',
                    data: $scope.userData,
                    headers: { 'Content-Type': 'application/json;charset=utf-8' }
                }).success(function (response) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Remove user\'s access successful");

                    $scope.userDevicesAccess = response.result;

                    userDb.devicesAccess = response.result;
                    simpalTek.global.localStorage.set('UserDb', userDb);


                    $rootScope.ownerDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() != authService.authentication.userName.toLowerCase() && item.vendorDeviceId == selectedVendorDeviceId });
                    $rootScope.guestDevicesAccess = userDb.devicesAccess.filter(function (item) { return item.userType == 'G' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() });

                    $scope.hideUserAddEdit();
                    $ionicLoading.hide();
                }).error(function (err, status) {

                    navigator.notification.alert("There was an issue removing this user's access.  Please try again.", function () {

                    }, "Delete Error", "Ok");

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Removing user's access  error " + err + " | " + status);
                });
            }
        }

        navigator.notification.confirm(
                'Are you sure you want to remove this user\'s access?', // message
                 onConfirm,            // callback to invoke with index of button pressed
                'Delete Device',           // title
                ['No', 'Yes']     // buttonLabels
            );
    }

    if ($scope.registeredDevices.length > 0) {
        $scope.deviceModel = $scope.registeredDevices[0];
        $scope.onDeviceSelect($scope.deviceModel);
    }
    else if (userAccessDevices.length > 0) {
        $scope.deviceModel = userAccessDevices[0];
        $scope.onDeviceSelect($scope.deviceModel);
    }
});