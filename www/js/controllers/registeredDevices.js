﻿app.controller("RegisteredDevicesCtrl", function (
    $scope,
    $ionicModal,
    $http,
    ngAuthSettings,
    $ionicLoading,
    $timeout,
    $rootScope,
    Geofence,
    authService
) {

    var userDb = simpalTek.global.localStorage.get('UserDb');
    var selectedDevice = {};
    var selectedDeviceNumber = 0;
    var selectedGeofence = {};
    var newLatLogPosition = { latitude: 0, longitude : 0};
    var origIsMain = false;
    var origNickName = "";

    $scope.registerDeviceData = {};
    $scope.registerDeviceData.deviceNickName = "";
    $scope.registerDeviceData.number = "";

    //if (userDb != null) {
        $rootScope.registeredDevices = userDb.registeredDevices;
    //}

        $scope.showEditDevice = function (deviceId, vendorDeviceId, nickname, isMain, isEnable, number) {

        $ionicModal.fromTemplateUrl('templates/editRegisteredDevice.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            userDb = simpalTek.global.localStorage.get('UserDb');

            $scope.deviceEditModal = modal;
            $scope.registerDeviceData.deviceNickName = nickname;
            $scope.registerDeviceData.number = number;
            selectedDeviceNumber = number;
            origIsMain = isMain;
            origNickName = nickname;

            if (isMain === true)
                $scope.isDisabled = true;
            else
                $scope.isDisabled = false;

            selectedDevice.deviceId = deviceId;
            selectedDevice.vendorDeviceId = vendorDeviceId;

            $scope.registerDeviceData.settings = [

                            { id: 1, text: "Primary Device", checked: isMain },
                            { id: 2, text: "Enable", checked: isEnable }
            ];

            $rootScope.selectedGeofence = JSON.parse(userDb.registeredDevices.filter(function (x) { return x.deviceId == deviceId })[0].gpsLoc);
            $rootScope.selectedGeofence.radius = 70;
            $rootScope.selectedGeofence.identifier = deviceId;
            $rootScope.selectedGeofence.vendorDeviceId = vendorDeviceId;

            if (userDb.preference.geofences) {
                var userDeviceGeoFenceConfig = userDb.preference.geofences.filter(function (x) { return x.identifier == deviceId });

                if (userDeviceGeoFenceConfig.length > 0) {
                    $rootScope.selectedGeofence.radius = parseFloat(userDeviceGeoFenceConfig[0].radius);
                }
            }

            $scope.center = {
                lat: $rootScope.selectedGeofence.latitude,
                lng: $rootScope.selectedGeofence.longitude,
                zoom: 17
            };

            $scope.markers = {
                marker: {
                    draggable: true,
                    message: $rootScope.selectedGeofence.identifier,
                    lat: $rootScope.selectedGeofence.latitude,
                    lng: $rootScope.selectedGeofence.longitude,
                    icon: {}
                }
            };
            $scope.paths = {
                circle: {
                    type: "circle",
                    radius: $rootScope.selectedGeofence.radius,
                    latlngs: $scope.markers.marker,
                    clickable: false
                }
            };

            $scope.deviceEditModal.show();
        });
    }

    $scope.$on('leafletDirectiveMarker.dragend', function (e, args) {
        userDb = simpalTek.global.localStorage.get('UserDb');

        //only the owner of the device can set the geo position
        if (userDb.devicesAccess.filter(function (item) { return item.userType == 'O' && item.email.toLowerCase() == authService.authentication.userName.toLowerCase() }).length > 0) {
            newLatLogPosition.longitude = args.leafletEvent.target._latlng.lng;
            newLatLogPosition.latitude = args.leafletEvent.target._latlng.lat;
        }
    });

    $scope.closeDeviceLocationConfigModal = function () {
        newLatLogPosition.longitude = 0;
        newLatLogPosition.longitude = 0;

        $scope.deviceLocationConfigModal.hide();
        $scope.deviceLocationConfigModal.remove();
    }

    $scope.deviceNumberSelect = function (newValue, oldValue) {

        if (newValue != selectedDeviceNumber) {
            var r = userDb.registeredDevices.filter(function (item) { return item.number == newValue });

            if (r.length > 0) {

                if (window.cordova) {
                    navigator.notification.alert("There is already a device with the same Voice Assistant Number.  Either change the current number or change the other device number.", function () {

                    }, "Duplicate Voice Assistant Number", "Ok");
                }

                $scope.registerDeviceData.number = oldValue;

            }
        }
    }

    $scope.resetDevice = function () {
        var domainModel = {};
        var deviceInfo = {};

        var onConfirm = function (buttonIndex) {

            if (buttonIndex == 2) {
                $ionicLoading.show({
                    template: 'Clearing previous WiFi configuration on your device.  Please wait.'
                });

                deviceInfo.deviceId = selectedDevice.deviceId;
                deviceInfo.vendorDeviceId = selectedDevice.vendorDeviceId;

                //_deviceInfo.deviceId = userDb.registeredDevices[0].deviceId;
                //_deviceInfo.vendorDeviceId = userDb.registeredDevices[0].deviceNickName;


                $http({
                    method: 'DELETE',
                    url: ngAuthSettings.apiDomainServiceBaseUri + '/DeviceConfig',
                    data: deviceInfo,
                    headers: { 'Content-Type': 'application/json;charset=utf-8' }
                }).success(function (response) {
                    
                    //$scope.hideRegisteredDevice();
                    //$rootScope.showRegisterDeviceWizard(deviceInfo.deviceId, deviceInfo.vendorDeviceId)


                    if (response.status == 0) {
                        
                        $timeout(function () {
                           
                            navigator.notification.alert("Your device WiFi configuration has been successfully cleared.", function () {

                            }, "Device WiFi Cleared", "Ok");

                            $ionicLoading.hide();

                            $scope.hideRegisteredDevice();
                            $rootScope.showRegisterDeviceWizard(deviceInfo.deviceId, deviceInfo.vendorDeviceId)

                        }, 5000);

                    }
                    else {

                        $timeout(function () {
                            navigator.notification.alert("There was an issue clearing WiFi configuration on the device.", function () {
                                $ionicLoading.hide();
                            }, "Device WiFi Clearing Error", "Ok");
                        }, 100);
                    }

                }).error(function (err, status) {

                    $timeout(function () {
                        navigator.notification.alert("There was an issue clearing WiFi configuration on the device.", function () {
                            $ionicLoading.hide();
                        }, "Device WiFi Clearing Error", "Ok");
                    }, 100);

                });
            }
        }

        navigator.notification.confirm(
            'Updating WiFi configuration will reset the device to factory default and the LED light on the device will then be steady green.  You will need to go through the WiFi configuration again if you wish to control the device via this app.  Do you wish to continue?', // message
             onConfirm,            // callback to invoke with index of button pressed
            'Update WiFi Configuration',           // title
            ['No', 'Yes']     // buttonLabels
        );


    }

    $scope.deleteDevice = function () {
        var domainModel = {};
        var deviceInfo = {};

        var onConfirm = function (buttonIndex) {

            if (buttonIndex == 2) {
                $ionicLoading.show({
                    template: 'Deleting your device.  Please wait.'
                });

                deviceInfo.deviceId = selectedDevice.deviceId;
                deviceInfo.vendorDeviceId = selectedDevice.vendorDeviceId;
                
                $http({
                    method: 'DELETE',
                    url: ngAuthSettings.apiDomainServiceBaseUri + '/Device',
                    data: deviceInfo,
                    headers: { 'Content-Type': 'application/json;charset=utf-8' }
                }).success(function (response) {
                    console.log(response)

                    userDb.registeredDevices = userDb.registeredDevices;
                    $rootScope.registeredDevices = response.result;

                    simpalTek.global.localStorage.set('UserDb', userDb)
                    
                    //if (userDb.registeredDevices)
                    //    $scope.registeredDeviceCount = userDb.registeredDevices.length;

                    navigator.notification.alert("Your device has been successfully deleted.", function () {

                    }, "Device Deleted", "Ok");

                     $scope.hideRegisteredDevice();

                }).error(function (err, status) {

                    $timeout(function () {
                        navigator.notification.alert("There was an issue deleting the device.", function () {
                            $ionicLoading.hide();
                        }, "Device Delete Error", "Ok");
                    }, 100);

                    $scope.hideRegisteredDevice();
                });
            }
        }

        navigator.notification.confirm(
            'Deleting this device from your account will also remove all history.  Are you sure you want to delete this device?', // message
             onConfirm,            // callback to invoke with index of button pressed
            'Delete Device',           // title
            ['No', 'Yes']     // buttonLabels
        );


    }

    
    $scope.saveDeviceLocationConfig = function () {
        var userGeoFencePref = userDb.preference;
        var currentGeoLoc = {};
        var deviceInfo = {};

        $ionicLoading.show({
            template: 'Save distance configuration.  Please wait.'
        });

        if (!userGeoFencePref.geofences)
            userGeoFencePref.geofences = [];

        if (userDb.preference.geofences) {
            var userDeviceGeoFenceConfig = userDb.preference.geofences.filter(function (x) { return x.identifier == $rootScope.selectedGeofence.identifier });
            

            if (userDeviceGeoFenceConfig.length > 0) {
                var indexDeviceGeoFenceConfig = userDb.preference.geofences.indexOf(userDeviceGeoFenceConfig[0]);

                userDb.preference.geofences.splice(indexDeviceGeoFenceConfig, 1)
            }
        }

        
        if (newLatLogPosition.latitude != 0) {
            $rootScope.selectedGeofence.latitude = newLatLogPosition.latitude;
            $rootScope.selectedGeofence.longitude = newLatLogPosition.longitude;

            currentGeoLoc.latitude = $rootScope.selectedGeofence.latitude;
            currentGeoLoc.longitude = $rootScope.selectedGeofence.longitude;

            deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);

            deviceInfo.deviceId = $rootScope.selectedGeofence.identifier;
            deviceInfo.vendorDeviceId = $rootScope.selectedGeofence.vendorDeviceId;
            //deviceInfo.NickName = $scope.deviceNickName;

            //Geofence.init();

            //only call the #config once
            //if (simpalTek.global.bgGeoConfigured == false)
            //    simpalTek.helper.geoFence().config();

            //simpalTek.helper.geoFence().updateAllLocations();

            $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/Device', deviceInfo)
            .then(function (response) {

            },
            function (err) {

            })
        }
        
        $rootScope.selectedGeofence.radius = parseFloat($scope.paths.circle.radius);

        userGeoFencePref.geofences.push($rootScope.selectedGeofence);
        
        
        //simpalTek.global.localStorage.set('UserDb', userDb);
        //var data = {};
        //data = JSON.parse(JSON.stringify(userGeoFencePref))
        //data.geofences = JSON.stringify(userGeoFencePref.geofences);

        $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/PreferenceSetting', userGeoFencePref).success(function (response) {
            
            simpalTek.helper.getUserPreloadData(function () {

                $ionicLoading.hide();

                simpalTek.helper.geoFence().updateAllLocations();

                navigator.notification.alert("Your auto open distance has been saved.", function () {

                }, "Auto Open Distance Setting", "Ok");

                $scope.deviceLocationConfigModal.hide();
                $scope.deviceLocationConfigModal.remove();

            }, function () {
                $ionicLoading.hide();
                $scope.deviceLocationConfigModal.hide();
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "getUserPreloadData on get GPS position");
            });
            
        }).error(function (err, status) {
            $ionicLoading.hide();

            navigator.notification.alert("There was an issue saving your auto open distance.  Please try again.", function () {

            }, "Auto Open Distance Setting Error", "Ok");

            $scope.deviceLocationConfigModal.hide();
            $scope.deviceLocationConfigModal.remove();
        });
        
        
    }

    $scope.openDeviceLocationConfig = function () {
        $ionicModal.fromTemplateUrl('templates/geofence.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.deviceLocationConfigModal = modal;
            
            $scope.paths.circle.radius = $rootScope.selectedGeofence.radius;

            $scope.deviceLocationConfigModal.show();
        });
    }

    $scope.updateDeviceLocation = function () {
        var deviceInfo = {};
        var currentGeoLoc = {};

        navigator.notification.confirm("Warning - Updating the device location will configure the \"Auto Open\" feature to automatically open the garage door.  You must be in the garage where the Nexx Garage device is located.  Are you sure you want to continue?", function (buttonIndex) {

            if (buttonIndex == 2) {
                $ionicLoading.show({
                    template: 'Updating device location.  Please wait.'
                });

                Geofence.getCurrentPosition().then(function (position) {
                    console.log("Current position found", position);

                    if (position.coords) {
                        currentGeoLoc.latitude = position.coords.latitude;
                        currentGeoLoc.longitude = position.coords.longitude;

                        deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);

                        deviceInfo.deviceId = selectedDevice.deviceId;
                        deviceInfo.vendorDeviceId = selectedDevice.vendorDeviceId;
                        //deviceInfo.NickName = $scope.deviceNickName;
                        
                        $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/Device', deviceInfo)
                        .then(function (response) {
                            console.log(response)
                            
                            if (response.data.status == 0) {

                                simpalTek.helper.getUserPreloadData(function () {
                                    $ionicLoading.hide();

                                    navigator.notification.alert("Your device location has been updated.", function () {

                                    }, "Device Location Update", "Ok");
                                }, function () {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "getUserPreloadData on get GPS position");
                                });
                            }
                            else {
                                $ionicLoading.hide();
                                navigator.notification.alert("There was an issue updating your device location.", function () {

                                }, "Device Location Update Error", "Ok");
                            }

                        },
                        function (err) {

                            $ionicLoading.hide();

                            navigator.notification.alert("There was an issue updating your device location.", function () {

                            }, "Device Location Update Error", "Ok");

                        })

                    }
                    
                }, function (reason) {
                    console.log("Cannot obtain current location", reason);
                });
            }

        }, "Device Location Update",
          ['Cancel', 'Continue']
        );
    }
        
    $scope.saveDeviceInfo = function () {
        var deviceInfo = {};

        deviceInfo.deviceId = selectedDevice.deviceId;
        deviceInfo.nickName = $scope.registerDeviceData.deviceNickName;
        deviceInfo.isMain = $scope.registerDeviceData.settings[0].checked;
        deviceInfo.enable = $scope.registerDeviceData.settings[1].checked;
        deviceInfo.number = $scope.registerDeviceData.number;
                
        //save method
        var doSaveDeviceInfo = function () {

            $ionicLoading.show({
                template: 'Save device information.  Please wait.'
            });

            $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/Device', deviceInfo)
            .then(function (response) {
                console.log(response)

                if (response.data.status == 0) {

                    userDb.registeredDevices = response.data.result.registeredDevices;
                    userDb.devicesAccess = response.data.result.devicesAccess;

                    $rootScope.registeredDevices = userDb.registeredDevices;

                    simpalTek.global.localStorage.set('UserDb', userDb);

                    if (deviceInfo.isMain && !origIsMain) {
                        if (userDb.preference.proximityMonitorAuto) {
                            Geofence.init();
                        }
                    }

                    if (window.cordova) {
                        navigator.notification.alert("Your device information has been updated.", function () {

                        }, "Device Information Update", "Ok");
                    }
                }
                else {

                    navigator.notification.alert("There was an issue updating your device information.", function () {

                    }, "Device Information Update Error", "Ok");
                }

                $scope.hideRegisteredDevice();
                $ionicLoading.hide();
            },
            function (err) {

                $scope.hideRegisteredDevice();
                $ionicLoading.hide();

                navigator.notification.alert("There was an issue updating your device information.", function () {

                }, "Device Information Update Error", "Ok");

            })
        };
        
        if (selectedDeviceNumber != deviceInfo.number) {
            var r = userDb.registeredDevices.filter(function (item) { return item.number == deviceInfo.number });

            if (r.length > 0) {
                navigator.notification.alert("There is already a device with the same Voice Assistant Number.  Either change the current device number or change the other device number.", function () {

                }, "Duplicate Voice Assistant Number", "Ok");

                return;
            }
        }
        else if (deviceInfo.isMain && !deviceInfo.enable) {
            var x = userDb.registeredDevices.filter(function (item) { return item.isEnable == true });

            if (x.length > 0) {
                navigator.notification.confirm(
                   'You do not have a primary device that is enabled.  Without a primary device enable you will not be able to use the Auto Open, Amazon Alexa, or Google Home feature.  Do you want to continue?', // message
                    function (buttonIndex) {
                        if (buttonIndex == 2) {

                            doSaveDeviceInfo();
                        }
                    },            // callback to invoke with index of button pressed
                   'Primary Device',           // title
                   ['No', 'Yes']     // buttonLabels
                );

                return;
            }

        }
        else if (origNickName.trim().toLowerCase() != deviceInfo.nickName.trim().toLowerCase()) {
            var deviceNickNameExist = ((userDb.registeredDevices.filter(function (x) { return x.locationNickName.trim().toLowerCase() == deviceInfo.nickName.trim().toLowerCase() }).length > 0) ? true : false);

            if (deviceNickNameExist) {
                navigator.notification.alert("The device nickname you entered already exist.  Please select another nickname.", function () {

                }, "Nickname already exist", "Ok");

                return;
            }
        }

        //must check if the user has a primary device enable
        if (!deviceInfo.isMain && deviceInfo.enable)
        {
            var d = userDb.registeredDevices.filter(function (item) { return item.isMain == true && item.isEnable == true });

            if(d.length == 0)
            {
                navigator.notification.confirm(
                   'You do not have a primary device that is enabled.  Would you like to set this as your primary device?', // message
                    function (buttonIndex) {
                        if(buttonIndex==2)
                        {
                            deviceInfo.isMain = true;

                            doSaveDeviceInfo();
                        }
                    },            // callback to invoke with index of button pressed
                   'Primary Device',           // title
                   ['No', 'Yes']     // buttonLabels
                );

                return;
            }
        }
        

        doSaveDeviceInfo();

        
    }

    $scope.hideRegisteredDevice = function () {
        $ionicLoading.hide();
        $scope.deviceEditModal.hide();
        $scope.deviceEditModal.remove();
    }
});
