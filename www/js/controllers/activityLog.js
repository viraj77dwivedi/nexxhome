﻿app.controller('ActivityLogCtrl', [

    '$scope',
    '$http',
    '$ionicModal',
    'ngAuthSettings',
    'localStorageService',
    '$filter',
    '$rootScope',
    'authService',
    function ($scope, $http, $ionicModal, ngAuthSettings, localStorageService, $filter, $rootScope, authService) {
        
        var userDb   = simpalTek.global.localStorage.get('UserDb');
        var nameArray;
        var authData = simpalTek.global.localStorage.get('authorizationData');
        var selectedVendorDeviceId = "";

        $scope.userAccessDevices = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() && item.ownerGrantViewActivityHistory == true });
        $scope.onDeviceSelect = function (newValue, oldValue) {
            //alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValue));
            if (newValue) {
                $scope.activities = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == newValue.vendorDeviceId });
                selectedVendorDeviceId = newValue.vendorDeviceId;

                if ($scope.activities.length > 0) {
                    $scope.shownGroup = $scope.activities[0].group;
                };
            };
        };
        
        
        /*
        * if given group is the selected group, deselect it
        * else, select the given group
        */
        $scope.toggleGroup = function (group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };

        $scope.isGroupShown = function (group) {
            return $scope.shownGroup === group;
        };

        $scope.doRefresh = function () {

            simpalTek.helper.getUserPreloadData(function () {

                userDb = simpalTek.global.localStorage.get('UserDb');

                //for (i = 0; i < userDb.gateActivities.length; i++) {
                //    userDb.gateActivities[i].group = $filter('utcToLocal')(userDb.gateActivities[i].activityDate, "MMMM EEEE d, y");
                //}

                for (i = 0; i < userDb.gateActivities.length; i++) {
                    
                    userDb.gateActivities[i].group = $filter('utcToLocal')(userDb.gateActivities[i].activityDate, "MMMM EEEE d, y");
                    userDb.gateActivities[i].userFullNameFriendly = userDb.gateActivities[i].userFullName;

                    nameArray = userDb.gateActivities[i].userFullName.split("-");

                    if (nameArray.length > 1) {
                        if (nameArray[0] == "A") {
                            userDb.gateActivities[i].activityTypeFriendly = "Auto Opened";
                            userDb.gateActivities[i].userFullNameFriendly = nameArray[1];
                        }
                        else {
                            if (userDb.gateActivities[i].activityType == "Open")
                                userDb.gateActivities[i].activityTypeFriendly = "Opened";
                            else if (userDb.gateActivities[i].activityType == "Close")
                                userDb.gateActivities[i].activityTypeFriendly = "Closed";
                            else
                                userDb.gateActivities[i].activityTypeFriendly = "Unknown";
                        }
                    }
                    else {
                        if (userDb.gateActivities[i].activityType == "Open")
                            userDb.gateActivities[i].activityTypeFriendly = "Opened";
                        else if (userDb.gateActivities[i].activityType == "Close")
                            userDb.gateActivities[i].activityTypeFriendly = "Closed";
                        else
                            userDb.gateActivities[i].activityTypeFriendly = "Unknown";
                    }
                }

                $scope.activities = userDb.gateActivities.filter(function (item) { return item.vendorDeviceId == selectedVendorDeviceId});
                //$scope.activities = userDb.gateActivities;

                //Stop the ion-refresher from spinning
                $rootScope.$broadcast('scroll.refreshComplete');
            },
            function () {

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "getUserPreloadData UserPreloadData Error");

                //Stop the ion-refresher from spinning
                $rootScope.$broadcast('scroll.refreshComplete');

            });
        }

        for (i = 0; i < userDb.gateActivities.length; i++) {
            userDb.gateActivities[i].group = $filter('utcToLocal')(userDb.gateActivities[i].activityDate, "MMMM EEEE d, y");
            userDb.gateActivities[i].userFullNameFriendly = userDb.gateActivities[i].userFullName;

            nameArray = userDb.gateActivities[i].userFullName.split("-");

            if (nameArray.length > 0) {
                if (nameArray[0] == "A") {
                    userDb.gateActivities[i].activityTypeFriendly = "Auto Opened";
                    userDb.gateActivities[i].userFullNameFriendly = nameArray[1];
                }
                else {
                    if (userDb.gateActivities[i].activityType == "Open")
                        userDb.gateActivities[i].activityTypeFriendly = "Opened";
                    else if (userDb.gateActivities[i].activityType == "Close")
                        userDb.gateActivities[i].activityTypeFriendly = "Closed";
                    else
                        userDb.gateActivities[i].activityTypeFriendly = "Unknown";
                }
            }
            else {
                if (userDb.gateActivities[i].activityType == "Open")
                    userDb.gateActivities[i].activityTypeFriendly = "Opened";
                else if (userDb.gateActivities[i].activityType == "Close")
                    userDb.gateActivities[i].activityTypeFriendly = "Closed";
                else
                    userDb.gateActivities[i].activityTypeFriendly = "Unknown";
            }
        }

        $scope.deviceModel = $scope.userAccessDevices[0];
        $scope.onDeviceSelect($scope.deviceModel);
        //$scope.activities = userDb.gateActivities;

		
    }])