/**
 * User controller
 *
 * @module UserCtrl

 * @class simpalGate.controllers
 */

app.controller("RegisterCtrl", [

    '$scope',
    '$state',
    '$timeout',
    '$rootScope',
    '$location',
    '$ionicModal',
    '$account',
    '$ionicLoading',
    'authService',
    '$http',
    'ngAuthSettings',
    '$ionicScrollDelegate',
    function ($scope, $state, $timeout, $rootScope, $location, $ionicModal, $account, $ionicLoading, authService, $http, ngAuthSettings, $ionicScrollDelegate) {


        $scope.loginData = {};
        $scope.values = null;
        $scope.registration = {
            email: "",
            password: "",
            firstName: "",
            lastName: "",
            deviceId: "",
            confirm : ""
        };

        $scope.timeZones = [{ id: 1, name: 'Pacific Standard Time' },
                            { id: 2, name: 'Mountain Standard Time' },
                            { id: 3, name: 'Central Standard Time' },
                            { id: 4, name: 'Eastern Standard Time' }];

        $scope.isTermsAccepted = { checked: false };

        $scope.ngfunction = function(val){
            document.getElementById("selectedTimeZone").value = val;
            $scope.registration.timeZone = val;
        };

        //$scope.selectableNames =  [
        //	{ name : "Direct TV", role : ""},
        //	{ name : "Time Warner Cable", role : ""},
        //	{ name : "Verison Fios", role : ""},
        //	{ name : "AT&T UVerse", role : ""},
        //];
        //$scope.someSetModel = 'Mauro';

        console.log("Register Called");

        /**
        * Callback for register message.
        *
        * @method OnRegister
        */
    function OnRegister(data) {




    }

        /**
    * Action that send the data of new user to the server.
    *
    * @method doRegister
    */
    $scope.doRegister = function () {
        var rules = {};
        var messages = {};

        $.validator.addMethod(
            "isTimeZoneSelected",
            function (value, element) {
                console.log("registration.timeZone", $scope.registration.timeZone)
                if ($scope.registration.timeZone == null)
                    return false;
                else
                    return true;
            },
            "Time Zone is required"
        );

        rules.firstName = { required : true };
        rules.lastName = { required : true };
        rules.email = {
            required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            }
            , email: true
        };
        rules.password = { required : true };
        rules.confirm = { required: true, equalTo: ".accountPassword" };
        rules.selectedTimeZone = { isTimeZoneSelected: true };

        messages.firstName = { required : "First Name is required" };
        messages.lastName = { required : "Last Name is required" };
        messages.email = { required : "Email is required", email: "Invalid email" };
        messages.password = { required: "Password is required" };
        messages.confirm = { required:"Confirm Password is required", equalTo: "Password does not match" };
        messages.selectedTimeZone = { isTimeZoneSelected: "Time Zone is required" };

        if (!$scope.isTermsAccepted.checked)
        {
            simpalTek.helper.messageCenterService("Before creating an account you must agree to \"Terms of Service & Privacy Policy\" ", simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown);

            angular.element(document).ready(function () {
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                }, 500);
            });

            return;
        }
        var validator = new simpalTek.helper.jQueryFormValidator("register", rules, messages);

        if (validator.validate()) {

                localStorage.setItem("email", $scope.loginData.email);

                //var credentials = base64_encode(Sha256.hash($scope.registration.email + new Date().getTime()));
                //localStorage.setItem("credentials", credentials);

                //$scope.registration.credentials = credentials;

                $ionicLoading.show({
                    template: 'Registering account.  Please wait.'
                });

                authService.saveRegistration($scope.registration).then(function (response) {
                    console.log("registration", response)

                    $ionicLoading.hide();

                    if (response.data.status == 0) {
                        $scope.savedSuccessfully = true;
                        //simpalTek.helper.messageCenterService("Your account has been registered successfully.", simpalTek.Enum.messageType.success, simpalTek.Enum.messageStatus.next);
                        simpalTek.global.userTimeZone = $scope.registration.timeZone;

                        //$ionicLoading.hide();
                        $location.path('/signin');

                        //$scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
                        startTimer();
                    }
                    else {

                        if (response.data.result == "EMAIL EXIST") {

                            navigator.notification.alert("Failed to register user.  There is an account with this email that has been already registered.", function () {

                            }, "Registration", "Ok");

                        }
                    }


                },
            function (response) {

                    //$ionicLoading.show({
                    //    template: 'Failed to register user'
                    //});
                    $ionicLoading.hide();

                    navigator.notification.alert("Failed to register user.", function () {

                    }, "Registration", "Ok");

                });
            };

        }


        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $ionicLoading.hide();
                //$location.path('/signin');

                simpalTek.helper.messageCenterService("Your account has been registered successfully.", simpalTek.Enum.messageType.success, simpalTek.Enum.messageStatus.shown);

                //$state.go("app.signin")
            }, 500);
        }

        //$scope.onTimeZoneSelect = function (newValue, oldValue) {
        //    if (newValue) {
        //        simpalTek.global.userTimeZone = newValue.id;
        //    }
        //}

        //$scope.registration.timeZone = $scope.timeZones[2];

      //$socket.emit('user:register', $scope.loginData, OnRegister);
	  //OnRegister();


}]);


app.controller('UserCtrl', [

    '$scope',
    '$timeout',
    '$rootScope',
    '$location',
    '$ionicModal',
    '$account',
    '$ionicLoading',
    '$state',
    'authService',
    '$http',
    'ngAuthSettings',
    '$ionicActionSheet',
    'localStorageService',
    '$ionicPopup',
    '$document',
    '$socket',
    '$ionicPlatform',
    'Geofence',
    'messageCenterService',
    '$fileLogger',
    function ($scope, $timeout, $rootScope, $location, $ionicModal, $account, $ionicLoading, $state, authService, $http, ngAuthSettings, $ionicActionSheet, localStorageService, $ionicPopup, $document, $socket, $ionicPlatform, Geofence, messageCenterService, $fileLogger) {

        //$ionicLoading.show({
        //    template: 'Loading...'
        //});

        var lastDigestRun = 0;
        var firstDigestRun = 0;
        var promiseTokenExpireInterval = null;
        var now = Date.now();
        var tokenExpireTime = 0;
        var _lastTick;
        var currentUrl = $location.url();
        var authData = simpalTek.global.localStorage.get('authorizationData')
        var loginData = simpalTek.global.localStorage.get("loginData");
        var deviceInfo = ionic.Platform.device();
        var userDb = simpalTek.global.localStorage.get('UserDb');


        $rootScope.navBgColor = '';

        $ionicPlatform.ready(function () {

            deviceInfo = ionic.Platform.device();

            //if (window.StatusBar) {
            //    window.StatusBar.overlaysWebView(true);
            //    window.StatusBar.styleDefault();
            //}

            if (window.cordova) {
                if (ionic.Platform.isAndroid()) {
                    ionic.Platform.fullScreen();
                    if (window.StatusBar) {

                        ionic.Platform.showStatusBar(false)
                    }
                }
                else {
                    window.StatusBar.styleDefault();
                    ionic.Platform.showStatusBar(true)
                }
            }

        });

        $scope.loginData = {};
        $scope.loginData.useRefreshTokens = true;

        $scope.resetPassword = {
            Password: "",
            Confirm: "",
            ResetCode : ""
        };

        //simpalTek.helper.messageCenterService("Test", simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown);

        if (loginData) {

            if (loginData.rememberEmail == true) {
                $scope.loginData.userName = loginData.userName;
                $scope.loginData.rememberEmail = loginData.rememberEmail;
            }

            if (loginData.rememberPassword == true) {
                $scope.loginData.password = loginData.password;
                $scope.loginData.rememberPassword = loginData.rememberPassword;
            }

        }


        //if (authService.authentication.isAuth ) {

        //    //if (authData != null) {
        //    //    if (authData.userName)
        //            $socket.emit('join', { email: authService.authentication.userName });
        //    //}

        //    //TMKmobile.Common.showLoader(true);
        //    //window.TMKmobile.Common.redirect("event/home");

        //    authService.refreshToken().then(function (response) {

        //        sessionStart(response.expires_in);

        //        //data.preLoad().then(function (response) {

        //        //    TMKmobile.Common.showLoader(false);
        //        //    $rootScope.$broadcast("loadNavigationList");

        //        //});
        //    },
        //    function (err) {
        //        authService.logOut();
        //    });
        //}
        ////else {

        ////    if($scope.modal != null)
        ////        $scope.modal.hide();

        ////    $ionicModal.fromTemplateUrl('templates/signin.html', {
        ////        scope: $scope,
        ////        animation: 'slide-in-up'
        ////    }).then(function (modal) {

        ////        $scope.modal = modal;

        ////        modal.scope.loginData.userName = "chris28@gmail.com";
        ////        modal.scope.loginData.password = "password";

        ////        $ionicLoading.hide();
        ////        $scope.modal.show();


        ////    });

        ////    authService.logOut();


        ////}

        //$document.find('body').on('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart tap', (function () { lastDigestRun = Date.now(); })); //monitor events

        //function detectWakeFromSleep() {
        //    var now = new Date().getTime();
        //    var delta = now - _lastTick;

        //    if (delta > ngAuthSettings.deviceSleepLapseIntervalThreshold) {
        //        if (now > tokenExpireTime) {
        //            //TMKmobile.Helper.messageCenterService(TMKmobile.Const.DEFAULT_SESSION_TIMEOUT_MSG, TMKmobile.Enum.messageType.danger, TMKmobile.Enum.messageStatus.next, TMKmobile.Config.Settings.resources.AppSetting.messageCenterTimeOut);
        //            //authService.logOut();
        //        }
        //    }
        //    _lastTick = now;

        //};

        $rootScope.showHelp = function () {

            if (window.cordova) {
                navigator.notification.confirm(
                    'This will launch an internet browser window.  Do you want to continue?', // message
                     (function (buttonIndex) {

                         if (buttonIndex == 2) {

                             //if (ionic.Platform.isAndroid())
                             //    navigator.app.loadUrl(simpalTek.global.appSettings.faqUrl, { openExternal: true });
                             //else
                             //    window.open(simpalTek.global.appSettings.faqUrl, '_system', 'location=yes');

                             cordova.InAppBrowser.open("http://www.nexxgarage.com/support", '_system', 'location=yes')
                         }

                     }),            // callback to invoke with index of button pressed
                    'Nexx Garage Help',           // title
                    ['No', 'Yes']     // buttonLabels
                );
            }


        }

        $rootScope.showSettings = function () {

            var userDb = simpalTek.global.localStorage.get('UserDb');

           
            if (userDb.registeredDevices.length == 0) {

                navigator.notification.confirm(
                    'You cannot set settings since you do not have any device registered.  If you were granted access from another user than you need to go to User Access and select the garage name under "Other\'s Granted Access To You" located at the bottom of the screen.  Would you like to go to the User Access screen?', // message
                     function (buttonIndex) {
                         if (buttonIndex == 2) {
                             //window.setTimeout(function () {
                                 $state.go("app.deviceUserManager");
                             //}, 500)

                         }
                     },            // callback to invoke with index of button pressed
                    'Settings',           // title
                    ['No', 'Yes']     // buttonLabels
                );

                return;
            }

         
            $ionicModal.fromTemplateUrl('templates/notificationSettings.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                $scope.settingsModal = modal;


                $scope.settingsModal.show();

            });
        }

        $rootScope.showPrivacy = function () {
            $ionicModal.fromTemplateUrl('templates/privacy.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                $scope.privacyModal = modal;


                $scope.privacyModal.show();

            });
        }

        $rootScope.showTerms = function () {
            $ionicModal.fromTemplateUrl('templates/terms.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                $scope.termsModal = modal;


                $scope.termsModal.show();

            });
        }

        $rootScope.sendLogList = function () {

            ionic.Platform.ready(function () {
                $fileLogger.getLogfile().then(function (l) {
                    console.log("$fileLogger.getLogfile()", l)
                    $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/SendMail", { SenderEmail: $scope.loginData.userName, RecipientEmail: "chris28@gmail.com", Subject: "Log List from " + $scope.loginData.userName, Message: l })
                });

                if (window.BackgroundGeolocation)
                    window.BackgroundGeolocation.emailLog($scope.loginData.userName);
            });

            //navigator.notification.alert("Log sent successfully.", function () {
            //    $rootScope.hideLogList();
            //}, "Send Log", "Ok");
        }

        $rootScope.sendDeviceLog = function () {

            ionic.Platform.ready(function () {
                //$fileLogger.getLogfile().then(function (l) {
                //console.log("$fileLogger.getLogfile()", l)
                simpalTek.helper.sendMail("techlogs@simpaltek.com", simpalTek.global.logger.log, "Device Info Log")
                //$http.post(ngAuthSettings.apiDomainServiceBaseUri + "/SendMail", { SenderEmail: $scope.loginData.userName, RecipientEmail: "support@nexxgarage.com", Subject: "Device Info Log from " + $scope.loginData.userName, Message: JSON.stringify(simpalTek.global.logger.log) })
                //});

            });

            navigator.notification.alert("Device Info Log sent successfully.", function () {

            }, "Send Device Info Log", "Ok");
        }

        $rootScope.sendGeoFenceLog = function () {

            if (window.BackgroundGeolocation)
                window.BackgroundGeolocation.emailLog("techlogs@simpaltek.com");

        }

        $rootScope.showLogList = function () {
            $ionicModal.fromTemplateUrl('templates/logList.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                $scope.logModal = modal;

                $scope.logList = simpalTek.global.logger.log;

                $scope.logModal.show();

            });
        }

        $rootScope.hideLogList = function () {

            $scope.logModal.hide();

            $scope.logModal.remove();
        }

        $scope.forgotPassword = function () {

            messageCenterService.remove();

            if ($scope.loginData.userName == "" || $scope.loginData.userName == undefined) {
                simpalTek.helper.messageCenterService("You must enter your account email to continue.", simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown);

                return;
            }
            authService.forgotPassword({ userName: $scope.loginData.userName }).then(function (forgotPasswordResponsePayload) {

            });

            $state.go('resetPassword');

            /*$ionicModal.fromTemplateUrl('templates/resetPassword.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                alert("svsvsv");

                $scope.modalResetPassword = modal;


                $scope.modalResetPassword.show();

            });*/
        }

        $rootScope.showRegisterDeviceWizard = function (deviceId, vendorDeviceId) {
            $ionicModal.fromTemplateUrl('templates/registerDeviceWizard.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {

                $scope.modalRegisterDeviceWizard = modal;
                $scope.editDeviceId = deviceId;
                $scope.editVendorDeviceId = vendorDeviceId;

                $scope.modalRegisterDeviceWizard.show();

            });
        }

        $scope.doResetPassword = function () {
            $scope.resetPassword.userName = $scope.loginData.userName;
            console.log("resetPassword", $scope.resetPassword)


            var rules = {};
            var messages = {};

            rules.code = { required: true };
            rules.password = { required: true };
            rules.confirm = { required: true, equalTo: ".newPassword" };

            messages.code = { required: "Password reset code is required"};
            messages.password = { required: "Password is required" };
            messages.confirm = { required: "Confirm Password is required", equalTo: "Password does not match" };



            //if ($scope.resetPassword.ResetCode == "") {
            //    $ionicPopup.alert({
            //        title: 'Validate Error',
            //        template: 'Password reset code is required.'
            //    });

            //    return;
            //}
            //else if ($scope.resetPassword.Password == "") {
            //    $ionicPopup.alert({
            //        title: 'Validate Error',
            //        template: 'Password is required.'
            //    });

            //    return;
            //}
            //else if ($scope.resetPassword.Password != $scope.resetPassword.Confirm) {
            //    $ionicPopup.alert({
            //        title: 'Validate Error',
            //        template: 'Password confirm does not match password.'
            //    });

            //    return;
            //}

            var validator = new simpalTek.helper.jQueryFormValidator("resetPassword", rules, messages);

            if (validator.validate()) {

                authService.resetPassword($scope.resetPassword).then(function (response) {

                    if (response.status == "1" && response.result == "PASSWORD RESET CODE EXPIRED") {
                        alert("The reset code you provided has expired.  Please provide a new password reset code.");
                    }
                    else if (response.status == "1" && response.result == "INVALID")
                        alert("The information you enter is invalid.  Please try again.");
                    else {

                        alert("Your password has been reset")

                        $scope.loginData.password = "";
                        $("#password").focus();

                        $scope.modalResetPassword.hide();
                    }



                },
                function (response) {

                    $ionicLoading.show({
                        template: 'Password reset failed.'
                    });

                    $ionicLoading.hide();

                });

            }

        }

        $rootScope.openContactUsEmail = function () {

            if(window.plugins && window.plugins.emailComposer) {
                window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                    console.log("Response -> " + result);
                },
                "Nexx Garage Contact Us", // Subject
                "",                      // Body
                ["techsupport@simpaltek.com"],    // To
                null,                    // CC
                null,                    // BCC
                false,                   // isHTML
                null,                    // Attachments
                null);                   // Attachment Data
            }

        }

        $scope.$on('sessionStart', function (event, args) {
            sessionStart(args.tokenExpireIn)
        });

        function sessionStart(tokenExpireIn) {
            var now = Date.now();
            tokenExpireTime = now + (tokenExpireIn * 1000);
            var tokenReminderTime = tokenExpireTime - (ngAuthSettings.tokenReminderMeIn * 1000);
            var lastActivityTime = 0;
            var firstActivityTime = 0;
            var sessionStartTime = now;

            //console.log("session start")
            //authService.refreshToken().then(function (response) {
            //    console.log("refreshToken response", response)
            //},
            //    function (err) {
            //    console.log("refreshToken response err", err)
            //});

            promiseTokenExpireInterval = setInterval(function () {

                var now = Date.now();

                detectWakeFromSleep();

                if (lastDigestRun != 0) {
                    lastActivityTime = now - lastDigestRun;

                    if (firstDigestRun == 0)
                        firstDigestRun = lastDigestRun;

                    firstActivityTime = now - firstDigestRun;

                    if (firstDigestRun != 0) {
                        firstActivityTime += firstDigestRun - sessionStartTime;
                    }
                }

                if (firstActivityTime > ((tokenExpireIn - ngAuthSettings.tokenReminderMeIn) * 1000)) {

                    authService.refreshToken().then(function (response) {
                        var now = Date.now();

                        tokenExpireIn = response.expires_in;
                        tokenExpireTime = now + (tokenExpireIn * 1000);
                        tokenReminderTime = tokenExpireTime - (ngAuthSettings.tokenReminderMeIn * 1000);
                        lastDigestRun = 0;
                        firstDigestRun = 0;
                        lastActivityTime = 0;
                        firstActivityTime = 0;
                        sessionStartTime = now;
                    },
                function (err) {
                        authService.logOut();
                    });

                }
                else if (now > tokenExpireTime) {

                    //TMKmobile.Helper.messageCenterService(TMKmobile.Const.DEFAULT_SESSION_TIMEOUT_MSG, TMKmobile.Enum.messageType.danger, TMKmobile.Enum.messageStatus.next, TMKmobile.Config.Settings.resources.AppSetting.messageCenterTimeOut);
                    authService.logOut();
                }
            }, ngAuthSettings.sessionInterval);

        }

        $scope.$on('sessionStop', function (event, args) {
            $scope.list = [];
            $scope.isLoggedIn = false;
            clearInterval(promiseTokenExpireInterval);
        });

        $scope.$on('refreshMenu', function (event, args) {
            $scope.list = [];
            $scope.isLoggedIn = false;
            clearInterval(promiseTokenExpireInterval);
        });

        $scope.logOutIn = function () {
            $timeout(function () {
                var actionSheet = $ionicActionSheet.show({
                    //buttons: [
                    //    { text: '<b>Share</b> This' },
                    //    { text: 'Move' }
                    //],
                    destructiveText: 'Log Out',
                    titleText: 'Are you sure you want to Log Out?',
                    cancelText: 'Cancel',
                    //cancel: function () {
                    // add cancel code..
                    //},
                    buttonClicked: function (index) {
                        return true;
                    },
                    destructiveButtonClicked: function () {

                        authService.logOut(true);

                        if (window.cordova) {
                            //Geofence.stop();
                            simpalTek.helper.geoFence().stop();
                        }

                        //$state.go("app.signin");


                        $timeout(function () {
                            //$location.path('/signin');
                            $state.go("signin");
                        }, 500);

                        return true;
                    }
                });
            }, 100);


        };

        $scope.showAdminMenuItem = function () {
            try {
                if ($scope.loginData.userName.toLowerCase() == "chris28@gmail.com" || $scope.loginData.userName.toLowerCase() == "scottvu@gmail.com" || $scope.loginData.userName.toLowerCase() == "lanikv@gmail.com")
                    return true;
            }
            catch (e) { };

                return false;
        }

        $scope.showPassword = function () {
            $("#password").attr("type", "text");
        }

        $scope.hidePassword = function () {
            $("#password").attr("type", "password");
        }

        $scope.doSignIn = function () {
            var rules = {};
            var messages = {};
            var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
            var appUpdateNotice = simpalTek.global.localStorage.get("appUpdateNotice")
            var deviceGeoLoc, deviceGeoLat, deviceGeoLon, deviceGeoNickName

            if (simpalTek.helper.networkConnection() == "No network connection") {
                navigator.notification.alert("There is currenlty no internet connection at this time.  Please try again.", function () {

                }, "No internet connection", "Ok");

                return;
            }

            if (window.cordova) {
                //var appVersion = AppVersion.version;

                if (appUpdateNotice != null) {
                    console.log("currentAppVersion", appUpdateNotice.currentAppVersion);

                    if (parseInt(AppVersion.version.replace(/\./g, "")) < parseInt(appUpdateNotice.currentAppVersion.replace(/\./g, "")) && appUpdateNotice.mandatory) {
                        navigator.notification.alert("Nexx Garage App is required to update.  Go to the App Store to download to latest App.", function () {
                            navigator.app.exitApp();
                        }, "Application Update Required", "Ok");

                        return;
                    }
                }
            }


            if (!currentNexxGarage) {
                currentNexxGarage = {};
                currentNexxGarage.deviceId = "";
                currentNexxGarage.vendorDeviceId = "";
                currentNexxGarage.garageDoorState = "Unknown";
            }

            rules.userName = {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }, email: true
            };
            rules.password = {required : true};

            messages.userName = { required : "Email is required", email: "Invalid Email" };
            messages.password = { required : "Password is required" };

            var validator = new simpalTek.helper.jQueryFormValidator("signin", rules, messages);

            if (validator.validate()) {

                $ionicLoading.show({
                    template: 'Validating credentials.  Please wait.'
                });


                authService.login($scope.loginData).then(function (loginResponsePayload) {

                    var params = {timeZoneId: simpalTek.global.userTimeZone};
                    simpalTek.global.localStorage.set("loginData", { userName: ($scope.loginData.rememberEmail)?$scope.loginData.userName:null, password: ($scope.loginData.rememberPassword)?$scope.loginData.password:null, rememberEmail: $scope.loginData.rememberEmail, rememberPassword: $scope.loginData.rememberPassword });

                    $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/?parameter=' + JSON.stringify(params)).then(function (response) {
                        console.log("UserPreloadData payload", response);

                        simpalTek.global.appSettings = response.data.result.appSettings;

                        //response.data.result.preference.geofences = JSON.parse(response.data.result.preference.geofences);

                        simpalTek.global.localStorage.set("UserDb", response.data.result);

                        document.addEventListener("deviceready", function () {
                            deviceInfo = ionic.Platform.device();

                            if (window.cordova) {
                                if (response.data.result.preference) {
                                    //if (response.data.result.preference.proximityMonitorAuto) {
                                    //    Geofence.start();
                                    //}

                                    if (response.data.result.preference.localNotification || response.data.result.preference.garageOpenNotification) {
                                        if (window.cordova.plugins.notification) {
                                            window.cordova.plugins.notification.local.registerPermission(function (granted) {
                                            });
                                        }
                                    }
                                }
                            }
                        }, false);

                        //$socket.emit('join', {email: loginResponsePayload.userName});


                        if (response.data.result.registeredDevices.length > 0) {

                            currentNexxGarage.garageDoorState = response.data.result.registeredDevices[0].currentGarageDoorState;
                            currentNexxGarage.vendorDeviceId = response.data.result.registeredDevices[0].deviceName;
                            currentNexxGarage.deviceId = response.data.result.registeredDevices[0].deviceId;

                            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
                        }
                        else if (response.data.result.devicesAccess.length > 0) {
                            currentNexxGarage.deviceId = response.data.result.devicesAccess[0].deviceId;
                            currentNexxGarage.vendorDeviceId = response.data.result.devicesAccess[0].vendorDeviceId
                            currentNexxGarage.garageDoorState = response.data.result.devicesAccess[0].currentDoorState;
                            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
                        }

                        //Geofence.init();
                        simpalTek.helper.configUserMobileDeviceSetting();

                        if (response.data.result.preference) {
                            if (response.data.result.preference.proximityMonitorAuto) {

                                //only call the #config once
                                //if (simpalTek.global.bgGeoConfigured == false)
                                    simpalTek.helper.geoFence().config();

                                setTimeout(function () {
                                    simpalTek.helper.geoFence().start();
                                }, 10000)
                            }
                        }
                           //  alert(response.data.result.registeredDevices.length);
                        if (response.data.result.registeredDevices.length == 0 && response.data.result.devicesAccess.length == 0)
                            $location.path('/app/registeredDevices');
                        else
                            $location.path('/app/garageDoorControl');

                         $ionicLoading.hide();

                    }, function (err) {

                        $ionicPopup.alert({
                            title: 'Application Error',
                            template: 'There is an issue retrieving your information.  Please try again.'
                        });
                    });

                    //$location.path('/');

                },
                function (err) {

                    authService.authentication.isAuth = false;

                    $ionicPopup.alert({
                        title: 'Invalid credential',
                        template: 'Either the email or password you entered is incorrect.  Please try again.'
                    });

                    $ionicLoading.hide();
                });
            }


	}


    /**
    * Callback for Login emit message
    *
    * @method OnLogin
    */
    function OnLogin(data){
      if(data.code != 200){
        return;
      }
      console.log("log in");
      $account.setLogged();
      $ionicLoading.hide();
    }

    function getMediaURL(s) {
        if (device.platform.toLowerCase() === "android") return "/android_asset/www/" + s;
        return s;
    }

    //$scope.$on('setUserGeoLoc', function (event, args) {
    //    var userDb = simpalTek.global.localStorage.get('UserDb');
    //    var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
    //    var pushRegisterFailed = false;
    //    var mainUserRegisterDevice = null;
    //    var notification = simpalTek.global.localStorage.get('Notification');

    //    try{
    //        deviceInfo = ionic.Platform.device();

    //        simpalTek.global.pushNotification = PushNotification.init({
    //            android: {
    //                senderID: userDb.appSettings.gcmSenderId,
    //                alert: true,
    //                //badge: true,
    //                sound: true,
    //                //vibrate: true,
    //                clearBadge: true
    //            },
    //            //browser: {
    //            //    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
    //            //},
    //            ios: {
    //                alert: true,
    //                //badge: true,
    //                sound: true,
    //                clearBadge: true
    //            },
    //            windows: {}
    //        });

    //        simpalTek.global.pushNotification.on('registration', function (data) {
    //            var $http = angular.element(document.body).injector().get('$http');
    //            var ngAuthSettings = angular.element(document.body).injector().get('ngAuthSettings');
    //            var currentRegisterId = "";
    //            var newRegisterId = "";

    //            try {

    //                ionic.Platform.ready(function () {
    //                    notification = simpalTek.global.localStorage.get('Notification');
    //                    deviceInfo = ionic.Platform.device();

    //                    if (notification == null) {
    //                        notification = {};

    //                        notification.oldRegisterId = "";
    //                        notification.newRegisterId = "";
    //                    }

    //                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification registration " + JSON.stringify(data));

    //                    newRegisterId = data.registrationId;
    //                    currentRegisterId = notification.newRegisterId;

    //                    if (notification.oldRegisterId != newRegisterId)
    //                        notification.oldRegisterId = currentRegisterId;

    //                    notification.newRegisterId = newRegisterId;

    //                    $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/MobileDevice", { mobileDeviceType: deviceInfo.platform, registeredId: data.registrationId, deviceId: (simpalTek.global.deviceInfo.uuid == "") ? deviceInfo.uuid : simpalTek.global.deviceInfo.uuid, deviceInfo: JSON.stringify(deviceInfo), oldRegisterId: notification.oldRegisterId })

    //                    simpalTek.global.localStorage.set('Notification', notification);

    //                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "notification object " + JSON.stringify(notification));
    //                });
    //            }
    //            catch (e) {
    //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification init : " + e);
    //            }
    //        });

    //        simpalTek.global.pushNotification.on('notification', function (data) {

    //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification data " + JSON.stringify(data));

    //            switch (data.title) {
    //                case "Nexx Garage Alert Open":

    //                    simpalTek.helper.getUserPreloadData(function () {
    //                        simpalTek.global.isDataRefreshed = true;
    //                    }, function (error) {
    //                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Push notify Open UserPreloadData : " + JSON.stringify(error));
    //                    })

    //                    break;
    //                case "Nexx Garage Alert Close":

    //                    simpalTek.helper.getUserPreloadData(function () {
    //                        simpalTek.global.isDataRefreshed = true;
    //                    }, function (error) {
    //                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData : " + JSON.stringify(error));
    //                    })

    //                    break;

    //                default:

    //                    if (data.additionalData.activityType) {

    //                        switch (data.additionalData.activityType.toLowerCase()) {
    //                            case "open":
    //                            case "close":
    //                                simpalTek.helper.getUserPreloadData(function () {
    //                                    simpalTek.global.isDataRefreshed = true;
    //                                }, function (error) {
    //                                    if (data.additionalData.activityType == "Open")
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Open UserPreloadData : " + error);
    //                                    else (data.additionalData.activityType == "Close")
    //                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData : " + error);
    //                                });

    //                                break;
    //                            case "wakeup":
    //                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify wakeup device");
    //                                break;
    //                            case "findme":

    //                                break;
    //                        }


    //                    }

    //            }

    //            simpalTek.global.pushNotification.finish(function () {
    //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "processing of push data is finished for ID = " + data.additionalData.notId);
    //            }, function () {
    //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "something went wrong with push.finish for ID = " + data.additionalData.notId);
    //            }, data.additionalData.notId);

    //        });

    //        simpalTek.global.pushNotification.on('error', function (e) {
    //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification error " + JSON.stringify(e));
    //        });

    //        if (userDb.devicesAccess.length > 0) {
    //            var autoOpenLocations = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() && item.ownerGrantAutoDoorOpen && item.isMain })

    //            //remove all existing geofence entry
    //            window.BackgroundGeolocation.removeGeofences(function () {

    //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'all geofences removed');

    //                if (userDb.preference) {

    //                    if (userDb.preference.proximityMonitorAuto) {
    //                        //re-add the geofence
    //                        if (autoOpenLocations.length > 0) {
    //                            for (i = 0; i < autoOpenLocations.length; i++) {

    //                                deviceGeoNickName = autoOpenLocations[i].deviceId;
    //                                deviceGeoLoc = JSON.parse(autoOpenLocations[i].ownerDeviceGPSLoc);
    //                                deviceGeoLat = deviceGeoLoc.latitude;
    //                                deviceGeoLon = deviceGeoLoc.longitude;

    //                                if (autoOpenLocations[i].ownerGrantAutoDoorOpen) {
    //                                    simpalTek.helper.geo().add(deviceGeoNickName, deviceGeoLat, deviceGeoLon,
    //                                        function () {

    //                                        }
    //                                    );

    //                                    if (autoOpenLocations[i].userType == 'O')
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for owner - ' + deviceGeoNickName);
    //                                    else if (autoOpenLocations[i].userType == 'G')
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for guest - ' + deviceGeoNickName);
    //                                    else
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for unknown - ' + deviceGeoNickName);

    //                                }
    //                                else {
    //                                    if (autoOpenLocations[i].userType == 'O')
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for owner - ' + deviceGeoNickName);
    //                                    else if (autoOpenLocations[i].userType == 'G')
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for guest - ' + deviceGeoNickName);
    //                                    else
    //                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for unknown - ' + deviceGeoNickName);
    //                                };

    //                            }
    //                        }


    //                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- startGeofences');

    //                        $timeout(function () {
    //                            Geofence.start();

    //                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geo fence monitor started");
    //                        }, 3000);

    //                    }
    //                }
    //            });

    //        }

    //    } catch (e) {
    //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'setUserGeoLoc: ' + e);
    //    }
    //});

    /**
    * Open a modal to show the register form
    *
    * @method openModal
    */
    $scope.openModal = function() {
      $scope.modal.show();
    };

    /**
    * Close the modal of register form.
    *
    * @method closeModal
    */
    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.finishRegisterDevice = function () {
        $timeout(function () {
            $location.path('/app/garageDoorControl');
        }, 500)

    }

	$scope.goRegister = function(){

		//console.log("register", $state);
		//$scope.modal.hide();
		//$state.go("app.register");
	    $location.path('/app/register');

	}



}]);
