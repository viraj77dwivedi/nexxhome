﻿app.controller('AboutDeviceCtrl', [

    '$scope',
    '$http',
    '$ionicModal',
    'ngAuthSettings',
    'localStorageService',
    '$filter',
    '$interval',
    '$timeout',
    '$rootScope',
    '$ionicPopup',
    function ($scope, $http, $ionicModal, ngAuthSettings, localStorageService, $filter, $interval, $timeout, $rootScope, $ionicPopup) {
       
        var userDb = simpalTek.global.localStorage.get('UserDb');
        var intervalCounter = 0;
        var data = {};
        var deviceFirmwareUpdate = [];
        var firmwareStatusInterval;

        $scope.devicesFirmware = [];

        data.vendorDeviceId = simpalTek.global.localStorage.get("currentNexxGarage").vendorDeviceId;

        if (window.cordova) {
            $scope.appVersion = AppVersion.version;
        }
        
        $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserRegisterDeviceInfo').then(function (response) {

            var userDevicesFirmwareInfo = response.data.result;
            var deviceFirmwareInfo = {};

            for (var i = 0; i < userDevicesFirmwareInfo.length; i++) {

                deviceFirmwareInfo.nexxGarageFirmwareVersion = userDevicesFirmwareInfo[i].currentFirmwareVersion;
                deviceFirmwareInfo.deviceName = userDevicesFirmwareInfo[i].deviceName;
                deviceFirmwareInfo.vendorDeviceId = userDevicesFirmwareInfo[i].vendorDeviceId;
                deviceFirmwareInfo.firmwareVersion = userDevicesFirmwareInfo[i].firmwareVersion;

                if (userDevicesFirmwareInfo[i].firmwareUpdateDate != null)
                    deviceFirmwareInfo.lastFirewareUpdateDate = $filter('utcToLocal')(userDevicesFirmwareInfo[i].firmwareUpdateDate, "MMMM d, y");//new Date(userDevicesFirmwareInfo[i].firmwareUpdateDate).toLocaleString();
               
                if (parseInt(userDevicesFirmwareInfo[i].firmwareVersion.replace(/\./g, '')) >= parseInt(userDevicesFirmwareInfo[i].currentFirmwareVersion.replace(/\./g, ''))) {
                    deviceFirmwareInfo.firmwareStatus = "current";
                    simpalTek.global.firmwareUpdating = false;
                }
                else if (userDevicesFirmwareInfo[i].firmwareVersion != userDevicesFirmwareInfo[i].currentFirmwareVersion)
                    deviceFirmwareInfo.firmwareStatus = "new";

                $scope.devicesFirmware.push(deviceFirmwareInfo);

                deviceFirmwareInfo = {};
            }

        });

        $rootScope.installFirmware = function (vendorDeviceId) {

            var data = {};
            data.vendorDeviceId = vendorDeviceId;
                        
            if (window.cordova) {
                navigator.notification.confirm(
                    'You are about to install an update to your Nexx Garage device.  Tap on  the "Install" button to continue.', // message
                     (function (buttonIndex) {

                         
                         if (buttonIndex == 2) {

                            //if(deviceFirmwareUpdate.filter(function (x) { return x.vendorDeviceId == vendorDeviceId}).length == 0)
                            //deviceFirmwareUpdate.push(vendorDeviceId);

                            $scope.devicesFirmware.filter(function (x) { return x.vendorDeviceId == vendorDeviceId })[0].firmwareStatus = "updating";

                             $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceFirmware', data).success(function (response) {

                                 //simpalTek.helper.messageCenterService("The new firmware is in the process of being installed onto your device.  Check the <a href='#/app/aboutDevice'>About Device</a> page for the status.", simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown, 15000);
                                 //simpalTek.global.firmwareUpdating = true;
                                 //$scope.firmwareStatus = "updating";

                                 firmwareStatusInterval = $interval(function () {
                                     
                                     $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserRegisterDeviceInfo').then(function (response) {

                                         var userDevicesFirmwareInfo = response.data.result;
                                         var currentDeviceFirmware = userDevicesFirmwareInfo.filter(function (x) { return x.firmwareVersion == x.currentFirmwareVersion });
                                         var deviceFirmwareInfo = {};
                                         $scope.devicesFirmware = [];

                                         for (var i = 0; i < userDevicesFirmwareInfo.length; i++) {
                                             deviceFirmwareInfo.nexxGarageFirmwareVersion = userDevicesFirmwareInfo[i].currentFirmwareVersion;
                                             deviceFirmwareInfo.deviceName = userDevicesFirmwareInfo[i].deviceName;
                                             deviceFirmwareInfo.vendorDeviceId = userDevicesFirmwareInfo[i].vendorDeviceId;
                                             deviceFirmwareInfo.firmwareVersion = userDevicesFirmwareInfo[i].firmwareVersion;

                                             if (userDevicesFirmwareInfo[i].firmwareUpdateDate != null)
                                                 deviceFirmwareInfo.lastFirewareUpdateDate = $filter('utcToLocal')(userDevicesFirmwareInfo[i].firmwareUpdateDate, "MMMM d, y");//new Date(userDevicesFirmwareInfo[i].firmwareUpdateDate).toLocaleString();

                                             
                                             if (userDevicesFirmwareInfo[i].firmwareVersion == userDevicesFirmwareInfo[i].currentFirmwareVersion ) {
                                                 deviceFirmwareInfo.firmwareStatus = "current";
                                             }
                                             else if (userDevicesFirmwareInfo[i].vendorDeviceId == vendorDeviceId)
                                                 deviceFirmwareInfo.firmwareStatus = "updating";
                                             else if (userDevicesFirmwareInfo[i].vendorDeviceId != vendorDeviceId)
                                                 deviceFirmwareInfo.firmwareStatus = "new";

                                             $scope.devicesFirmware.push(deviceFirmwareInfo);
                                                                                          
                                             deviceFirmwareInfo = {};
                                         }

                                         if ($scope.devicesFirmware.filter(function (x) { return x.firmwareStatus == "updating" }).length == 0) {
                                             $interval.cancel(firmwareStatusInterval);
                                             //return;
                                         }
                                     });

                                     

                                 }, 10000);

                                 $timeout(function () {
                                     $interval.cancel(firmwareStatusInterval);

                                     if (simpalTek.global.firmwareUpdating) {
                                         if (simpalTek.global.appSettings.currentFirmwareVersion != $scope.nexxGarageFirmwareVersion) {
                                             $ionicPopup.confirm({
                                                 title: 'Firmware update failed',
                                                 template: 'There was an issue updating your firmware.  Would you like to retry?',
                                                 buttons: [
                                                            { text: 'Cancel' },
                                                              {
                                                                  text: '<b>Retry</b>',
                                                                  type: 'button-positive',
                                                                  onTap: function (e) {
                                                                      $rootScope.installFirmware();
                                                                  }
                                                              }
                                                 ]
                                             });
                                         }
                                     }
                                     
                                 }, 300000);

                             }).error(function (err, status) {

                                 $ionicPopup.confirm({
                                     title: 'Firmware update failed',
                                     template: 'There was an issue updating your firmware.  Would you like to retry?',
                                     buttons: [
                                                { text: 'Cancel' },
                                                  {
                                                      text: '<b>Retry</b>',
                                                      type: 'button-positive',
                                                      onTap: function (e) {
                                                          $rootScope.installFirmware();
                                                      }
                                                  }
                                      ]
                                 });

                             });
                         }

                     }),            // callback to invoke with index of button pressed
                    'Install New Firmware',           // title
                    ['Cancel', 'Install']     // buttonLabels
                );
            }
            return false;
        }


        
        $timeout(function () {
            $interval.cancel(firmwareStatusInterval);
        }, 300000);


 }])