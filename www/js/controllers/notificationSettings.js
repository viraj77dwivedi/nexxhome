﻿app.controller('NotificationSettingsCtrl', [

    '$scope',
    '$http',
    '$ionicModal',
    'ngAuthSettings',
    'localStorageService',
    '$ionicPopup',
    'Geofence',
    '$ionicLoading',
    function ($scope, $http, $ionicModal, ngAuthSettings, localStorageService, $ionicPopup, Geofence, $ionicLoading) {
        var authData = simpalTek.global.localStorage.get('authorizationData');
        var userDb = simpalTek.global.localStorage.get('UserDb');
        var rules = {};
        var messages = {};
        var activeAutoOpenTimeStart = null;
        var activeAutoOpenTimeEnd = null;
        var activeAutoOpenTimeMax = null;
        var activeAutoOpenTimeMin = null;

        $scope.settings = {};
        $scope.disableOpenDoorNotificationData = [
            { hour: 0, text: "Remove disable time period" },
            { hour: 1, text: "1 hour" },
            { hour: 2, text: "2 hour" },
            { hour: 3, text: "3 hour" },
            { hour: 4, text: "4 hour" },
        ];

        $scope.settings.enableAlexaPinCmd = userDb.preference.enableAlexaPINCommand;
        $scope.settings.alexaPin = userDb.preference.alexaPIN;
        $scope.settings.enableGooglePinCmd = userDb.preference.enableGooglePINCommand;
        $scope.settings.googlePin = userDb.preference.googlePIN;
        $scope.settings.disableOpenDoorNotificationTime = {};
        $scope.settings.autoFirmwareUpdate = userDb.preference.autoFirmwareUpdate;
        $scope.settings.enableAutoOpen = userDb.preference.proximityMonitorAuto;
        $scope.settings.activeAutoOpenTimePeriod = userDb.preference.activeAutoOpenTimePeriod;

        activeAutoOpenTimeStart = userDb.preference.activeAutoOpenTimeStart;
        activeAutoOpenTimeEnd = userDb.preference.activeAutoOpenTimeEnd;
        activeAutoOpenTimeMax = userDb.preference.activeAutoOpenTimeMax;
        activeAutoOpenTimeMin = userDb.preference.activeAutoOpenTimeMin;

        if (activeAutoOpenTimeMin != null)
            $scope.settings.activeAutoOpenTimeStart = moment(activeAutoOpenTimeMin, "Hmm").format("h:mm a");

        if (activeAutoOpenTimeMax != null)
            $scope.settings.activeAutoOpenTimeEnd = moment(activeAutoOpenTimeMax, "Hmm").format("h:mm a");

        rules.alexaPin = {
            required: {
                depends: function () {
                    if ($scope.settings.enableAlexaPinCmd == true)
                        return true;
                    else
                        return false;
                }
            },
            digits: true
        };

        rules.googlePin = {
            required: {
                depends: function () {
                    if ($scope.settings.enableGooglePinCmd == true)
                        return true;
                    else
                        return false;
                }
            },
            digits: true
        };

        rules.activeAutoOpenTimeStart = {
            required: {
                depends: function () {
                    if ($scope.settings.activeAutoOpenTimePeriod == true)
                        return true;
                    else
                        return false;
                }
            }
        };

        rules.activeAutoOpenTimeEnd = {
            required: {
                depends: function () {
                    if ($scope.settings.activeAutoOpenTimePeriod == true)
                        return true;
                    else
                        return false;
                }
            }
        };


        messages.alexaPin = { required: "PIN Code is required", digits: "Only numbers are allowed" };
        messages.googlePin = { required: "PIN Code is required", digits: "Only numbers are allowed" };
        messages.activeAutoOpenTimeStart = { required: "Start time is required" };
        messages.activeAutoOpenTimeEnd = { required: "End time is required" };

        $scope.settingsList = [
            //{ text: "Email Notification", checked: (userDb.preference)?userDb.preference.emailNotification:false },
            //{ text: "Mobile Notification", checked: (userDb.preference) ? userDb.preference.localNotification : false },
            { text: "Door Open Notification", checked: (userDb.preference) ? userDb.preference.garageOpenNotification : false },
            { text: "Door Close Notification", checked: (userDb.preference) ? userDb.preference.garageCloseNotification : false },
            { text: "Enable Voice Notification", checked: (userDb.preference) ? userDb.preference.isVoiceNotify : false },
            
            //{ text: "Auto Open Door", checked: (userDb.preference) ? userDb.preference.proximityMonitorAuto : false },
            { text: "Door Open Reminder", checked: (userDb.preference) ? userDb.preference.doorOpenReminder : false },
            //{ text: "Door Open Reminder", checked: (userDb.preference) ? userDb.preference.proximityMonitor : false },
            //{ text: "Proximity Auto Close Garage Door", checked: (userDb.preference) ? userDb.preference.proximityAutoClose : false },

        ];

        $scope.saveSettings = function () {
            var data = {};

            var validator = new simpalTek.helper.jQueryFormValidator("formSettings", rules, messages);
            
            if (validator.validate()) {

                $ionicLoading.show({
                    template: 'Saving settings.  Please wait.'
                });

                data = userDb.preference;

                for (var i = 0; i < $scope.settingsList.length; i++) {
                    switch ($scope.settingsList[i].text) {
                        case "Email Notification":
                            data.emailNotification = $scope.settingsList[i].checked;
                            break;
                        case "Mobile Notification":
                            data.localNotification = $scope.settingsList[i].checked;
                            break;
                        case "Door Open Notification":
                            data.garageOpenNotification = $scope.settingsList[i].checked;
                            break;
                        case "Door Close Notification":
                            data.garageCloseNotification = $scope.settingsList[i].checked;
                            break;
                        case "Enable Voice Notification":
                            data.isVoiceNotify = $scope.settingsList[i].checked;
                            break;
                        //case "Auto Open Door":
                        //    data.proximityMonitorAuto = $scope.settingsList[i].checked;
                        //    break;
                        case "Door Open Reminder":
                            //data.proximityMonitor = $scope.settingsList[i].checked;
                            data.doorOpenReminder = $scope.settingsList[i].checked;

                            break;
                        case "Proximity Auto Close Garage Door":
                            data.proximityAutoClose = $scope.settingsList[i].checked;

                            //if ($scope.settingsList[i].checked == true)
                            //    Geofence.start();

                            break;

                    }
                }

                if (userDb.preference) {
                    //data.id = userDb.preference.id;
                    
                    data.enableAlexaPINCommand = $scope.settings.enableAlexaPinCmd;
                    data.alexaPIN = $scope.settings.alexaPin;
                    data.autoFirmwareUpdate = $scope.settings.autoFirmwareUpdate;
                    data.enableGooglePINCommand = $scope.settings.enableGooglePinCmd;
                    data.googlePIN = $scope.settings.googlePin;
                    data.geofences = data.geofences;
                    data.proximityMonitorAuto = $scope.settings.enableAutoOpen;
                    data.activeAutoOpenTimePeriod = $scope.settings.activeAutoOpenTimePeriod;
                    data.activeAutoOpenTimeStart = (activeAutoOpenTimeStart)?moment.utc(activeAutoOpenTimeStart).format():null;
                    data.activeAutoOpenTimeEnd = (activeAutoOpenTimeEnd)?moment.utc(activeAutoOpenTimeEnd).format():null;
                    data.activeAutoOpenTimeMax = activeAutoOpenTimeMax;
                    data.activeAutoOpenTimeMin = activeAutoOpenTimeMin;

                    //if (data.activeAutoOpenTimePeriod) {
                    //    BackgroundGeolocation.setConfig({
                    //        schedule: [
                    //             '1,2,3,4,5,6,7 ' + data.activeAutoOpenTimeStart + '-' + data.activeAutoOpenTimeEnd
                    //        ]
                    //    });

                    //    BackgroundGeolocation.startSchedule(function () {
                    //        console.info('- Scheduler started');
                    //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- Scheduler started');
                    //    });

                    //}

                    $http.put(ngAuthSettings.apiDomainServiceBaseUri + '/PreferenceSetting', data).success(function (response) {
                        console.log("PreferenceSetting response", response)
                        $ionicLoading.hide();

                        if (data.proximityMonitorAuto || data.localNotification) {
     
                            //Geofence.init();

                            //only call the #config once
                            //if (simpalTek.global.bgGeoConfigured == false)
                                simpalTek.helper.geoFence().config();

                                simpalTek.helper.geoFence().updateAllLocations();

                            setTimeout(function () {
                                simpalTek.helper.geoFence().restart();
                            }, 3000)
                        }
                        else if (!data.proximityMonitorAuto && !data.localNotification)
                            Geofence.stop();

                        if (window.cordova) {
                            if (data.garageOpenNotification || data.localNotification) {
                                window.cordova.plugins.notification.local.registerPermission(function (granted) {
                                });
                            }
                        }

                        userDb.preference = data;
                        
                        simpalTek.global.localStorage.set('UserDb', userDb);

                        navigator.notification.alert("Your settings has been saved.", function () {

                        }, "Settings", "Ok");

                        $scope.settingsModal.hide();

                    }).error(function (err, status) {
                        $ionicLoading.hide();

                        navigator.notification.alert("There was an issue saving your settings.  Please try again.", function () {

                        }, "Settings", "Ok");

                        console.log("User Preference error", err, status)
                    });
                }
                else {
                    $http.post(ngAuthSettings.apiDomainServiceBaseUri + '/PreferenceSetting', data).success(function (response) {
                        console.log("PreferenceSetting response", response)
                        $ionicLoading.hide();

                        data.id = response.result.id;
                        userDb.preference = data;

                        simpalTek.global.localStorage.set('UserDb', userDb);

                        if (window.cordova) {

                            //if (data.proximityMonitorAuto || data.localNotification) {
                            if (data.proximityMonitorAuto) {
                                Geofence.init();
                            }

                            if (data.garageOpenNotification || data.localNotification) {
                                window.cordova.plugins.notification.local.registerPermission(function (granted) {
                                });
                            }
                        }

                        navigator.notification.alert("Your settings has been saved.", function () {

                        }, "Settings", "Ok");

                        $scope.settingsModal.hide();

                    }).error(function (err, status) {
                        $ionicLoading.hide();


                        navigator.notification.alert("There was an issue saving your settings.  Please try again.", function () {

                        }, "Settings", "Ok");

                        console.log("User Preference error", err, status)
                    });
                }
            }

        }
        
        $scope.showTimeModal = function (type) {
            var options = {
                mode: 'time'
            };
            
            if (type == 'start') {
                if (activeAutoOpenTimeStart == "")
                    options.date = new Date();
                else
                    options.date = new Date(activeAutoOpenTimeStart);
            }
            else if (type == 'end') {
                if (activeAutoOpenTimeEnd == "")
                    options.date = new Date();
                else
                    options.date = new Date(activeAutoOpenTimeEnd);
            }
            

            datePicker.show(options, function (date) {
                var formatteddatestr = moment(date).format('h:mm a');
                var time = moment(date).format('HHmm');

                if (!date)
                    return;

                if (type == 'start') {
                    $scope.settings.activeAutoOpenTimeStart = formatteddatestr;
                    activeAutoOpenTimeStart = date;
                    activeAutoOpenTimeMin = time;
                }
                else if (type == 'end') {
                    $scope.settings.activeAutoOpenTimeEnd = formatteddatestr;
                    activeAutoOpenTimeEnd = date;
                    activeAutoOpenTimeMax = time;
                }

                if (activeAutoOpenTimeMin != null && activeAutoOpenTimeMax != null)
                {
                    if (parseInt(activeAutoOpenTimeMax) <= parseInt(activeAutoOpenTimeMin))
                    {
                        navigator.notification.alert("End time can't be before Start time.", function () {

                        }, "Invalid Time Range", "Ok");

                        $scope.settings.activeAutoOpenTimeEnd = "";
                        activeAutoOpenTimeEnd = null;
                        activeAutoOpenTimeMax = null;
                    }
                    else if(parseInt(activeAutoOpenTimeMin) >= parseInt(activeAutoOpenTimeMax))
                    {
                        navigator.notification.alert("Start time can't be after End time.", function () {

                        }, "Invalid Time Range", "Ok");

                        $scope.settings.activeAutoOpenTimeEnd = "";
                        activeAutoOpenTimeStart = null;
                        activeAutoOpenTimeMin = null;
                    }
                }

                $scope.$safeApply();

            }, function (error) {
                //alert(JSON.stringify(error))
            });

        }

        $scope.autoOpenEnabled = function () {

            if ($scope.settings.enableAutoOpen) {
                navigator.notification.alert("This feature is currently in Beta.  Auto Open uses geofencing technology which depends upon the device's ability to read the gps location, device electronics, data speed, and network environment.  With these dependents the auto open may or may not work at times.", function () {

                }, "Auto Open Disclaimer", "Ok");
            }
        }

        $scope.setHomeGeoLocation = function () {
            window.navigator.geolocation.getCurrentPosition(function (location) {
                var geoPosition = {};
                geoPosition.latitude = location.coords.latitude;
                geoPosition.longitude = location.coords.longitude;

                simpalTek.global.localStorage.set('HomeGPSLocation', geoPosition);

                console.log('Location from Phonegap', simpalTek.global.localStorage.get('HomeGPSLocation'));

                $ionicPopup.alert({
                    title: 'Home Location',
                    template: 'Your home location has been set.'
                });
            });
        }

    }])