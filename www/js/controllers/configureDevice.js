/**
 * Garage Door controller
 *
 * @module GarageDoorCtrl
 
 * @class simpalGate.controllers
 */

app.controller('ConfigureDeviceCtrl', [

	'$scope',
    '$http',
	'$ionicModal',
    '$q',
    'localStorageService', 
    'ngAuthSettings',
    'localStorageService',
    '$ionicPopup',
    '$timeout',
    'authService',
    '$rootScope',
    'Geofence',
    '$ionicLoading',
    '$interval',
    '$ionicScrollDelegate',
    '$ionicPlatform',
	function ($scope, $http, $ionicModal, $q, localStorageService, ngAuthSettings, localStorageService, $ionicPopup, $timeout, authService, $rootScope, Geofence, $ionicLoading, $interval, $ionicScrollDelegate, $ionicPlatform) {
        $scope.wifiList = [];
        $scope.config = {};
        $scope.choice_ssid = "";
        $scope.wifiNetworkIssue = false;

        var userDb = simpalTek.global.localStorage.get('UserDb');
        var _selectedWifiSsId = "";
        var _vendorDeviceId = "";
        var _deviceInfo = {};
        var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
        var currentGeoLoc = {};
        var userFirstName = "";
        var resetWiFiTimer = null;
        var isConnected2NexxGarageWiFi = false;
        var ngConfigSuccess = false;

        if(!currentNexxGarage) {
            currentNexxGarage = {};
	    }
        
        $scope.registeredDeviceCount = userDb.registeredDevices.length;
        $scope.showRetry = false;

        if (window.WifiWizard) {

            WifiWizard.getCurrentSSID(function (ssid) {
                $scope.currentSSID = ssid;

                if (ssid.toLowerCase().indexOf("nexxgarage") > 0)
                    isConnected2NexxGarageWiFi = true;
                else
                    isConnected2NexxGarageWiFi = false;

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);
            }, function (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
            });
            
        }

	    try {
	        userFirstName = authService.authentication.userFullName.split(" ")[0];
	    } catch (e) { }

	    //$scope.config.wifiPassword = "2347986140"
	    $scope.config.locationNickName = userFirstName + "'s home garage";

	    $scope.showPassword = function () {
	        $("#wifiPassword").attr("type", "text");
	    }

	    $scope.hidePassword = function () {
	        $("#wifiPassword").attr("type", "password");
	    }

	    $scope.countdownFinished = function () {
	        $scope.wifiNetworkIssue = true;
	        $interval.cancel(resetWiFiTimer);
	    }

	    
	    if (userDb.registeredDevices.length == 0) {
	        //$timeout(function () {
	            resetWiFiTimer = $interval(function () {

	                if ($scope.wifiList.length > 0)
	                    $interval.cancel(resetWiFiTimer);
	                else
	                    $scope.getList();

	            }, 3000);
	        //}, 60000);  //start retrieving the wifi list after 1 min.  this will give the user
	                    // time to switch to the NG network before retrieving the list.
	    }
	    


	    $timeout(function () {
	        $interval.cancel(resetWiFiTimer);
	    }, 300000);  //stop the interval after 5 min

	    Geofence.getCurrentPosition()
           .then(function (position) {
               //console.log("Current position found", position);

               if (position.coords) {
                   currentGeoLoc.latitude = position.coords.latitude;
                   currentGeoLoc.longitude = position.coords.longitude;
               }
               
              
           }, function (reason) {
               //console.log("Cannot obtain current location", reason);
               simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Cannot obtain current location reason :" + reason);

               if (reason == 1 && ionic.Platform.isAndroid()) {
                   navigator.notification.alert("Location Service must be turned on before setting up your Nexx Garage device.  You can go to the \"Settings\" to turn it on.", function () {

                   }, "Location Service Error", "Ok");
               }

               //$ionicLoading.show({
               //    template: "Cannot obtain current location",
               //    duration: 1500
               //});
           });

	    

        //$q.all([
        //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/heart'), // <--- check device connectivity
        //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/device-info'), // <--- retrieve device info
        //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/networks') // <--- retrieve wifi network from the device
        //]).then(function (value) {
        //    // Success callback where value is an array containing the success values
        //    //console.log("Success", value);
            
        //    try {
        //        _deviceInfo.vendorDeviceId = value[1].data.device_id;
        //    } catch (e) {
        //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device info error :" + e);
        //    }
        //    //set the vendor device id to localstorage so we can later save it back to the server.
        //    //simpalTek.global.localStorage.set("currentVendorDeviceId", _deviceInfo.vendorDeviceId);
        //    //$rootScope.currentVendorDeviceId = _deviceInfo.vendorDeviceId;
        //    currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;

        //    try{
        //        $scope.wifiList = value[2].data.networks;
        //    } catch (e) {
        //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device network error :" + e);
        //    }
        //    //$scope.$apply();
        //    //console.log("value[2].networks", value[2])
        //}, function (reason) {
        //    // Error callback where reason is the value of the first rejected promise
        //    //console.log("Error", reason);
        //});
        

        $scope.connectWifi = function (wifi){
            //console.log("wifi network", wifi)
            $scope.config.ssId = wifi;
        }
        
        $scope.getList = function () {
            $ionicPlatform.ready(function () {
                if (window.cordova) {

                    WifiWizard.getCurrentSSID(function (ssid) {
                        
                        $scope.currentSSID = ssid;

                        if (ssid.toLowerCase().indexOf("nexxgarage") > 0)
                            isConnected2NexxGarageWiFi = true;
                        else
                            isConnected2NexxGarageWiFi = false;

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);
                    }, function (e) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
                    });

                    //WifiWizard.listNetworks(function (networks) {
                    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WiFi network " + JSON.stringify(networks));
                    //}, function (e) {
                    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard WiFi network error " + JSON.stringify(e));
                    //});

                }
            });

            $.ajax({
                url: ngAuthSettings.apiDeviceBaseUri + '/heart?t=' + new Date().getTime(),
                type: 'GET',
                statusCode: {
                    200: function (response) {

                        try {
                            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "NG device info:" + JSON.stringify(deviceInfoData));
                            
                            $.ajax({
                                url: ngAuthSettings.apiDeviceBaseUri + '/device-info?t=' + new Date().getTime(),
                                type: 'GET',
                                statusCode: {
                                    200: function (response) {

                                        _deviceInfo.vendorDeviceId = response.device_id;
                                        currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;

                                        $.ajax({
                                            url: ngAuthSettings.apiDeviceBaseUri + '/networks?t=' + new Date().getTime(),
                                            type: 'GET',
                                            statusCode: {
                                                200: function (response) {
                                                    try {
                                                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "Wifi Networks :" + JSON.stringify(networkData));

                                                        $scope.wifiList = response.networks;
                                                        $interval.cancel(resetWiFiTimer);

                                                    } catch (e) {
                                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device network error");
                                                    }
                                                }
                                            },
                                            error: function (jqXHR, status, error) {
                                                if (status != 200)
                                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting network api :" + JSON.stringify(jqXHR));
                                            },
                                            complete: function (event, request, settings) {
                                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Complete event - Getting network api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                                            }

                                        });
                                    }
                                },
                                error: function (jqXHR, status, error) {
                                    if (status != 200)
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting device api :" + JSON.stringify(jqXHR));
                                },
                                complete: function (event, request, settings) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Complete event - Getting device api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                                }
                            });

                        } catch (e) {
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device info error " + JSON.stringify(e));
                        }

                        
                    }
                },
                error: function (jqXHR, status, error) {

                    if (status != 200) 
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting heart api :" + JSON.stringify(jqXHR));
                },
                complete: function (event, request, settings) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Complete event - Getting heart api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                }

            });

            //$http.get(ngAuthSettings.apiDeviceBaseUri + '/heart').then(function (response) {
            //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/device-info').then(function (deviceInfoData) {

            //        try {
            //            _deviceInfo.vendorDeviceId = deviceInfoData.data.device_id;
            //            currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;

            //            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "NG device info:" + JSON.stringify(deviceInfoData.data));

            //            $http.get(ngAuthSettings.apiDeviceBaseUri + '/networks').then(function (networkData) {
            //                try {
            //                    $scope.wifiList = networkData.data.networks;
            //                    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "Wifi Networks :" + JSON.stringify(networkData.data));
            //                } catch (e) {
            //                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device network error");
            //                }
            //            }, function (errorNetwork) {
            //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting network api :" + JSON.stringify(errorNetwork));
            //            });

            //        } catch (e) {
            //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device info error");
            //        }

                    
            //    }, function (errorDevice) {
            //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting device api :" + JSON.stringify(errorDevice));
            //    });
            //}, function (errorHearBeat) {
            //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting heart api :" + JSON.stringify(errorHearBeat));
            //});

            //$q.all([
            //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/heart'), // <--- check device connectivity
            //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/device-info'), // <--- retrieve device info
            //    $http.get(ngAuthSettings.apiDeviceBaseUri + '/networks') // <--- retrieve wifi network from the device
            //]).then(function (value) {
            //    // Success callback where value is an array containing the success values
            //    //console.log("Success", value);

            //    try {
            //        _deviceInfo.vendorDeviceId = value[1].data.device_id;
            //    } catch (e) {
            //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device info error :" + e);
            //    }
            //    //set the vendor device id to localstorage so we can later save it back to the server.
            //    //simpalTek.global.localStorage.set("currentVendorDeviceId", _deviceInfo.vendorDeviceId);
            //    //$rootScope.currentVendorDeviceId = _deviceInfo.vendorDeviceId;
            //    currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;

            //    try{
            //        $scope.wifiList = value[2].data.networks;
            //    } catch (e) {
            //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device network error :" + e);
            //    }
            //    //$scope.$apply();
            //    //console.log("value[2].networks", value[2])
            //}, function (reason) {
            //    // Error callback where reason is the value of the first rejected promise
            //    //console.log("Error", reason);
            //});
        }
        

	    //$scope.getList();

        $scope.retryAddDevice = function () {

            $.ajax({
                url: ngAuthSettings.apiDomainServiceBaseUri + '/Device?t=' + new Date().getTime(),
                type: 'POST',
                data: JSON.stringify(_deviceInfo),
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                    , 'Authorization': 'bearer ' + simpalTek.global.localStorage.get('authorizationData').token
                },
                contentType: 'application/json',
                statusCode: {
                    200: function (response) {
                        currentNexxGarage.deviceId = response.result.id;
                        currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
                        currentNexxGarage.garageDoorState = "Unknown";

                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);

                        $.ajax({
                            url: ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/',
                            type: 'GET',
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                                , 'Authorization': 'bearer ' + simpalTek.global.localStorage.get('authorizationData').token
                            },
                            contentType: 'application/json',
                            statusCode: {
                                200: function (response) {

                                    if (!socket.connected) {
                                        //socket.io.reconnection = true;
                                        socket.connect();
                                        //socket.emit('join', { email: authService.authentication.userName });

                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "socket reconnected");
                                    }

                                    simpalTek.global.localStorage.set("UserDb", response.result);

                                    $timeout(function () {

                                        $ionicLoading.hide();

                                        navigator.notification.alert("Your device has been successfully saved.", function () {

                                        }, "Device Register", "Ok");

                                        simpalTek.common.redirect('app/garageDoorControl');
                                    }, 500);
                                },
                                401: function () {
                                    //alert("The username or password were not correct. Try again.");
                                }
                            },
                            success: function (data) {
                                console.info(data);
                            },
                            error: function (jqXHR, status, error) {
                                console.log("error", jqXHR, status, error)
                            }

                        });

                    },
                    401: function () {
                        //alert("The username or password were not correct. Try again.");
                    }
                },
                success: function (data) {
                    console.info(data);
                },
                error: function (jqXHR, status, error) {

                    if (err.status != 200) {
                        $timeout(function () {
                            $ionicLoading.hide();

                            navigator.notification.confirm(
                                "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                                    function (buttonIndex) {
                                        if (buttonIndex == 2) {
                                            $scope.retryAddDevice();
                                        }
                                    }, "Failed Cloud Service Registration",
                                    ['Cancel', 'Retry']
                                );

                        }, 1000);
                    }
                }

            });

            //$http.post(ngAuthSettings.apiDomainServiceBaseUri + '/Device', _deviceInfo)
            //    .then(function (response) {

            //        if (!socket.connected) {
            //            //socket.io.reconnection = true;
            //            socket.connect();
            //            socket.emit('join', { email: authService.authentication.userName });

            //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "socket reconnected");
            //        }

            //        if (!currentNexxGarage) {
            //            currentNexxGarage = {};
            //        }

            //        currentNexxGarage.deviceId = response.data.result.id;
            //        currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
            //        currentNexxGarage.garageDoorState = "Unknown";

            //        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)

            //        $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/').then(function (response) {

            //            simpalTek.global.localStorage.set("UserDb", response.data.result);

            //            $timeout(function () {

            //                $ionicLoading.hide();

            //                navigator.notification.alert("Your device has been successfully saved to the cloud service.", function () {

            //                }, "Device Register", "Ok");

            //                simpalTek.common.redirect('app/garageDoorControl');
            //            }, 500);

            //        }, function (err) {
            //            //simpalTek.helper.messageCenterService("Error on reconnect user preload data: " + JSON.stringify(err), simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown, 10000);
            //        });
                    
            //    }, function (err) {
            //        $timeout(function () {
            //            $ionicLoading.hide();

            //            $ionicPopup.confirm({
            //                title: 'Failed Cloud Service Registration',
            //                template: 'There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?',
            //                buttons: [
            //                           { text: 'Cancel' },
            //                             {
            //                                 text: '<b>Retry</b>',
            //                                 type: 'button-positive',
            //                                 onTap: function (e) {
            //                                     $scope.retryAddDevice();
            //                                 }
            //                             }
            //                ]
            //            });
            //            //navigator.notification.confirm(
            //            //    "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
            //            //        function (buttonIndex) {
            //            //            if (buttonIndex == 2) {
            //            //                $scope.retryAddDevice();
            //            //            }
            //            //        }, "Failed Cloud Service Registration",
            //            //        ['Cancel', 'Retry']
            //            //    );

            //        }, 1000);
            //    });
        }

        $scope.updateDeviceLocation = function () {
            var deviceInfo = {};

            navigator.notification.confirm("Warning - Updating the device location will configure the \"Auto Door Open\" feature to automatically open the garage door.  You must be in the garage where the Nexx Garage device is located.  Are you sure you want to continue?", function (buttonIndex) {

                if (buttonIndex == 2) {
                    $ionicLoading.show({
                        template: 'Updating device location.  Please wait.'
                    });

                    Geofence.getCurrentPosition().then(function (position) {
                        //console.log("Current position found", position);

                        if (position.coords) {
                            currentGeoLoc.latitude = position.coords.latitude;
                            currentGeoLoc.longitude = position.coords.longitude;

                            deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);

                            if (userDb.registeredDevices.length > 0) {
                                deviceInfo.deviceId = userDb.registeredDevices[0].deviceId;
                                deviceInfo.vendorDeviceId = userDb.registeredDevices[0].deviceName;
                                deviceInfo.NickName = userDb.registeredDevices[0].locationNickName;
                            } else
                                deviceInfo.vendorDeviceId = currentNexxGarage.vendorDeviceId; //simpalTek.global.localStorage.get("currentVendorDeviceId");


                            $http.post(ngAuthSettings.apiDomainServiceBaseUri + '/Device', deviceInfo)
                            .then(function (response) {
                                //console.log(response)

                                $ionicLoading.hide();

                                if (response.data.status == 0) {

                                    navigator.notification.alert("Your device location has been updated.", function () {

                                    }, "Device Location Update", "Ok");
                                }
                                else {

                                    navigator.notification.alert("There was an issue updating your device location.", function () {

                                    }, "Device Location Update Error", "Ok");
                                }

                            },
                            function (err) {

                                $ionicLoading.hide();

                                navigator.notification.alert("There was an issue updating your device location.", function () {

                                }, "Device Location Update Error", "Ok");

                            })

                        }


                    }, function (reason) {
                        //console.log("Cannot obtain current location", reason);
                    });
                }

            }, "Device Location Update", 
              ['Cancel', 'Continue']
            );

            

        }

        $scope.resetDevice = function (){

            var domainModel = {};
            var deviceInfo = {};
            

            //domainModel.domainModelData = deviceInfo;
            
            
            var onConfirm = function (buttonIndex) {

                if (buttonIndex == 2) {

                    $ionicLoading.show({
                        template: 'Resetting your device.  Please wait.'
                    });

                    if (userDb.registeredDevices.length > 0) {
                        _deviceInfo.deviceId = userDb.registeredDevices[0].deviceId;
                        _deviceInfo.vendorDeviceId = userDb.registeredDevices[0].deviceName;
                    }
                    //else
                    //    _deviceInfo.vendorDeviceId = currentNexxGarage.vendorDeviceId; //simpalTek.global.localStorage.get("currentVendorDeviceId");

                    $http({
                        method: 'DELETE',
                        url: ngAuthSettings.apiDomainServiceBaseUri + '/DeviceConfig',
                        data: _deviceInfo,
                        headers: { 'Content-Type': 'application/json;charset=utf-8' }
                    }).success(function (response) {
                        //console.log(response)

                        userDb.registeredDevices = userDb.registeredDevices.filter(function (item) { item.deviceId != _deviceInfo.deviceId });
                        userDb.devicesAccess = userDb.devicesAccess.filter(function (item) { item.deviceId != _deviceInfo.deviceId});
                        simpalTek.global.localStorage.set('UserDb', userDb)
                        simpalTek.global.localStorage.set("currentNexxGarage", null);

                        if (response.status == 0) {
                            //$ionicPopup.alert({
                            //    title: 'Device Reset',
                            //    template: 'Your device has been successfully reset.'
                            //});

                            resetWiFiTimer = $interval(function () {
                                //console.log("refreshing WiFi Timer")
                                $scope.getList();

                                if ($scope.wifiList.length > 0)
                                    $interval.cancel(resetWiFiTimer);

                                //$scope.wifiList = [1,2]
                            }, 3000);

                            $timeout(function () {

                                $scope.registeredDeviceCount = 0;
                                $scope.wifiList = [];

                                navigator.notification.alert("Your device has been successfully reset.", function () {

                                }, "Device Reset", "Ok");

                                $ionicLoading.hide();

                            }, 5000);
                            
                        }
                        else {
                            //$ionicPopup.alert({
                            //    title: 'Device Reset Error',
                            //    template: 'There was an issue reseting the device information.'
                            //});

                            $timeout(function () {
                                navigator.notification.alert("There was an issue reseting the device information.", function () {
                                    $ionicLoading.hide();
                                }, "Device Reset Error", "Ok");
                            }, 500);    
                        }

                    }).error(function (err, status) {

                        //$ionicLoading.hide();

                        //$ionicPopup.alert({
                        //    title: 'Device Reset Error',
                        //    template: 'There was an issue reseting the device information.'
                        //});

                        $timeout(function () {
                            navigator.notification.alert("There was an issue reseting the device information.", function () {
                                $ionicLoading.hide();
                            }, "Device Reset Error", "Ok");
                        }, 500);

                        //console.log("Device config error", err, status)
                    });


                    
                }
            }

            navigator.notification.confirm(
                'Are you sure you want to reset your device settings?', // message
                 onConfirm,            // callback to invoke with index of button pressed
                'Reset Device Settings',           // title
                ['No', 'Yes']     // buttonLabels
            );

            ////$http.delete(ngAuthSettings.apiDomainServiceBaseUri + '/DeviceConfig', { domainModelData: { smartGateDeviceID: 1 } }, { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } })
            //.success(function (response) {
            //    console.log(response)
                
                
            //    if (response.data.status == 0) {
            //        $ionicPopup.alert({
            //            title: 'Device Reset',
            //            template: 'Your device has been successfully reset.'
            //        });
            //    }
            //    else {
            //        $ionicPopup.alert({
            //            title: 'Device Reset Error',
            //            template: 'There was an issue reseting the device information.'
            //        });
            //    }


            //}).error(function (err, status) {
            //    $ionicPopup.alert({
            //        title: 'Device Reset Error',
            //        template: 'There was an issue reseting the device information.'
            //    });
            //    console.log("Device config error", err, status)
            //});
        }

        $scope.sendDeviceInfo = function () {
            
            try {
                simpalTek.global.tempDeviceInfo.wifi.password = "*****";
                simpalTek.global.tempDeviceInfo.mqtt.password = "*****";
                simpalTek.global.tempDeviceInfo.mqtt.fingerprint = "*****";

                $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/SendMail", { SenderEmail: "chris28@gmail.com", RecipientEmail: "support@nexxgarage.com", Subject: "Nexx Garage App Config", Message: JSON.stringify(simpalTek.global.tempDeviceInfo) });

                navigator.notification.alert("Troubleshooting information has been sent.", function () {

                }, "Troubleshooting", "Ok");
            } catch (e) { }
        }

        $scope.saveDeviceConfig = function () {
            var deviceConfigPayload = {};
            var deviceInfo = {};
            var rules = {};
            var messages = {};
            var errMsg = "";
            var wifiPassword = $scope.config.wifiPassword;

            if (wifiPassword == undefined)
                wifiPassword = "";
            else
                wifiPassword = wifiPassword.trim();

            rules.wifiPassword = { required: true };
            rules.locationNickName = { required: true };

            messages.wifiPassword = { required: "Your WiFi password is required" };
            messages.locationNickName = { required: "Your location Nick Name is required" };

            var ssId = $scope.config.ssId;

            var locationNickName = $scope.config.locationNickName;

            if (ssId == undefined)
                ssId = "";
            else
                ssId = ssId.trim();

            var validator = new simpalTek.helper.jQueryFormValidator("signin", rules, messages);

            //if (validator.validate()) {

                $ionicLoading.show({
                    template: 'Saving device configuration.  Please wait.'
                });
                //deviceConfigPayload.jsonVersion = "2";
                deviceConfigPayload.name = "NexxGarage";
                deviceConfigPayload.device_id = _deviceInfo.vendorDeviceId;
                deviceConfigPayload.userAccount = authService.authentication.userName;
                deviceConfigPayload.userId = authService.authentication.userName;
                deviceConfigPayload.wifi = {};
                deviceConfigPayload.wifi.ssid = encodeURIComponent(ssId);
                deviceConfigPayload.wifi.password = encodeURIComponent(wifiPassword);
                deviceConfigPayload.mqtt = {};
                deviceConfigPayload.mqtt.host = userDb.appSettings.mqttHost;
                deviceConfigPayload.mqtt.port = parseInt(userDb.appSettings.mqttPort);
                deviceConfigPayload.mqtt.base_topic = "devices/";
                deviceConfigPayload.mqtt.auth = Boolean(userDb.appSettings.mqttAuth);
                deviceConfigPayload.mqtt.username = userDb.appSettings.mqttAcctUserName;
                deviceConfigPayload.mqtt.password = encodeURIComponent(userDb.appSettings.mqttAcctPwd);
                deviceConfigPayload.mqtt.ssl = Boolean(userDb.appSettings.mqttSSL);
                deviceConfigPayload.mqtt.fingerprint = "79 61 3B 45 F3 2E D1 66 9D 35 9C 24 01 35 2F C9 05 AA 9C 57"
                deviceConfigPayload.ota = {};
                deviceConfigPayload.ota.enabled = true;
                deviceConfigPayload.ota.host = userDb.appSettings.otaHost;
                deviceConfigPayload.ota.port = parseInt(userDb.appSettings.otaPort);
                deviceConfigPayload.ota.path = "/ota"
                //deviceConfigPayload.mqtt.ota.ssl = false;
                //deviceConfigPayload.mqtt.ota.fingerprint = "CF 05 98 89 CA FF 8E D8 5E 5C E0 C2 E4 F7 E6 C3 C7 50 DD 5C";

                
                          

                if (locationNickName == undefined)
                    locationNickName = "";
                else
                    locationNickName = locationNickName.trim();

                if (ssId == "") {
                    navigator.notification.alert("You must select a WiFi network.", function () {

                    }, "Invalid WiFi Network", "Ok");

                    $ionicLoading.hide();

                    return;
                }
                else if (_deviceInfo.vendorDeviceId == undefined || _deviceInfo.vendorDeviceId == "") {
                    navigator.notification.alert("There was an issue retrieving the device information, please try refreshing the WiFi list.  If the issue continues please contact support.", function () {

                    }, "Device Info Error", "Ok");

                    $ionicLoading.hide();

                    return;
                }
                //else if (wifiPassword == "") {

                //    navigator.notification.alert("You must enter WiFi password.", function () {

                //    }, "Invalid Input", "Ok");

                //    $ionicLoading.hide();

                //    return;
                //}
                //else if (locationNickName == "") {
                //    navigator.notification.alert("You must enter a location nick name.", function () {

                //    }, "Invalid Input", "Ok");

                //    $ionicLoading.hide();

                //    return;
                //}

                var confirmSaveDeviceInfoMsg = "";

                if (ionic.Platform.isAndroid()) 
                    confirmSaveDeviceInfoMsg = 'Please verify your WiFi network is ' + ssId + " and WiFi password is " + ((wifiPassword=="")?"[Empty]":wifiPassword) + ".  Do you want to continue?";
                else
                    confirmSaveDeviceInfoMsg = 'Please verify your WiFi network and password below. \r\r Wifi network: ' + ssId + " \r WiFi password: " + ((wifiPassword == "") ? "[Empty]" : wifiPassword) + "\r(Password is case sensitive)\r\rDo you want to continue?";

                navigator.notification.confirm(
                    confirmSaveDeviceInfoMsg, // message
                     function (buttonIndex) {
                         if (buttonIndex == 2) {

                             simpalTek.global.tempDeviceInfo = angular.copy(deviceConfigPayload);

                             $.ajax({
                                 url: ngAuthSettings.apiDeviceBaseUri + '/config?t=' + new Date().getTime(),
                                 type: 'PUT',
                                 data: JSON.stringify(deviceConfigPayload),
                                 dataType: 'application/json',
                                 contentType: 'application/json',
                                 headers: { 'Content-Type': 'application/json;charset=utf-8' },
                                 async: true,
                                 statusCode: {
                                     200: function (response) {
                                         //if (response.success == true) {

                                             var reconnectCounter = 0;
                                             var offNGNetwork = false;

                                             $scope.registeredDeviceCount = 0;
                                             $scope.wifiList = [];
                                            
                                            //incase the retry alert does not popup
                                             $timeout(function () {
                                                 $ionicLoading.hide();
                                                 $scope.showRetry = true;
                                                 $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                                             }, 30000)

                                             var intervalReconnect = $interval(function () {


                                                 WifiWizard.getCurrentSSID(function (ssid) {
                                                     simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);

                                                     if (ssid.toLowerCase().indexOf("nexxgarage") < 0 && !offNGNetwork) {
                                                         offNGNetwork = true;
                                                         $interval.cancel(intervalReconnect);

                                                         

                                                         //$timeout(function () {

                                                         //clear all existing geo location
                                                         Geofence.removeAll();

                                                         Geofence.getCurrentPosition().then(function (position) {
                                                             //console.log("Current position found", position);

                                                             if (position.coords) {
                                                                 currentGeoLoc.latitude = position.coords.latitude;
                                                                 currentGeoLoc.longitude = position.coords.longitude;

                                                                 simpalTek.helper.geo().add($scope.config.locationNickName, currentGeoLoc.latitude, currentGeoLoc.longitude,
                                                                      function () {

                                                                      }
                                                                  );
                                                             }


                                                         }, function (reason) {
                                                             //console.log("Cannot obtain current location", reason);

                                                         });

                                                         _deviceInfo.NickName = $scope.config.locationNickName;

                                                         try {
                                                             if (typeof currentGeoLoc === "object")
                                                                 _deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);
                                                             else
                                                                 _deviceInfo.GPSLoc = "";
                                                         }
                                                         catch (e) {
                                                             _deviceInfo.GPSLoc = "";
                                                         }

                                                         simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "cloud add device payload:" + JSON.stringify(_deviceInfo));

                                                         $.ajax({
                                                             url: ngAuthSettings.apiDomainServiceBaseUri + '/Device',
                                                             type: 'POST',
                                                             data: JSON.stringify(_deviceInfo),
                                                             headers: {
                                                                 'Content-Type': 'application/json;charset=utf-8'
                                                                 , 'Authorization': 'bearer ' + simpalTek.global.localStorage.get('authorizationData').token
                                                             },
                                                             contentType: 'application/json',
                                                             statusCode: {
                                                                 200: function (response) {
                                  
                                                                     currentNexxGarage.deviceId = response.result.id;
                                                                     currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
                                                                     currentNexxGarage.garageDoorState = "Unknown";

                                                                     simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);

                                                                     $.ajax({
                                                                         url: ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/',
                                                                         type: 'GET',
                                                                         headers: {
                                                                             'Content-Type': 'application/json;charset=utf-8'
                                                                             , 'Authorization': 'bearer ' + simpalTek.global.localStorage.get('authorizationData').token
                                                                         },
                                                                         contentType: 'application/json',
                                                                         statusCode: {
                                                                             200: function (response) {

                                                                                 //if (!socket.connected) {
                                                                                 //    //socket.io.reconnection = true;
                                                                                 //    //socket.connect();
                                                                                 //    //socket.emit('join', { email: authService.authentication.userName });

                                                                                 //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "socket reconnected");
                                                                                 //}

                                                                                 simpalTek.global.localStorage.set("UserDb", response.result);

                                                                                 $timeout(function () {

                                                                                     $ionicLoading.hide();

                                                                                     navigator.notification.alert("Your device has been successfully saved.", function () {

                                                                                     }, "Device Register", "Ok");

                                                                                     simpalTek.common.redirect('app/garageDoorControl');
                                                                                 }, 500);
                                                                             },
                                                                             401: function () {
                                                                                 //alert("The username or password were not correct. Try again.");
                                                                             }
                                                                         },
                                                                         success: function (data) {
                                                                             console.info(data);
                                                                         },
                                                                         error: function (jqXHR, status, error) {
                                                                             console.log("error", jqXHR, status, error)
                                                                         }

                                                                     });

                                                                 },
                                                                 401: function () {
                                                                     //alert("The username or password were not correct. Try again.");
                                                                 }
                                                             },
                                                             success: function (data) {
                                                                 console.info(data);
                                                             },
                                                             error: function (jqXHR, status, error) {

                                                                 $ionicLoading.hide();
                                                                 $scope.registeredDeviceCount = 0;
                                                                 $scope.wifiList = [];

                                                                 $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                                                                 $scope.showRetry = true;

                                                                 if (err.status != 200) {
                                                                     simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(err));
                                                                     
                                                                     $timeout(function () {

                                                                         navigator.notification.confirm(
                                                                         "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                                                                          function (buttonIndex) {
                                                                              if (buttonIndex == 2) {
                                                                                  $scope.retryAddDevice();
                                                                              }
                                                                          }, "Failed Cloud Service Registration",
                                                                           ['Cancel', 'Retry']
                                                                         );
                                                                         
                                                                     }, 1000);
                                                                 }
                                                             }

                                                         });

                                                         //$http.post(ngAuthSettings.apiDomainServiceBaseUri + '/Device', _deviceInfo)
                                                         //.then(function (response) {

                                                         //    currentNexxGarage.deviceId = response.data.result.id;
                                                         //    currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
                                                         //    currentNexxGarage.garageDoorState = "Unknown";

                                                         //    simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)

                                                         //    $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/').then(function (response) {

                                                         //        if (!socket.connected) {
                                                         //            //socket.io.reconnection = true;
                                                         //            socket.connect();
                                                         //            socket.emit('join', { email: authService.authentication.userName });

                                                         //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "socket reconnected");
                                                         //        }

                                                         //        simpalTek.global.localStorage.set("UserDb", response.data.result);

                                                         //        $timeout(function () {

                                                         //            $ionicLoading.hide();

                                                         //            navigator.notification.alert("Your device has been successfully saved.", function () {

                                                         //            }, "Device Register", "Ok");

                                                         //            simpalTek.common.redirect('app/garageDoorControl');
                                                         //        }, 500);

                                                         //    }, function (err) {
                                                         //        //simpalTek.helper.messageCenterService("Error on reconnect user preload data: " + JSON.stringify(err), simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown, 10000);
                                                         //    });


                                                         //    //console.log(response)
                                                         //    if (response.data.status == 0) {
                                                         //        //$ionicPopup.alert({
                                                         //        //    title: 'Device Register',
                                                         //        //    template: 'Your device has been successfully saved.'
                                                         //        //});
                                                         //    }
                                                         //    else {

                                                         //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(response));

                                                         //        //$ionicPopup.alert({
                                                         //        //    title: 'Device Register',
                                                         //        //    template: 'There was an issue saving the device information.'
                                                         //        //});
                                                         //    }

                                                         //}, function (err) {

                                                         //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(err));
                                                         //    $ionicLoading.hide();

                                                         //    $timeout(function () {
                                                         //        navigator.notification.confirm(
                                                         //        "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                                                         //         function (buttonIndex) {
                                                         //             if (buttonIndex == 2) {
                                                         //                 $scope.retryAddDevice();
                                                         //             }
                                                         //         }, "Failed Cloud Service Registration",
                                                         //          ['Cancel', 'Retry']
                                                         //        );

                                                         //        //$ionicPopup.confirm({
                                                         //        //    title: 'Failed Cloud Service Registration',
                                                         //        //    template: 'There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?',
                                                         //        //    buttons: [
                                                         //        //               { text: 'Cancel' },
                                                         //        //                 {
                                                         //        //                     text: '<b>Retry</b>',
                                                         //        //                     type: 'button-positive',
                                                         //        //                     onTap: function (e) {
                                                         //        //                         $scope.retryAddDevice();
                                                         //        //                     }
                                                         //        //                 }
                                                         //        //    ]
                                                         //        //});

                                                         //        $scope.registeredDeviceCount = 0;
                                                         //        $scope.wifiList = [];


                                                         //        //    $scope.showRetry = true;

                                                         //        //    navigator.notification.alert("There was an issue saving the device information.", function () {

                                                         //        //    }, "Device Register Error", "Ok");

                                                         //        //    angular.element(document).ready(function () {
                                                         //        //        $timeout(function () {
                                                         //        //            $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                                                         //        //        }, 500);
                                                         //        //    });

                                                         //    }, 1000);
                                                         //})


                                                         //}, 3000);


                                                     }

                                                 }, function (e) {
                                                     simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
                                                 });

                                                 if (reconnectCounter >= 30) {
                                                     $interval.cancel(intervalReconnect)

                                                     simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(err));

                                                     $timeout(function () {
                                                         $scope.registeredDeviceCount = 0;
                                                         $scope.wifiList = [];

                                                         $ionicLoading.hide();
                                                         $scope.showRetry = true;

                                                         navigator.notification.confirm(
                                                         "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                                                          function (buttonIndex) {
                                                              if (buttonIndex == 2) {
                                                                  $scope.retryAddDevice();
                                                              }
                                                          }, "Failed Cloud Service Registration",
                                                           ['Cancel', 'Retry']
                                                         );

                                                         //$ionicPopup.confirm({
                                                         //    title: 'Failed Cloud Service Registration',
                                                         //    template: 'There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?',
                                                         //    buttons: [
                                                         //               { text: 'Cancel' },
                                                         //                 {
                                                         //                     text: '<b>Retry</b>',
                                                         //                     type: 'button-positive',
                                                         //                     onTap: function (e) {
                                                         //                         $scope.retryAddDevice();
                                                         //                     }
                                                         //                 }
                                                         //    ]
                                                         //});


                                                        

                                                         //    navigator.notification.alert("There was an issue saving the device information.", function () {

                                                         //    }, "Device Register Error", "Ok");

                                                         //    angular.element(document).ready(function () {
                                                         //        $timeout(function () {
                                                         //            $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                                                         //        }, 500);
                                                         //    });

                                                     }, 1000);

                                                 }
                                                 reconnectCounter += 5;
                                             }, 5000);

                                             //$timeout(function () {


                                             //}, (ionic.Platform.isAndroid())?30000:10000);


                                         //}
                                         //else {

                                         //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "[2]-NG save device config error info: " + JSON.stringify(response));

                                         //    $timeout(function () {
                                         //        $ionicLoading.hide();
                                         //        //$ionicPopup.alert({
                                         //        //    title: 'Device Register Error',
                                         //        //    template: 'There was an issue saving the device information.'
                                         //        //});
                                         //        navigator.notification.alert("[2]-There was an issue configuring the device.", function () {

                                         //        }, "Device Configuration Error", "Ok");
                                         //    }, 500);
                                         //}
                                     }
                                 },
                                 success: function (response) {
                                     

                                 },
                                 error: function (err) {

                                     if (err.status != 200) {
                                         simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "[1]-NG save device info:" + JSON.stringify(err));

                                         $timeout(function () {
                                             $ionicLoading.hide();

                                             navigator.notification.alert("[1]-There was an issue configuring the device.", function () {

                                             }, "Device Configuration Error", "Ok");

                                             //console.log("Device config error", err, status)
                                         }, 500);
                                     }
                                 }
                             });

                             //$http({
                             //    method: 'PUT',
                             //    url: ngAuthSettings.apiDeviceBaseUri + '/config',
                             //    data: deviceConfigPayload,
                             //    headers: { 'Content-Type': 'application/json;charset=utf-8' }
                             //}).success(function (response) {
                             //    //console.log(response)

                             //    ////set the vendor device id to localstorage so we can later save it back to the server.
                             //    //simpalTek.global.localStorage.set("currentVendorDeviceId", _deviceInfo.vendorDeviceId);

                             //    if (response.success == true) {
                                                
                             //        var reconnectCounter = 0;
                             //        var offNGNetwork = false;

                             //        ngConfigSuccess = true;

                             //        var intervalReconnect = setInterval(function () {

                                         
                             //            WifiWizard.getCurrentSSID(function (ssid) {
                             //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);

                             //                if (ssid.toLowerCase().indexOf("nexxgarage") < 0 && !offNGNetwork) {
                             //                    offNGNetwork = true;
                             //                    clearInterval(intervalReconnect);

                             //                    //$timeout(function () {

                             //                        //clear all existing geo location
                             //                        Geofence.removeAll();

                             //                        Geofence.getCurrentPosition().then(function (position) {
                             //                            //console.log("Current position found", position);

                             //                            if (position.coords) {
                             //                                currentGeoLoc.latitude = position.coords.latitude;
                             //                                currentGeoLoc.longitude = position.coords.longitude;

                             //                                simpalTek.helper.geo().add($scope.config.locationNickName, currentGeoLoc.latitude, currentGeoLoc.longitude,
                             //                                     function () {

                             //                                     }
                             //                                 );
                             //                            }


                             //                        }, function (reason) {
                             //                            //console.log("Cannot obtain current location", reason);

                             //                        });

                             //                        _deviceInfo.NickName = $scope.config.locationNickName;

                             //                        try {
                             //                            if (typeof currentGeoLoc === "object")
                             //                                _deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);
                             //                            else
                             //                                _deviceInfo.GPSLoc = "";
                             //                        }
                             //                        catch (e) {
                             //                            _deviceInfo.GPSLoc = "";
                             //                        }

                             //                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "cloud add device payload:" + JSON.stringify(_deviceInfo));

                             //                        $http.post(ngAuthSettings.apiDomainServiceBaseUri + '/Device', _deviceInfo)
                             //                        .then(function (response) {

                             //                            currentNexxGarage.deviceId = response.data.result.id;
                             //                            currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
                             //                            currentNexxGarage.garageDoorState = "Unknown";

                             //                            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)

                             //                            $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/').then(function (response) {

                             //                                if (!socket.connected) {
                             //                                    //socket.io.reconnection = true;
                             //                                    socket.connect();
                             //                                    socket.emit('join', { email: authService.authentication.userName });

                             //                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "socket reconnected");
                             //                                }

                             //                                simpalTek.global.localStorage.set("UserDb", response.data.result);

                             //                                $timeout(function () {

                             //                                    $ionicLoading.hide();

                             //                                    navigator.notification.alert("Your device has been successfully saved.", function () {

                             //                                    }, "Device Register", "Ok");

                             //                                    simpalTek.common.redirect('app/garageDoorControl');
                             //                                }, 500);

                             //                            }, function (err) {
                             //                                //simpalTek.helper.messageCenterService("Error on reconnect user preload data: " + JSON.stringify(err), simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown, 10000);
                             //                            });


                             //                            //console.log(response)
                             //                            if (response.data.status == 0) {
                             //                                //$ionicPopup.alert({
                             //                                //    title: 'Device Register',
                             //                                //    template: 'Your device has been successfully saved.'
                             //                                //});
                             //                            }
                             //                            else {

                             //                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(response));

                             //                                //$ionicPopup.alert({
                             //                                //    title: 'Device Register',
                             //                                //    template: 'There was an issue saving the device information.'
                             //                                //});
                             //                            }

                             //                        }, function (err) {

                             //                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(err));
                             //                            $ionicLoading.hide();

                             //                            $timeout(function () {
                             //                                navigator.notification.confirm(
                             //                                "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                             //                                 function (buttonIndex) {
                             //                                     if (buttonIndex == 2) {
                             //                                         $scope.retryAddDevice();
                             //                                     }
                             //                                 }, "Failed Cloud Service Registration",
                             //                                  ['Cancel', 'Retry']
                             //                                );

                             //                                //$ionicPopup.confirm({
                             //                                //    title: 'Failed Cloud Service Registration',
                             //                                //    template: 'There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?',
                             //                                //    buttons: [
                             //                                //               { text: 'Cancel' },
                             //                                //                 {
                             //                                //                     text: '<b>Retry</b>',
                             //                                //                     type: 'button-positive',
                             //                                //                     onTap: function (e) {
                             //                                //                         $scope.retryAddDevice();
                             //                                //                     }
                             //                                //                 }
                             //                                //    ]
                             //                                //});

                             //                                $scope.registeredDeviceCount = 0;
                             //                                $scope.wifiList = [];

                                                             
                             //                                //    $scope.showRetry = true;

                             //                                //    navigator.notification.alert("There was an issue saving the device information.", function () {

                             //                                //    }, "Device Register Error", "Ok");

                             //                                //    angular.element(document).ready(function () {
                             //                                //        $timeout(function () {
                             //                                //            $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                             //                                //        }, 500);
                             //                                //    });

                             //                            }, 1000);
                             //                        })

                                                     
                             //                    //}, 3000);

                                                 
                             //                }

                             //            }, function (e) {
                             //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
                             //            });

                             //            if (reconnectCounter >= 30) {
                             //                clearInterval(intervalReconnect)

                             //                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "register adding error:" + JSON.stringify(err));

                             //                $timeout(function () {
                             //                    navigator.notification.confirm(
                             //                    "There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?", // message
                             //                     function (buttonIndex) {
                             //                         if (buttonIndex == 2) {
                             //                             $scope.retryAddDevice();
                             //                         }
                             //                     }, "Failed Cloud Service Registration",
                             //                      ['Cancel', 'Retry']
                             //                    );

                             //                    //$ionicPopup.confirm({
                             //                    //    title: 'Failed Cloud Service Registration',
                             //                    //    template: 'There was an issue trying to save your device information to the cloud service.  This issue is typically caused by a slow reconnect to your home WiFi network. Please check if you are on your home WiFi network and retry.  Would you like to retry?',
                             //                    //    buttons: [
                             //                    //               { text: 'Cancel' },
                             //                    //                 {
                             //                    //                     text: '<b>Retry</b>',
                             //                    //                     type: 'button-positive',
                             //                    //                     onTap: function (e) {
                             //                    //                         $scope.retryAddDevice();
                             //                    //                     }
                             //                    //                 }
                             //                    //    ]
                             //                    //});
                                                                                                 

                             //                    $scope.registeredDeviceCount = 0;
                             //                    $scope.wifiList = [];

                             //                    $ionicLoading.hide();
                             //                    //    $scope.showRetry = true;

                             //                    //    navigator.notification.alert("There was an issue saving the device information.", function () {

                             //                    //    }, "Device Register Error", "Ok");

                             //                    //    angular.element(document).ready(function () {
                             //                    //        $timeout(function () {
                             //                    //            $ionicScrollDelegate.$getByHandle('mainContent').scrollTop();
                             //                    //        }, 500);
                             //                    //    });

                             //                }, 1000);

                             //            }
                             //            reconnectCounter += 5;
                             //        },5000);

                             //        //$timeout(function () {
                                         

                             //        //}, (ionic.Platform.isAndroid())?30000:10000);


                             //    }
                             //    else {

                             //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "[2]-NG save device config error info: " + JSON.stringify(response));

                             //        $timeout(function () {
                             //            $ionicLoading.hide();
                             //            //$ionicPopup.alert({
                             //            //    title: 'Device Register Error',
                             //            //    template: 'There was an issue saving the device information.'
                             //            //});
                             //            navigator.notification.alert("[2]-There was an issue configuring the device.", function () {

                             //            }, "Device Configuration Error", "Ok");
                             //        }, 500);
                             //    }

                             //}).error(function (err, status) {
                             //    //$ionicPopup.alert({
                             //    //    title: 'Device Register Error',
                             //    //    template: 'There was an issue saving the device information.'
                             //    //});

                             //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "[1]-NG save device info:" + JSON.stringify(err));

                             //    $timeout(function () {
                             //        $ionicLoading.hide();

                             //        navigator.notification.alert("[1]-There was an issue configuring the device.", function () {

                             //        }, "Device Configuration Error", "Ok");

                             //        //console.log("Device config error", err, status)
                             //    }, 500);

                             //});
                         }
                         else {
                             $ionicLoading.hide();
                         }
                     },            // callback to invoke with index of button pressed
                    'Verify WiFi account info',           // title
                    ['No', 'Yes']     // buttonLabels
                );


                

                //$http.put(ngAuthSettings.apiDeviceBaseUri + '/config', deviceConfigPayload)
                //.success(function (response) {
                //    console.log(response)

                //    //set the vendor device id to localstorage so we can later save it back to the server.
                //    simpalTek.global.localStorage.set("currentVendorDeviceId", _deviceInfo.vendorDeviceId);

                //    if (response.data.status == 0) {
                //        $ionicPopup.alert({
                //            title: 'Device Register',
                //            template: 'Your device has been successfully saved.'
                //        });
                //    }
                //    else {
                //        $ionicPopup.alert({
                //            title: 'Device Register Error',
                //            template: 'There was an issue saving the device information.'
                //        });
                //    }


                //}).error(function (err, status) {
                //    $ionicPopup.alert({
                //        title: 'Device Register Error',
                //        template: 'There was an issue saving the device information.'
                //    });
                //    console.log("Device config error", err, status)
                //});



            };

        //}
}])

;
