app.controller("GeofenceCtrl", function (
    $scope,
    $ionicLoading,
    $state,
    geofence,
    Geofence,
    $window,
    $rootScope,
    $timeout
) {

    $scope.geofence = geofence;
    $scope.TransitionType = TransitionType;
    
    $scope.center = {
        lat: geofence.latitude,
        lng: geofence.longitude,
        zoom: 17
    };
    $scope.markers = {
        marker: {
            draggable: true,
            message: geofence.identifier,
            lat: geofence.latitude,
            lng: geofence.longitude,
            icon: {}
        }
    };
    $scope.paths = {
        circle: {
            type: "circle",
            radius: geofence.radius,
            latlngs: $scope.markers.marker,
            clickable: false
        }
    };

    $scope.isTransitionOfType = function (transitionType) {
        return ($scope.geofence.transitionType & transitionType);
    };

    $scope.isWhenGettingCloser = function () {
        return $scope.geofence.transitionType === TransitionType.ENTER;
    };

    $scope.toggleWhenIgetCloser = function () {
        $scope.geofence.transitionType ^= TransitionType.ENTER;
    };

    $scope.toggleWhenIamLeaving = function () {
        $scope.geofence.transitionType ^= TransitionType.EXIT;
    };

    $scope.save = function () {
        if (validate()) {
            
            //var params = [geofence.id, $scope.markers.marker.lat, $scope.markers.marker.lng, parseInt($scope.paths.circle.radius).toString()];
            
            //$window.plugins.DGGeofencing.startMonitoringRegion(params, function (result) { console.log('watching'); $state.go("app.geofences");}, function (error) {
            //    console.log("failed to add region");
            //});
            
            //$window.plugins.DGGeofencing.startMonitoringSignificantLocationChanges(
            //    function (result) {
            //        console.log("Location Monitor Success: " + result);
            //    },
            //    function (error) {
            //        console.log("failed to monitor location changes");
            //    }
            //);

            //var params = ["2", "40.781552", "-73.967171", "150"];
            //$window.plugins.DGGeofencing.startMonitoringRegion(params, function (result) { console.log('watching'); }, function (error) {
            //    console.log("failed to add region");
            //});

            //window.plugins.DGGeofencing.startMonitoringRegion(params, function (result) { 
            //    $state.go("app.geofences");
            //}, function (error) {
            //    $ionicLoading.show({
            //        template: "Failed to add geofence, check if your location provider is enabled",
            //        duration: 3000
            //    });
            //    console.log("Failed to add geofence", error);
            //});
            

            //$scope.geofence.radius = parseInt($scope.paths.circle.radius);
            //$scope.geofence.latitude = $scope.markers.marker.lat;
            //$scope.geofence.longitude = $scope.markers.marker.lng;
            //$scope.geofence.notification.data = angular.copy($scope.geofence);
 
            $scope.geofence.latitude = $scope.markers.marker.lat;
            $scope.geofence.longitude = $scope.markers.marker.lng;
            $scope.geofence.identifier = $scope.geofence.identifier;
            $scope.geofence.radius = parseInt($scope.paths.circle.radius);
            $scope.geofence.notifyOnEntry = true; //($scope.geofence.transitionType == TransitionType.ENTER || $scope.geofence.transitionType == TransitionType.BOTH) ? true : false;
            $scope.geofence.notifyOnExit = true; //($scope.geofence.transitionType == TransitionType.EXIT || $scope.geofence.transitionType == TransitionType.BOTH) ? true : false;
            $scope.geofence.notifyOnDwell = true;
            $scope.geofence.loiteringDelay = undefined;
            //$scope.geofence.notification.data = angular.copy($scope.geofence);
            

            //$scope.geofence.notification.data = angular.copy($scope.geofence);

            Geofence.addOrUpdate($scope.geofence).then(function () {

            //simpalTek.lib.bgGeo.addOrUpdate($scope.geofence);
            $timeout(function () {
                $state.go("app.geofences");
            }, 100);
                
            }, function (error) {
                $ionicLoading.show({
                    template: "Failed to add geofence, check if your location provider is enabled",
                    duration: 3000
                });
                console.log("Failed to add geofence", error);
            });
        }
    };

    function validate () {
        if (!$scope.geofence.identifier) {
            $ionicLoading.show({
                template: "Please enter some notification text.",
                duration: 3000
            });
            return false;
        }

        if ($scope.geofence.transitionType === 0) {
            $ionicLoading.show({
                template: "You must select when you want notification. When entering or/and exiting region?",
                duration: 3000
            });
            return false;
        }

        return true;
    }
});
