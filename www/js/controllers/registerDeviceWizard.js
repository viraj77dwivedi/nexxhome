﻿app.controller("RegisterDeviceWizardCtrl", function (
    $scope,
    WizardHandler,
    $timeout,
    authService,
    ngAuthSettings,
    $interval,
    $ionicLoading,
    Geofence,
    $http,
    $q,
    $state,
    $ionicPlatform,
    $sce
) {
    $scope.wifiList = [];
    $scope.config = {};
    $scope.choice_ssid = "";
    $scope.wifiNetworkIssue = false;
    $scope.startConfiguring = false;
    $scope.showConfigSection1 = true;
    $scope.showConfigSection2 = false;
    $scope.showConfigSection3 = false;
    $scope.startRegister2cloud = false;
    //$scope.editDeviceId = null;
    //$scope.editVendorDeviceId = null;

    var userDb = simpalTek.global.localStorage.get('UserDb');
    var _selectedWifiSsId = "";
    var _vendorDeviceId = "";
    var _deviceInfo = {};
    var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
    var currentGeoLoc = {};
    var userFirstName = "";
    var resetWiFiTimer = null;

    $scope.ngConnectStage = "";

    if (!currentNexxGarage) {
        currentNexxGarage = {};
    }

    try {
        userFirstName = authService.authentication.userFullName.split(" ")[0];
    } catch (e) { }

    $scope.config.locationNickName = userFirstName + "'s home garage";

    Geofence.getCurrentPosition().then(function (position) {
        console.log("Current position found", position);

        if (position.coords) {
            currentGeoLoc.latitude = position.coords.latitude;
            currentGeoLoc.longitude = position.coords.longitude;

            simpalTek.helper.geo().add($scope.config.locationNickName, currentGeoLoc.latitude, currentGeoLoc.longitude,
                function () {

                }
            );
        }


    }, function (reason) {
        console.log("Cannot obtain current location", reason);

    });

    $scope.countdownFinished = function () {
        $scope.wifiNetworkIssue = true;
        $interval.cancel(resetWiFiTimer);
    }

    $scope.resetDeviceAlert = function () {
        var msg = "The device has already been configured.  To reset the device to configuration mode, hold the reset button for about 10 seconds or until you see a steady green light.";

        navigator.notification.alert(msg, function () {

        }, "Reset configuration mode", "Ok");
    }

    $scope.closeModalRegisterDeviceWizard = function () {
        $scope.$parent.modalRegisterDeviceWizard.hide();
        $interval.cancel(resetWiFiTimer);
    }

    $scope.nextTab = function(){
         WizardHandler.wizard().next(); 
    }

    $scope.goWifiSettings = function () {

        WizardHandler.wizard().next();
        
        //retrieve wifi networks from NG device timeout after 3.5 mins
        $timeout(function () {
            $scope.wifiNetworkIssue = true;
            $interval.cancel(resetWiFiTimer);
        }, 210000);

        $timeout(function () {

            resetWiFiTimer = $interval(function () {

                $scope.getList();

                if (!$scope.wifiList)
                    $scope.wifiList = [];

                if ($scope.wifiList.length > 0)
                    $interval.cancel(resetWiFiTimer);
            }, 2000);

            $("#detectWiFiCounter").show();

        }, 500);


        $timeout(function () {
            $interval.cancel(resetWiFiTimer);
        }, 300000);  //stop the interval after 5 min

                
    }

    $scope.openConfigSection3 = function () {
        $scope.showConfigSection2 = false;
        $scope.showConfigSection3 = true;
        $scope.startConfiguring = false;
    }

    $scope.backWiFiSetting = function () {
        $scope.startConfiguring = false;
        $scope.showConfigSection1 = true;
        $scope.showConfigSection2 = false;
        $scope.showConfigSection3 = false;

        WizardHandler.wizard().goTo(1);
    }

    $scope.goCloudRegister = function () {

        var deviceExist = ((userDb.registeredDevices.filter(function (x) { return x.deviceId == $scope.editDeviceId }).length > 0)? true: false);

        if (deviceExist) {
            if (window.cordova) {
                navigator.notification.alert("Your device has been successfully configured.", function () {

                }, "Device Configured", "Ok");
            }

            $scope.$parent.finishRegisterDevice();

            $scope.startRegister2cloud = false;
            $scope.closeModalRegisterDeviceWizard();
        }
        else
            WizardHandler.wizard().next();

    }

    $scope.getList = function () {
        $ionicPlatform.ready(function () {
                if (window.cordova) {
                    WifiWizard.getCurrentSSID(function (ssid) {
                        $scope.currentSSID = ssid;

                        if (ssid.toLowerCase().indexOf("nexxgarage") > 0)
                            isConnected2NexxGarageWiFi = true;
                        else
                            isConnected2NexxGarageWiFi = false;

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "You are connect to WiFi network " + ssid);
                    }, function (e) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard current WiFi network error " + JSON.stringify(e));
                    });

                    //WifiWizard.listNetworks(function (networks) {
                    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WiFi network " + JSON.stringify(networks));
                    //}, function (e) {
                    //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "WifiWizard WiFi network error " + JSON.stringify(e));
                    //});

                }
            });

        $scope.ngConnectStage = "Trying to connect to NexxGarage WiFi.";

            $.ajax({
                url: ngAuthSettings.apiDeviceBaseUri + '/heart?t=' + new Date().getTime(),
                type: 'GET',
                statusCode: {
                    200: function (response) {

                        $scope.ngConnectStage = "Successfully connect to NexxGarage WiFi.  Retrieving device information.";

                        try {
                            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "NG device info:" + JSON.stringify(deviceInfoData));
                            
                            $.ajax({
                                url: ngAuthSettings.apiDeviceBaseUri + '/device-info?t=' + new Date().getTime(),
                                type: 'GET',
                                statusCode: {
                                    200: function (response) {
                                        $scope.ngConnectStage = "Retrieved device information successfully.  Retrieving available WiFi networks.";

                                        _deviceInfo.vendorDeviceId = response.device_id;
                                        currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;

                                        $.ajax({
                                            url: ngAuthSettings.apiDeviceBaseUri + '/networks?t=' + new Date().getTime(),
                                            type: 'GET',
                                            statusCode: {
                                                200: function (response) {
                                                    try {
                                                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "Wifi Networks :" + JSON.stringify(networkData));

                                                        $scope.wifiList = response.networks;
                                                        $interval.cancel(resetWiFiTimer);

                                                    } catch (e) {
                                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device network error");
                                                    }
                                                }
                                            },
                                            error: function (jqXHR, status, error) {
                                                if (status != 200) {
                                                    $scope.ngConnectStage = $sce.trustAsHtml("<font style='color:red'>There was an error trying to retrieve the WiFi networks.</font>");

                                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting network api :" + JSON.stringify(jqXHR) + "|error:" + error);
                                                }
                                            },
                                            complete: function (event, request, settings) {
                                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Complete event - Getting network api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                                            }

                                        });
                                    }
                                },
                                error: function (jqXHR, status, error) {
                                    if (status != 200) {
                                        $scope.ngConnectStage = $sce.trustAsHtml("<font style='color:red'>There was an error trying to retrieve the device information.</font>");
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting device api :" + JSON.stringify(jqXHR));
                                    }
                                },
                                complete: function (event, request, settings) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Complete event - Getting device api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                                }
                            });

                        } catch (e) {
                            $scope.ngConnectStage = $sce.trustAsHtml("<font style='color:red'>There was an error during device information.</font>");
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting NG device info error " + JSON.stringify(e));
                        }

                        
                    }
                },
                error: function (jqXHR, status, error) {

                    if (status != 200) 
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Getting heart api :" + JSON.stringify(jqXHR));
                },
                complete: function (event, request, settings) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Complete event - Getting heart api : event-" + JSON.stringify(event) + "|request-" + JSON.stringify(request) + "|settings-" + JSON.stringify(settings));
                }

            });
    }

    var addDevice = function () {
        _deviceInfo.NickName = $scope.config.locationNickName;
        _deviceInfo.GPSLoc = JSON.stringify(currentGeoLoc);
        _deviceInfo.ProductCode = "NXGRG";
        var deviceNickNameExist = ((userDb.registeredDevices.filter(function(x){ return x.locationNickName.trim().toLowerCase() == _deviceInfo.NickName.trim().toLowerCase()}).length > 0)? true: false);


        if (deviceNickNameExist) {
            navigator.notification.alert("The device nickname you entered already exist.  Please select another nickname.", function () {
               
            }, "Nickname already exist", "Ok");

            $scope.startRegister2cloud = false;

            return;
        }


        if (!currentNexxGarage)
            currentNexxGarage = {};

        $http.post(ngAuthSettings.apiDomainServiceBaseUri + '/Device', _deviceInfo)
        .then(function (response) {

            currentNexxGarage.deviceId = response.data.result.id;
            currentNexxGarage.vendorDeviceId = _deviceInfo.vendorDeviceId;
            currentNexxGarage.garageDoorState = "Unknown";

            simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)

            $http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/').then(function (response) {

                simpalTek.global.localStorage.set("UserDb", response.data.result);

                $timeout(function () {

                    $ionicLoading.hide();

                    if (window.cordova) {
                        navigator.notification.alert("Your device has been successfully saved.", function () {

                        }, "Device Register", "Ok");
                    }

                    //if (response.data.result.registeredDevices.length == 1) 
                        $scope.$parent.finishRegisterDevice();

                }, 500);

            }, function (err) {
                
                //simpalTek.helper.messageCenterService("Error on reconnect user preload data: " + JSON.stringify(err), simpalTek.Enum.messageType.danger, simpalTek.Enum.messageStatus.shown, 10000);
            });

            $scope.startRegister2cloud = false;
            $scope.closeModalRegisterDeviceWizard();

        }, function (err) {

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Error adding device to cloud: " + err);

            $timeout(function () {
                $scope.startRegister2cloud = false;
                $ionicLoading.hide();
                //$scope.showRetry = true;

                navigator.notification.alert("There was an issue saving the device information.  Please check if you are back on your WiFi network and tap on the \"Register to Cloud\" button to retry.", function () {

                }, "Device Register Error", "Ok");

                
            }, 100);
        })

    }
        
    $scope.goVerify = function(){

        var rules = {};
        var messages = {};
        var errMsg = "";
                
        //rules.wifiPassword = { required: true };
        rules.wifiNetwork = { required: true };

        //messages.wifiPassword = { required: "Your Wifi password is required" };
        messages.wifiNetwork = { required: "Your must select your home WiFi network" };

        var validator = new simpalTek.helper.jQueryFormValidator("formWizardConfig", rules, messages);

        if (validator.validate()) {
            WizardHandler.wizard().next();
        }
    }

    $scope.register2cloud = function () {
        var rules = {};
        var messages = {};

        rules.locationNickName = { required: true };

        messages.locationNickName = { required: "Your location Nick Name is required" };

        var validator = new simpalTek.helper.jQueryFormValidator("formWizardCloud", rules, messages);
        var pingTryCount = 0;

        if (validator.validate()) {
            
            $scope.startRegister2cloud = true;

            addDevice();
         }
    }

    $scope.saveDeviceConfig = function () {
        var deviceConfigPayload = {};
        var deviceInfo = {};
        var rules = {};
        var messages = {};
        var errMsg = "";

        $scope.startConfiguring = true;

        //deviceConfigPayload.jsonVersion = "2";
        deviceConfigPayload.name = "NexxGarage";
        deviceConfigPayload.device_id = _deviceInfo.vendorDeviceId;
        deviceConfigPayload.userAccount = authService.authentication.userName;
        deviceConfigPayload.userId = authService.authentication.userName;
        deviceConfigPayload.wifi = {};
        deviceConfigPayload.wifi.ssid = $scope.config.ssId;
        deviceConfigPayload.wifi.password = encodeURIComponent($scope.config.wifiPassword);
        deviceConfigPayload.mqtt = {};
        deviceConfigPayload.mqtt.host = userDb.appSettings.mqttHost;
        deviceConfigPayload.mqtt.port = parseInt(userDb.appSettings.mqttPort);
        deviceConfigPayload.mqtt.base_topic = "devices/";
        deviceConfigPayload.mqtt.auth = Boolean(userDb.appSettings.mqttAuth);
        deviceConfigPayload.mqtt.username = userDb.appSettings.mqttAcctUserName;
        deviceConfigPayload.mqtt.password = userDb.appSettings.mqttAcctPwd;
        deviceConfigPayload.mqtt.ssl = Boolean(userDb.appSettings.mqttSSL);
        deviceConfigPayload.mqtt.fingerprint = "79 61 3B 45 F3 2E D1 66 9D 35 9C 24 01 35 2F C9 05 AA 9C 57"
        deviceConfigPayload.ota = {};
        deviceConfigPayload.ota.enabled = true;
        deviceConfigPayload.ota.host = userDb.appSettings.otaHost;
        deviceConfigPayload.ota.port = parseInt(userDb.appSettings.otaPort);
        deviceConfigPayload.ota.path = "/ota"
        
        $http({
            method: 'PUT',
            url: ngAuthSettings.apiDeviceBaseUri + '/config',
            data: deviceConfigPayload,
            headers: { 'Content-Type': 'application/json;charset=utf-8' }
        }).success(function (response) {
            console.log(response)

            ////set the vendor device id to localstorage so we can later save it back to the server.
            //simpalTek.global.localStorage.set("currentVendorDeviceId", _deviceInfo.vendorDeviceId);

            if (response.success == true) {

                //clear all existing geo location
                //Geofence.removeAll();
                
                $scope.showConfigSection1 = false;
                $scope.showConfigSection2 = true;

            }
            else {

                $scope.showConfigSection1 = false;
                $scope.showConfigSection2 = false;
                $scope.showConfigSection3 = true;

                $timeout(function () {
                    $ionicLoading.hide();

                    navigator.notification.alert("There was an issue configuring the device.", function () {

                    }, "Device Register Error", "Ok");
                }, 100);
            }

        }).error(function (err, status) {

            $timeout(function () {
                $ionicLoading.hide();

                navigator.notification.alert("There was an issue while sending configuration information to the device.", function () {

                }, "Device Register Error", "Ok");

                console.log("Device config error", err, status)
            }, 100);

        });
                
    };


});