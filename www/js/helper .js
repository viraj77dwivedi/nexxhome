﻿simpalTek.helper = simpalTek || {};

simpalTek.helper.test = function (){
    alert("hello")
}

simpalTek.helper.geo = function (){
    
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //:::                                                                         :::
    //:::  This routine calculates the distance between two points (given the     :::
    //:::  latitude/longitude of those points). It is being used to calculate     :::
    //:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
    //:::                                                                         :::
    //:::  Definitions:                                                           :::
    //:::    South latitudes are negative, east longitudes are positive           :::
    //:::                                                                         :::
    //:::  Passed to function:                                                    :::
    //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
    //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
    //:::    unit = the unit you desire for results                               :::
    //:::           where: 'M' is statute miles (default)                         :::
    //:::                  'K' is kilometers                                      :::
    //:::                  'N' is nautical miles                                  :::
    //:::                                                                         :::
    //:::  Worldwide cities and other features databases with latitude longitude  :::
    //:::  are available at http://www.geodatasource.com                          :::
    //:::                                                                         :::
    //:::  For enquiries, please contact sales@geodatasource.com                  :::
    //:::                                                                         :::
    //:::  Official Web site: http://www.geodatasource.com                        :::
    //:::                                                                         :::
    //:::               GeoDataSource.com (C) All Rights Reserved 2015            :::
    //:::                                                                         :::
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    this.distance = function distance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1 / 180
        var radlat2 = Math.PI * lat2 / 180
        var radlon1 = Math.PI * lon1 / 180
        var radlon2 = Math.PI * lon2 / 180
        var theta = lon1 - lon2
        var radtheta = Math.PI * theta / 180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180 / Math.PI
        dist = dist * 60 * 1.1515
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }
        if (unit == "F") { dist = dist * 5280 }
        return dist
    }
    
    this.add = function (nickName, lat, lon, cb) {
        var geofence = {};
        var radius = 200;
        var Geofence = angular.element(document.body).injector().get('Geofence');
        
        geofence.latitude = parseFloat(lat);
        geofence.longitude = parseFloat(lon);
        geofence.identifier = nickName;
        geofence.radius = parseFloat(radius);
        geofence.notifyOnEntry = true;
        geofence.notifyOnExit = true;
        geofence.notifyOnDwell = true;
        geofence.loiteringDelay = undefined;
                
        Geofence.addOrUpdate(geofence).then(function () {
            cb();
        }, function (error) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Failed to add geofence on configure device " + JSON.stringify(error));
            console.log("Failed to add geofence on configure device", error);
        });

        geofence.identifier = nickName + "-monitor";
        geofence.radius = 550;

        Geofence.addOrUpdate(geofence).then(function () {
            //cb();
        }, function (error) {
            //simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Failed to add geofence on configure device " + JSON.stringify(error));
            //console.log("Failed to add geofence on configure device", error);
        });
    }

    return this;

    //return {
    //    distance: distance,
    //}
}

simpalTek.helper.networkConnection = function () {
    var networkState = null;

    if (!window.cordova)
        return "";

    try{
        networkState = navigator.connection.type;

        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';

        return states[networkState];
    }
    catch(e)
    {
        return "Error - Unknown connection";
    }
}

simpalTek.helper.device = function(){

    this.hidStatusBar = function () {
        console.log("hidStatusBar")
        document.addEventListener("deviceready", function () {

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (ionic.Platform.isAndroid()) {
                window.addEventListener("native.hidekeyboard", function () {
                    //show stuff on keyboard hide
                    StatusBar.hide();
                    window.AndroidFullScreen.immersiveMode(false, false);
                });
            }

            if (window.StatusBar) {
                StatusBar.hide();
                ionic.Platform.fullScreen();
            }



        }, false);
    }

    return this;
}

simpalTek.helper.sendMail = function (to, msg, subject) {
    var $injector = angular.element(document.body).injector();
    var $http = $injector.get('$http');
    var ngAuthSettings = $injector.get('ngAuthSettings');
    var authService = $injector.get('authService');
    var mailMsg = {};
    var userDb = simpalTek.global.localStorage.get('UserDb');
    var vendorDeviceId = "";

    mailMsg.userName = authService.authentication.userName;

    if(window.cordova)
        mailMsg.appVersion = AppVersion.version;

    mailMsg.deviceInfo = ionic.Platform.device();

    try {
        if (userDb) {
            if (userDb.registeredDevices.length > 0)
                mailMsg.vendorDeviceId = userDb.registeredDevices[0].deviceName;
                        
        }
    } catch (e) { };

    //if (typeof (msg) == "object")
    //    mailMsg.message = JSON.stringify(msg);
    //else
        mailMsg.message = msg;

        $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/SendMail", { SenderEmail: "no-reply@nexxgarage.com", RecipientEmail: to, Subject: subject, Message: JSON.stringify(mailMsg) })

}

simpalTek.helper.getUserPreloadData = function (cbSuccess, cbError) {
    var $http = angular.element(document.body).injector().get('$http');
    var $timeout = angular.element(document.body).injector().get('$timeout');
    var ngAuthSettings = angular.element(document.body).injector().get('ngAuthSettings');
    var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
    var $rootScope = angular.element(document.body).injector().get('$rootScope');
    var messageCenterService = angular.element(document.body).injector().get('messageCenterService');
    var $injector = angular.element(document.body).injector();
    var authService = $injector.get('authService');

    simpalTek.global.isDataRefreshed = false;
    simpalTek.global.gettingPreloadData = true;

    simpalTek.helper.http().get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/', null,
        function (response) {
            //if (response.data.result && response.data.result != "") {

            simpalTek.global.localStorage.set("UserDb", response.result);

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Preload data retrieved.");

            if (response.result.devicesAccess.length > 0) {

                if (currentNexxGarage != null) {
                    var currentDevice = response.result.devicesAccess.filter(function (item) { return item.vendorDeviceId == currentNexxGarage.vendorDeviceId })

                    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume currentNexxGarage.vendorDeviceId " + currentNexxGarage.vendorDeviceId);

                    if (currentDevice.length > 0) {
                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume current device info set");

                        currentNexxGarage.garageDoorState = currentDevice[0].currentDoorState;
                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
                    }
                }
            }
            //}

            $timeout(function () {
                //    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume broadcast loadCurrentNexxGarageState");

                try {
                    $rootScope.gateActivitiesCount = response.result.gateActivities.length;
                } catch (e) { }

                $rootScope.$broadcast('loadCurrentNexxGarageState');
            }, 500);

            if (messageCenterService.mcMessages.length > 0) {
                if (messageCenterService.mcMessages[0].message == simpalTek.const.NO_INTERNET_MSG
                    || messageCenterService.mcMessages[0].message == simpalTek.const.CURRENT_DATA_BEING_RETRIEVED_MSG
                    || messageCenterService.mcMessages[0].message == simpalTek.const.PERLOAD_RETRIEVE_ERROR_MSG
                )
                    messageCenterService.remove();

            }

            simpalTek.global.isDataRefreshed = true;
            simpalTek.global.gettingPreloadData = false;

            cbSuccess(response);
        },
        function (error, status) {

            if (messageCenterService.mcMessages.length > 0) {
                if (messageCenterService.mcMessages[0].message != simpalTek.const.NO_INTERNET_MSG)
                    simpalTek.helper.messageCenterService(simpalTek.const.PERLOAD_RETRIEVE_ERROR_MSG, simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown);
            }
            else
                simpalTek.helper.messageCenterService(simpalTek.const.PERLOAD_RETRIEVE_ERROR_MSG, simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.shown);

             cbError(error);
        });

    //$.ajax({
    //    url: ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/',
    //    type: 'GET',
    //    headers: {
    //        'Content-Type': 'application/json;charset=utf-8'
    //        , 'Authorization': 'bearer ' + simpalTek.global.localStorage.get('authorizationData').token
    //    },
    //    contentType: 'application/json',
    //    statusCode: {
    //        200: function (response) {

    //            //if (response.data.result && response.data.result != "") {
    //            simpalTek.global.localStorage.set("UserDb", response.result);

    //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Preload data retrieved.");

    //            if (response.result.devicesAccess.length > 0) {

    //                if (currentNexxGarage != null) {
    //                    var currentDevice = response.result.devicesAccess.filter(function (item) { return item.vendorDeviceId == currentNexxGarage.vendorDeviceId })

    //                    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume currentNexxGarage.vendorDeviceId " + currentNexxGarage.vendorDeviceId);

    //                    if (currentDevice.length > 0) {
    //                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume current device info set");

    //                        currentNexxGarage.garageDoorState = currentDevice[0].currentDoorState;
    //                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
    //                    }
    //                }
    //            }
    //            //}

    //            $timeout(function () {
    //                //    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume broadcast loadCurrentNexxGarageState");

    //                try {
    //                    $rootScope.gateActivitiesCount = response.result.gateActivities.length;
    //                } catch (e) { }

    //                $rootScope.$broadcast('loadCurrentNexxGarageState');
    //            }, 500);

    //            if (messageCenterService.mcMessages.length > 0) {
    //                if (messageCenterService.mcMessages[0].message == simpalTek.const.NO_INTERNET_MSG
    //                    || messageCenterService.mcMessages[0].message == simpalTek.const.CURRENT_DATA_BEING_RETRIEVED_MSG
    //                )
    //                    messageCenterService.remove();

    //            }


    //            cbSuccess();
    //        },
    //        401: function () {
    //            //alert("The username or password were not correct. Try again.");
    //        }
    //    },
    //    success: function (data) {
    //        console.info(data);
    //    },
    //    error: function (jqXHR, status, error) {
    //        cbError(error);
    //    }

    //});

    //$http.get(ngAuthSettings.apiDomainServiceBaseUri + '/UserPreloadData/').then(function (response) {

    //    //if (response.data.result && response.data.result != "") {
    //        simpalTek.global.localStorage.set("UserDb", response.data.result);

    //        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Preload data retrieved.");

    //        if (response.result.devicesAccess.length > 0) {

    //            if (currentNexxGarage != null) {
    //                var currentDevice = response.data.result.devicesAccess.filter(function (item) { return item.vendorDeviceId == currentNexxGarage.vendorDeviceId })

    //                //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume currentNexxGarage.vendorDeviceId " + currentNexxGarage.vendorDeviceId);

    //                if (currentDevice.length > 0) {
    //                    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume current device info set");

    //                    currentNexxGarage.garageDoorState = currentDevice[0].currentDoorState;
    //                    simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage);
    //                }
    //            }
    //        }
    //    //}

    //    $timeout(function () {
    //        //    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "onResume broadcast loadCurrentNexxGarageState");

    //        try {
    //            $rootScope.gateActivitiesCount = response.data.result.gateActivities.length;
    //        } catch (e) { }

    //        $rootScope.$broadcast('loadCurrentNexxGarageState');
    //    }, 500);

    //    if (messageCenterService.mcMessages.length > 0) {
    //        if (messageCenterService.mcMessages[0].message == simpalTek.const.NO_INTERNET_MSG
    //            || messageCenterService.mcMessages[0].message == simpalTek.const.CURRENT_DATA_BEING_RETRIEVED_MSG
    //        )
    //            messageCenterService.remove();

    //    }
        

    //    cbSuccess();

    //}, function (err) {
    //    cbError(err);
    //    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "getUserPreloadData UserPreloadData Error");
    //});
}

simpalTek.helper.log = function (logType, message) {
    var log = {};
    var $rootScope = angular.element(document.body).injector().get('$rootScope');
    var $timeout = angular.element(document.body).injector().get('$timeout');
    var $fileLogger = angular.element(document.body).injector().get('$fileLogger');

    log.errorSeverity = logType.name;
    log.message = message;
    log.date = new Date();


    $timeout(function () {
        simpalTek.global.logger.log.push(log);

        if (simpalTek.global.logger.log.length >= 300)
            simpalTek.global.logger.log.shift();

        //$fileLogger.writeLog(JSON.stringify(simpalTek.global.logger.log));
        //    simpalTek.global.logger.log = simpalTek.global.logger.log.slice(0, 99);
    }, 500);

   

}

simpalTek.helper.sendDoorSignal = function (data, cbSuccess, cbError) {
    var $http = angular.element(document.body).injector().get('$http');
    var $timeout = angular.element(document.body).injector().get('$timeout');
    var ngAuthSettings = angular.element(document.body).injector().get('ngAuthSettings');
    var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
    var authService = angular.element(document.body).injector().get('authService');
    var maxNoRetry = 20;
    var retryCount = 0;
    var signalEmittedSuccessful = false;

    simpalTek.global.doorSignalSentAcknowledged = false;
    simpalTek.global.doorStateChanged = false;

    data.mobileDeviceUUID = (simpalTek.global.deviceInfo.uuid == "" || simpalTek.global.deviceInfo.uuid == undefined) ? ionic.Platform.device().uuid : simpalTek.global.deviceInfo.uuid;
    data.SmartGateDeviceID = currentNexxGarage.vendorDeviceId;
    data.vendorDeviceId = currentNexxGarage.vendorDeviceId;
    data.deviceId = currentNexxGarage.deviceId;
    data.userName = authService.authentication.userName;
    data.userFullName = authService.authentication.userFullName;
    data.accessToken = authService.authentication.accessToken;
    data.sentDateTime = moment.utc().format('YYYY-MM-DD HH:mm:ss');//new Date().toISOString();
    
    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "signal pre-send");
             
    BackgroundGeolocation.startBackgroundTask(function (taskId) {

        simpalTek.helper.http().post(ngAuthSettings.apiDomainServiceBaseUri + '/GateControlSignal', data,
        function (response) {
            cbSuccess(response);

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Garage button send door signal successful");
            BackgroundGeolocation.finish(taskId);
        },
        function (error) {
            cbError(error, status);
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Garage button send door signal error " + error + " | " + status);
            BackgroundGeolocation.finish(taskId);
        });

        
    });

    

                   
                

}

simpalTek.helper.pushNotification = function () {

    var notification = simpalTek.global.localStorage.get('Notification');
    var userDb = simpalTek.global.localStorage.get('UserDb');
    
    simpalTek.global.pushNotification = PushNotification.init({
        android: {
            senderID: userDb.appSettings.gcmSenderId,
            alert: true,
            //badge: true,
            sound: true,
            //vibrate: true,
            clearBadge: true
        },
        //browser: {
        //    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        //},
        ios: {
            alert: true,
            //badge: true,
            sound: true,
            clearBadge: true
        },
        windows: {}
    });

    simpalTek.global.pushNotification.on('registration', function (data) {
        var $http = angular.element(document.body).injector().get('$http');
        var ngAuthSettings = angular.element(document.body).injector().get('ngAuthSettings');
        var currentRegisterId = "";
        var newRegisterId = "";

        try {

            ionic.Platform.ready(function () {
                notification = simpalTek.global.localStorage.get('Notification');
                deviceInfo = ionic.Platform.device();

                if (notification == null) {
                    notification = {};

                    notification.oldRegisterId = "";
                    notification.newRegisterId = "";
                }

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification registration " + JSON.stringify(data));

                newRegisterId = data.registrationId;
                currentRegisterId = notification.newRegisterId;

                if (notification.oldRegisterId != newRegisterId)
                    notification.oldRegisterId = currentRegisterId;

                notification.newRegisterId = newRegisterId;

                $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/MobileDevice", { mobileDeviceType: deviceInfo.platform, registeredId: data.registrationId, deviceId: (simpalTek.global.deviceInfo.uuid == "") ? deviceInfo.uuid : simpalTek.global.deviceInfo.uuid, deviceInfo: JSON.stringify(deviceInfo), oldRegisterId: notification.oldRegisterId })

                simpalTek.global.localStorage.set('Notification', notification);

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "notification object " + JSON.stringify(notification));
            });
        }
        catch (e) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification init : " + e);
        }
    });

    simpalTek.global.pushNotification.on('notification', function (data) {

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification data " + JSON.stringify(data));

        switch (data.title) {
            case "Nexx Garage Alert Open":

                simpalTek.helper.getUserPreloadData(function () {
                    simpalTek.global.isDataRefreshed = true;
                }, function () {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Open UserPreloadData Error");
                })

                break;
            case "Nexx Garage Alert Close":

                simpalTek.helper.getUserPreloadData(function () {
                    simpalTek.global.isDataRefreshed = true;
                }, function () {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData Error");
                })

                break;

            default:

                if (data.additionalData.activityType) {

                    switch (data.additionalData.activityType.toLowerCase()) {
                        case "open":
                        case "close":
                            simpalTek.helper.getUserPreloadData(function () {
                                simpalTek.global.isDataRefreshed = true;
                            }, function (error) {
                                if (data.additionalData.activityType == "Open")
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Open UserPreloadData : " + error);
                                else (data.additionalData.activityType == "Close")
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData : " + error);
                            });

                            break;
                        case "wakeup":
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify wakeup device");
                            break;
                        case "findme":

                            break;
                    }


                }

        }

        simpalTek.global.pushNotification.finish(function () {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "processing of push data is finished for ID = " + data.additionalData.notId);
        }, function () {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "something went wrong with push.finish for ID = " + data.additionalData.notId);
        }, data.additionalData.notId);

    });

    simpalTek.global.pushNotification.on('error', function (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification error " + JSON.stringify(e));
            });



}

simpalTek.helper.messageCenterService = function (message, type, status, timeout) {
    //var injector = angular.element(document).injector();
    //var messageCenterService = injector.get('messageCenterService');
    var messageCenterService = angular.element(document.body).injector().get('messageCenterService');
    var $ionicScrollDelegate = angular.element(document.body).injector().get('$ionicScrollDelegate');
    
    //if (status.name != TMKmobile.Enum.messageStatus.next.name)
    //    ionicScrollDelegateService.$getByHandle('mainContent').scrollTop();
    
    if (messageCenterService.mcMessages.length > 0) {
        messageCenterService.mcMessages[0].status = status.name;
        messageCenterService.mcMessages[0].type = type.name;
        messageCenterService.mcMessages[0].message = message;
    }
    else {
        if (timeout > 0)
            messageCenterService.add(type.name, message, { timeout: timeout });
        else
            messageCenterService.add(type.name, message, { status: status.name });
    }
}

simpalTek.helper.showLoader = function (show, msg) {
    var $ionicLoading = angular.element(document.body).injector().get('$ionicLoading');
    var loaderMsg = (msg == undefined) ? "Loading" : msg;

    if (show) {
        $ionicLoading.show({
            content: loaderMsg,
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
    }
    else {
        $ionicLoading.hide();
    }

}

simpalTek.helper.jQueryFormValidator = function (formName, rules, messages) {
    var formId = "#" + formName;
    
    this.validator = $(formId).validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        onkeyup: false,
        //onfocusout: false,
        rules: rules,
        messages: messages,
        
        //invalidHandler: function (event, validator) { //display error alert on form submit   
        //    $('.alert-danger', $('.login-form')).show();
        //},
        
        highlight: function (e) {
            //console.log("e", e)
            $(e).closest('label.block').addClass('has-error');
            $(e).closest('div.block').addClass('has-error'); //for testing
            $(e).closest('input, select').addClass('input-validation-error');

   
        },
        unhighlight: function (element, errorClass) {
                            //var id = element.id;
                            //$("#" + id).removeClass(errorClass);
        },
        success: function (d, e) {
            
            $(e).closest('label.block').removeClass('has-error');
            $(e).closest('input, select').removeClass('input-validation-error');
            $(e).closest('input, select').removeClass('has-error');
            $(e).closest('span.block').removeClass('has-error');
            //$(e).closest('span.errorGroup').removeClass('has-error');

            $(d).remove();
        },
        
        errorPlacement: function (error, element) {
            
            if (element.is('.select2')) {
                error.insertAfter(element.parents('div.ui-select'));
            }
            else if (element.is('.errorGroup')) {
                error.css("color", "#a94442");
                error.insertAfter($(element).closest('span.block'));
            }
            else {
                error.insertAfter(element);  // default placement
            }
        },
        
        submitHandler: function (form) {
        },
        invalidHandler: function (form) {
        }
    });
    
    this.validate = function () {
        var result = this.validator.form();
        
        //if (!result)
        //    $(window).scrollTop(0);  //$('ion-content').animate({ scrollTop: "0px" }, "slow");
        
        return result;
    }
    
    this.reset = function () {
        this.validator.resetForm();
        $(".input-validation-error").removeClass("input-validation-error");
        $(".has-error").removeClass("has-error");
    }
}

simpalTek.helper.http = function () {
    var $timeout = angular.element(document.body).injector().get('$timeout');
    var $location = angular.element(document.body).injector().get('$location');
    var authService = angular.element(document.body).injector().get('authService');
    var messageCenterService = angular.element(document.body).injector().get('messageCenterService');

    this.get = function (url, data, cbSuccess, cbFail) {
        httpExec('GET', url, data, cbSuccess, cbFail);
    }

    this.post = function (url, data, cbSuccess, cbFail) {
        httpExec('POST', url, data, cbSuccess, cbFail);
    }

    this.edit = function (url, data, cbSuccess, cbFail) {
        httpExec('PUT', url, data, cbSuccess, cbFail);
    }

    this.delete = function (url, data, cbSuccess, cbFail) {
        httpExec('DELETE', url, data, cbSuccess, cbFail);
    }

    var httpExec = function (method, url, data, cbSuccess, cbFail) {
        $.ajax({
            url: url,
            type: method,
            data: (data==null)?{}:JSON.stringify(data),
            contentType: 'application/json;charset=utf-8',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "BEARER " + ((simpalTek.global.localStorage.get('authorizationData'))?simpalTek.global.localStorage.get('authorizationData').token:""));
            },
            statusCode: {
                200: function (response) {
                    cbSuccess(response);
                },
                401: function () {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Service rejected.  User unauthorize.");

                    authService.logOut();

                    simpalTek.helper.messageCenterService(simpalTek.const.ACCESS_EXPIRE_MSG, simpalTek.Enum.messageType.info, simpalTek.Enum.messageStatus.next);

                    $timeout(function () {
                        $location.path('/signin');
                    }, 500);


                }
            },
            //success: function (response) {
            //    cbSuccess(response);
            //},
            error: function (error) {
                cbFail(error)
            },
            complete: function () {
                simpalTek.helper.showLoader(false);
            }
        });
    }

    return this;
}

simpalTek.helper.configUserMobileDeviceSetting = function () {
    var userDb = simpalTek.global.localStorage.get('UserDb');
    var currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
    var pushRegisterFailed = false;
    var mainUserRegisterDevice = null;
    var notification = simpalTek.global.localStorage.get('Notification');
    var authService = angular.element(document.body).injector().get('authService');

    try {
        deviceInfo = ionic.Platform.device();

        simpalTek.global.pushNotification = PushNotification.init({
            android: {
                senderID: userDb.appSettings.gcmSenderId,
                alert: true,
                //badge: true,
                sound: true,
                //vibrate: true,
                clearBadge: true
            },
            //browser: {
            //    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            //},
            ios: {
                alert: true,
                //badge: true,
                sound: true,
                clearBadge: true
            },
            windows: {}
        });

        simpalTek.global.pushNotification.on('registration', function (data) {
            var $http = angular.element(document.body).injector().get('$http');
            var ngAuthSettings = angular.element(document.body).injector().get('ngAuthSettings');
            var currentRegisterId = "";
            var newRegisterId = "";

            try {

                ionic.Platform.ready(function () {
                    notification = simpalTek.global.localStorage.get('Notification');
                    deviceInfo = ionic.Platform.device();

                    if (notification == null) {
                        notification = {};

                        notification.oldRegisterId = "";
                        notification.newRegisterId = "";
                    }

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification registration " + JSON.stringify(data));

                    newRegisterId = data.registrationId;
                    currentRegisterId = notification.newRegisterId;

                    if (notification.oldRegisterId != newRegisterId)
                        notification.oldRegisterId = currentRegisterId;

                    notification.newRegisterId = newRegisterId;

                    $http.post(ngAuthSettings.apiDomainServiceBaseUri + "/MobileDevice", { mobileDeviceType: deviceInfo.platform, registeredId: data.registrationId, deviceId: (simpalTek.global.deviceInfo.uuid == "") ? deviceInfo.uuid : simpalTek.global.deviceInfo.uuid, deviceInfo: JSON.stringify(deviceInfo), oldRegisterId: notification.oldRegisterId })

                    simpalTek.global.localStorage.set('Notification', notification);

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "notification object " + JSON.stringify(notification));
                });
            }
            catch (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification init : " + e);
            }
        });

        simpalTek.global.pushNotification.on('notification', function (data) {

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "push notification data " + JSON.stringify(data));

            switch (data.title) {
                case "Nexx Garage Alert Open":

                    simpalTek.helper.getUserPreloadData(function () {
                        simpalTek.global.isDataRefreshed = true;
                    }, function (error) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Push notify Open UserPreloadData : " + JSON.stringify(error));
                    })

                    break;
                case "Nexx Garage Alert Close":

                    simpalTek.helper.getUserPreloadData(function () {
                        simpalTek.global.isDataRefreshed = true;
                    }, function (error) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData : " + JSON.stringify(error));
                    })

                    break;

                default:

                    if (data.additionalData.activityType) {

                        switch (data.additionalData.activityType.toLowerCase()) {
                            case "open":
                            case "close":
                                simpalTek.helper.getUserPreloadData(function () {
                                    simpalTek.global.isDataRefreshed = true;
                                }, function (error) {
                                    if (data.additionalData.activityType == "Open")
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Open UserPreloadData : " + error);
                                    else (data.additionalData.activityType == "Close")
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify Close UserPreloadData : " + error);
                                });

                                break;
                            case "wakeup":
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Push notify wakeup device");
                                break;
                            case "findme":

                                break;
                        }


                    }

            }

            simpalTek.global.pushNotification.finish(function () {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "processing of push data is finished for ID = " + data.additionalData.notId);
            }, function () {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "something went wrong with push.finish for ID = " + data.additionalData.notId);
            }, data.additionalData.notId);

        });

        simpalTek.global.pushNotification.on('error', function (e) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "push notification error " + JSON.stringify(e));
        });

        if (userDb.devicesAccess.length > 0) {
            var autoOpenLocations = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() && item.ownerGrantAutoDoorOpen && item.isMain })

            //remove all existing geofence entry
            window.BackgroundGeolocation.removeGeofences(function () {

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'all geofences removed');

                if (userDb.preference) {

                    if (userDb.preference.proximityMonitorAuto) {
                        //re-add the geofence 
                        if (autoOpenLocations.length > 0) {
                            for (i = 0; i < autoOpenLocations.length; i++) {

                                deviceGeoNickName = autoOpenLocations[i].deviceId;
                                deviceGeoLoc = JSON.parse(autoOpenLocations[i].ownerDeviceGPSLoc);
                                deviceGeoLat = deviceGeoLoc.latitude;
                                deviceGeoLon = deviceGeoLoc.longitude;

                                if (autoOpenLocations[i].ownerGrantAutoDoorOpen) {
                                    simpalTek.helper.geo().add(deviceGeoNickName, deviceGeoLat, deviceGeoLon,
                                        function () {

                                        }
                                    );

                                    if (autoOpenLocations[i].userType == 'O')
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for owner - ' + deviceGeoNickName);
                                    else if (autoOpenLocations[i].userType == 'G')
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for guest - ' + deviceGeoNickName);
                                    else
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for unknown - ' + deviceGeoNickName);

                                }
                                else {
                                    if (autoOpenLocations[i].userType == 'O')
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for owner - ' + deviceGeoNickName);
                                    else if (autoOpenLocations[i].userType == 'G')
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for guest - ' + deviceGeoNickName);
                                    else
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for unknown - ' + deviceGeoNickName);
                                };

                            }
                        }


                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- startGeofences');

                        //$timeout(function () {
                        //    Geofence.start();

                        //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geo fence monitor started");
                        //}, 3000);

                    }
                }
            });

        }

    } catch (e) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'configUserMobileDeviceSetting: ' + e);
    }
}

simpalTek.helper.geoFence = function () {
    var bgGeo;
    var alreadyNotified = false;
    var locCurrentSpeed = 0;
    var currentPositionCache;
    var addOrUpdateCache;
    var lastgeoFenceTriggerDateTime = "";
    var lastgeoFenceTriggerDateTimeTrue = null;
    var lastInVehicleActivityDate = null;
    var lastActivity = "";
    var lastProximityLocationDesc = "";
    var latLogDelta;
    var secSinceGeofenceEventTriggered = 0;
    var last2LatLogDelta = [];
    var geofenceTrigger = {};
    var enterSignalTrigger = false;
    var userDb = null;
    var signalData = {};
    var userDeviceAccess = null;
    var currentNexxGarage;
    var isMovingCloser = false;
    var currentActivity = "";
    var geoLocDataStore = simpalTek.global.localStorage.get('GeoLocDataStore');
    var geoFenceExitDistance = 0;
    var aggressiveMonitoringActive = false;
    var userDevicePreferOpenDistance = 250;  //default to 250 ft
    var self = this;
    var geoFenceEnterTime = "";

    geofenceTrigger.latitude = 0;
    geofenceTrigger.longitude = 0;

    //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'BackgroundGeolocation Configured: ' + simpalTek.global.bgGeoConfigured);

    document.addEventListener('deviceready', function () {
        bgGeo = window.BackgroundGeolocation;

        if (!geoLocDataStore) {
            geoLocDataStore = {};
            geoLocDataStore.exitDistance = -1;
        }

    }, false);
    

    function configureBackgroundGeolocation() {
        //var config = Settings.getConfig();
        var config = simpalTek.global.bgGeoConfig;

        // NOTE:  Optionally generate a schedule here.  @see /www/js/tests.js
        //  1: how many schedules?
        //  2: delay (minutes) from now to start generating schedules
        //  3: schedule duration (minutes)
        //  4: time between (minutes) generated schedule ON events
        //
        // UNCOMMENT TO AUTO-GENERATE A SERIES OF SCHEDULE EVENTS BASED UPON CURRENT TIME:
        //config.schedule = Tests.generateSchedule(24, 1, 1, 1);
        //
        //config.url = 'http://192.168.11.120:8080/locations';
        config.params = {};

        // Attach Device info to BackgroundGeolocation params.device    
        config.params.device = ionic.Platform.device();
                
        bgGeo.configure(config, function (state) {
            console.info('- configure success: ', JSON.stringify(state));
            simpalTek.global.bgGeoConfigured = true;

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- configure success: ' + JSON.stringify(state));
            
            //try {
            //    //window.setTimeout(function () {
            //        self.restart();
            //    //}, 3000)
            //}
            //catch (e) {
            //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'issue starting geofence monitoring ' + JSON.stringify(e));
            //}

        }, function (error) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, '- configure failed: ' + JSON.stringify(error));
        });

        bgGeo.on('location', onLocation, onLocationError);
        bgGeo.on('motionchange', onMotionChange);
        bgGeo.on('geofence', onGeofence);
        bgGeo.on('http', onHttpSuccess, onHttpError);
        bgGeo.on('heartbeat', onHeartbeat);
        bgGeo.on('schedule', onSchedule);
        bgGeo.on('providerchange', onProviderChange);
        bgGeo.on('activitychange', onActivityChange);
        bgGeo.on('error', onError);
        bgGeo.on('geofenceschange', onGeofencesChange);
    }

    function configureBackgroundFetch() {
        //var config = Settings.getConfig();
        var config = simpalTek.global.bgGeoConfig;

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'configureBackgroundFetch');

        var Fetcher = window.BackgroundFetch;
        // Your background-fetch handler.
        var fetchCallback = function () {
            console.log('[js] BackgroundFetch initiated');
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundFetch INITIATED');

            Fetcher.finish();
        }

        var failureCallback = function () {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- BackgroundFetch failed');
        };

        Fetcher.configure(fetchCallback, failureCallback, {
            stopOnTerminate: config.stopOnTerminate
        });
    }

    //*************** Geofence events ***************************

    /**
 * BackgroundGeolocation Location callback
 * @param {Object} location
 * @param {Integer} taskId
 */
    function onLocation(location, taskId) {

        var proximityLocationDesc = "";
        var config = simpalTek.global.bgGeoConfig;

        try {

            BackgroundGeolocation.getCount(function (count) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Location count: " + count);
            });

            if (location.sample !== true) {

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'onLocation triggered ' + JSON.stringify(location));

                if (geofenceTrigger.latitude != 0) {
                    latLogDelta = simpalTek.helper.geo().distance(geofenceTrigger.latitude, geofenceTrigger.longitude, location.coords.latitude, location.coords.longitude, 'F');

                    last2LatLogDelta.push(latLogDelta);

                    if (last2LatLogDelta.length >= 3)
                        last2LatLogDelta.shift();

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'latLogDelta - ' + latLogDelta);
                    if (last2LatLogDelta.length > 1) {
                        if (last2LatLogDelta[0] > last2LatLogDelta[1])
                            isMovingCloser = true;
                        else
                            isMovingCloser = false;
                    }
                    else
                        isMovingCloser = true;

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Open Condition ' + userDevicePreferOpenDistance + "ft|enterSignalTrigger:" + enterSignalTrigger + "|currentActivity:" + currentActivity + "|isMovingCloser:" + isMovingCloser + "|location.accuracy:" + location.coords.accuracy + "|location.activity.confidence:" + location.activity.confidence);

                    //if ((latLogDelta > 100 && latLogDelta < 250) && enterSignalTrigger) {
                    //if (latLogDelta < (ionic.Platform.isAndroid() ? 150 : 250) && enterSignalTrigger && (currentActivity != 'on_foot' && currentActivity != 'still') && isMovingCloser) {
                    //if (latLogDelta < (ionic.Platform.isAndroid() ? 200 : 250) && enterSignalTrigger && geoLocDataStore.exitDistance > 400) {
                    if (latLogDelta <= userDevicePreferOpenDistance && enterSignalTrigger) {

                        location.exitDistance = geoLocDataStore.exitDistance;
                        location.latLogDelta = latLogDelta;
                        location.geoFenceEnterTime = geoFenceEnterTime;

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'GEO FENCE OPEN TRIGGER ' + JSON.stringify(location));

                        //reset the geo fence exit distance
                        geoLocDataStore.exitDistance = 0;
                        simpalTek.global.localStorage.set('GeoLocDataStore', geoLocDataStore);

                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Signal sent to open");
                        enterSignalTrigger = false;
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Disable aggressive GPS monitoring.  Engages stationary-mode.');
                        bgGeo.changePace(false); // <-- Disable aggressive GPS monitoring.  Engages stationary-mode.
                        aggressiveMonitoringActive = false;
                        geofenceTrigger.longitude = 0;

                        self.restart();

                        signalData.signalType = 'auto';

                        
                        signalData.locationData = JSON.stringify(location);
                        
                        if (userDeviceAccess) {
                            signalData.SmartGateDeviceID = userDeviceAccess[0].vendorDeviceId; //userDb.registeredDevices[0].deviceName;
                            signalData.deviceId = userDeviceAccess[0].deviceId;

                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device open ' + JSON.stringify(signalData));

                        }
                        else {
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "auto open userDeviceAccess is null");
                        }

                        //simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, "signalData " + JSON.stringify(signalData));

                        simpalTek.helper.sendDoorSignal(signalData,
                            function (response) {
                                userDb.gateActivities = response.result;
                                simpalTek.global.localStorage.set('UserDb', userDb);
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Enter - Garage Signal successfully sent " + signalData.SmartGateDeviceID);
                            },
                            function (err, status) {
                                console.log("Garage signal error", err, status)
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Garage signal error [" + JSON.stringify(err) + "]");
                            });

                        userDeviceAccess = null;
                        signalData = {};
                        geoFenceEnterTime = "";

                    }
                }

            }

            lastProximityLocationDesc = proximityLocationDesc;
        }
        catch (e) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'onLocation proximity checked failed - ' + JSON.stringify(e) + " | userDeviceAccess:" + JSON(userDeviceAccess) + "| userdbdevicesAccess: " + JSON(simpalTek.global.localStorage.get('UserDb').devicesAccess));
        }

        config = null;

    }
    /**
    * Background Geolocation error callback
    * @param {Integer} code
    */
    function onLocationError(error) {
        console.error('[js] Location error: ', error);
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(error));
    }
    /**
    * Background Geolocation HTTP callback
    */
    function onHttpSuccess(response) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(response));
    }
    /**
    * BackgroundGeolocation HTTP error
    */
    function onHttpError(error) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] Location error: ' + JSON.stringify(error));
    };
    /**
    * Background Geolocation motionchange callback
    */
    function onMotionChange(isMoving, location, taskId) {
        //console.log('[js] onMotionChange: ', isMoving, location);


        if (isMoving) {
            //console.log('Device has just started MOVING', location);
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device has just started MOVING');
        } else {
            //console.log('Device is in STATIONARY MODE', location);
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Device is in STATIONARY MODE');
        }


        bgGeo.finish(taskId);
    }

    function onError(error) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence error : " + error.type + " error: " + error.code);
    }

    function onGeofencesChange(event) {
        var on = event.on;   //<-- new geofences activiated.
        var off = event.off; //<-- geofences that were de-activated.
        var activeGeofence;

        for (var n = 0, len = on.length; n < len; n++) {
            activeGeofence = on[n];

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence activiated : " + JSON.stringify(activeGeofence))
        }


        // de-activated 
        for (var n = 0, len = off.length; n < len; n++) {
            var identifier = off[n];
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence de-activated : " + JSON.stringify(identifier))
        }
    }

    function onActivityChange(activityName) {

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] onActivityChange: ' + activityName);
        currentActivity = activityName;
    }

    function onProviderChange(provider) {
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] onProviderChange: ' + JSON.stringify(provider));
    }

    /**
    * BackgroundGeolocation heartbeat event handler
    */
    function onHeartbeat(params) {
        var shakes = params.shakes;
        var location = params.location;
        console.log('- heartbeat: ', params);
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- heartbeat: ' + JSON.stringify(params));
        /**
        * OPTIONAL.  retrieve current position during heartbeat callback
        *
        bgGeo.getCurrentPosition(function(location, taskId) {
          console.log("- location: ", location);
          bgGeo.finish(taskId);      
        });
        *
        *
        */
    }
    /**
    * BackgroundGeolocation schedule event-handler
    */
    function onSchedule(state) {
        console.info('- Schedule event: ', state.enabled, state);

        //$scope.$apply(function () {
        //    $scope.state.enabled = state.enabled;
        //    $scope.state.isMoving = false;
        //});
    }

    /**
    * BackgroundGeolocation geofence callback
    */
    function onGeofence(params, taskId) {
        console.log('- onGeofence: ', JSON.stringify(params, null, 2));

        var garageSignalData = {};
        var authService = null;
        var currentSpeed = 0;
        var identifier = "";
        var userFirstName = "";
        var debug = "";

        var geoFenceTriggerDateTime = "";
        var geoFenceLatLogDelta = 0;


        if (ionic.Platform.isAndroid())
            currentActivity = params.location.activity.type;


        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence data: " + JSON.stringify(params, null, 2));
        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'lastProximityLocationDesc ' + lastProximityLocationDesc);

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'last2LatLogDelta ' + JSON.stringify(last2LatLogDelta));

        document.addEventListener('deviceready', function () {
            currentNexxGarage = simpalTek.global.localStorage.get("currentNexxGarage");
            var autoCloseMsg = "";

            if (lastgeoFenceTriggerDateTimeTrue != null)
                secSinceGeofenceEventTriggered = (new Date() - lastgeoFenceTriggerDateTimeTrue) / 1000;
            else if (secSinceGeofenceEventTriggered == 0 && (lastProximityLocationDesc != "at_home" && lastProximityLocationDesc != "in_neighborhood")) {
                secSinceGeofenceEventTriggered = 99;
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "App restart outside home & neighborhood area");
            }

            geoFenceTriggerDateTime = new Date().toString();
            lastgeoFenceTriggerDateTimeTrue = new Date();

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Second from last Geofence Event Triggered " + secSinceGeofenceEventTriggered);

            try {
                userDb = simpalTek.global.localStorage.get('UserDb');
                authService = angular.element(document.body).injector().get('authService');
                identifier = params.identifier;

                if (userDb.preference.geofences) {
                    var userDeviceGeoFenceConfig = userDb.preference.geofences.filter(function (x) { return x.identifier == identifier });

                    if (userDeviceGeoFenceConfig.length > 0) {
                        userDevicePreferOpenDistance = userDeviceGeoFenceConfig[0].radius * 3.28084;
                    }
                }

                //garageSignalData.signalType = 'auto';

                //if (userDb.registeredDevices.length > 0)
                //    garageSignalData.SmartGateDeviceID = userDb.registeredDevices[0].deviceName;
                //else
                //    garageSignalData.SmartGateDeviceID = currentNexxGarage.vendorDeviceId; //simpalTek.global.localStorage.get("currentVendorDeviceId");

                if (userDb.devicesAccess.length > 0) {

                    if (identifier.indexOf("-monitor") > 0) {
                        identifier = identifier.split("-")[0];
                    }

                    userDeviceAccess = userDb.devicesAccess.filter(function (item) { return item.deviceId == identifier })

                    if (userDeviceAccess.length > 0) {
                        currentNexxGarage.vendorDeviceId = userDeviceAccess[0].vendorDeviceId;
                        currentNexxGarage.deviceId = userDeviceAccess[0].deviceId;

                        simpalTek.global.localStorage.set("currentNexxGarage", currentNexxGarage)
                    }
                }
                //alert(JSON.stringify(params, null, 2))
            } catch (e) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "On Geofence preset: " + e.toString())
            }

            if (params.action == "EXIT") {
                geofenceTrigger.latitude = 0;
                geofenceTrigger.longitude = 0;
                enterSignalTrigger = false;

                bgGeo.getGeofences(function (geofences) {

                    try {
                        for (var i = 0; i < geofences.length; i++) {

                            if (params.identifier == geofences[i].identifier) {
                                geoLocDataStore.exitDistance = simpalTek.helper.geo().distance(geofences[i].latitude, geofences[i].longitude, params.location.coords.latitude, params.location.coords.longitude, 'F');

                                simpalTek.global.localStorage.set('GeoLocDataStore', geoLocDataStore);

                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'geoFenceLatLogDelta Exit - ' + geoLocDataStore.exitDistance);

                            }
                        }
                    }
                    catch (e) { simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Geo Exit Error: " + e.toString()) }

                    if (params.identifier.indexOf("monitor") > 0) {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Monitor zone EXIT.');
                        self.restart();
                    }
                    else {
                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Start location tracking on EXIT.');
                        BackgroundGeolocation.start();
                    }

                    bgGeo.finish(taskId);
                });


            }
            else if (params.action == "ENTER") {

                geoFenceEnterTime = moment.utc().format('YYYY-MM-DD HH:mm:ss');

                if (lastgeoFenceTriggerDateTime == geoFenceTriggerDateTime) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence double event called.  Call terminated.");
                }

                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "lastgeoFenceTriggerDateTime " + lastgeoFenceTriggerDateTime);
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geoFenceTriggerDateTime " + geoFenceTriggerDateTime);

                //if (lastgeoFenceTriggerDateTime != geoFenceTriggerDateTime) {

                //    lastgeoFenceTriggerDateTime = geoFenceTriggerDateTime;

                bgGeo.getGeofences(function (geofences) {
                    try {

                        for (var i = 0; i < geofences.length; i++) {

                            if (params.identifier == geofences[i].identifier) {
                                geoFenceLatLogDelta = simpalTek.helper.geo().distance(geofences[i].latitude, geofences[i].longitude, params.location.coords.latitude, params.location.coords.longitude, 'F');
                                geofenceTrigger.latitude = geofences[i].latitude;
                                geofenceTrigger.longitude = geofences[i].longitude;

                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'geoFenceLatLogDelta - ' + geoFenceLatLogDelta);

                                if (params.identifier.indexOf("monitor") > 0) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Monitor zone start tracking.');
                                    BackgroundGeolocation.start();
                                }
                                else {
                                    enterSignalTrigger = true;
                                    aggressiveMonitoringActive = true;

                                    setTimeout(function () {
                                        simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Aggressive GPS monitoring immediately engaged.');
                                        BackgroundGeolocation.changePace(true);  // <-- Aggressive GPS monitoring immediately engaged.
                                    }, 1000);

                                    //stop the auto open to trigger after 2.9 minutes
                                    //BackgroundGeolocation.startBackgroundTask(function (taskId) {
                                    //    setTimeout(function () {
                                    //        if (enterSignalTrigger) {
                                    //            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "auto open stop after time period has lapse");

                                    //            geofenceTrigger.longitude = 0;
                                    //            enterSignalTrigger = false;
                                    //            aggressiveMonitoringActive = false;
                                    //            self.restart();
                                    //        }

                                    //        BackgroundGeolocation.finish(taskId);

                                    //    }, 174000)
                                    //})
                                }

                            }
                        }
                    }
                    catch (e) { simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Geo Enter Error: " + e.toString()) }

                    bgGeo.finish(taskId);

                });

                //}

            }
            else if (params.action == "DWELL") {

                bgGeo.finish(taskId);

            }

        }, false);

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "geofence bgtask finished");

    }

    //*********************  End Events ***************************


    //********************** Geofence functions ***************************
    this.start = function () {
        
        if (window.BackgroundGeolocation) {
            // Add BackgroundGeolocationService event-listeners when Platform is ready.
            ionic.Platform.ready(function () {
                    
                window.BackgroundGeolocation.destroyLocations(function () { simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "database cleared location"); });

                BackgroundGeolocation.getCount(function (count) {
                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Location count: " + count);
                });

                //BackgroundGeolocation.getState(function (state) {

                //    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'Geofence plugin enabled : ' + state.enabled);

                //    if (!state.enabled) {
                        BackgroundGeolocation.startGeofences(function () {
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- Geofence-only monitoring started');

                            BackgroundGeolocation.getGeofences(function (geofences) {
                                for (var n = 0, len = geofences.length; n < len; n++) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence: " + geofences[n].identifier + " | " + geofences[n].radius + " | " + geofences[n].latitude + " | " + geofences[n].longitude);
                                }
                            }, function (error) {
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Failed to fetch geofences from server");
                            });

                    //    })
                    //}
                });
                
            });
        }


    }

    this.stop = function () {

        if (bgGeo) {
            bgGeo.stop(function () {
                console.info('[js] BackgroundGeolocation stopped');
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation stopped');
            });
        }
    }

    this.config = function () {

        ionic.Platform.ready(function () {
            //only call the #config once
            if (simpalTek.global.bgGeoConfigured) {
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.warning, 'BackgroundGeolocation already configured.');

                return;
            }

            // Configure BackgroundGeolocation
            if (!window.BackgroundGeolocation) {
                console.warn('Could not detect BackgroundGeolocation API');
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.warning, 'Could not detect BackgroundGeolocation API');
                return;
            }

            if (window.BackgroundFetch) {
                configureBackgroundFetch();
            }

            configureBackgroundGeolocation();
                
        });
    }

    this.restart = function () {

        simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation restarted');

        if (bgGeo) {
            bgGeo.stop(function () {
                //console.info('[js] BackgroundGeolocation stopped');
                simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '[js] BackgroundGeolocation stopped');

                //window.setTimeout(function () {
                //    //self.init();
                //    self.start();
                //}, 3000)

                BackgroundGeolocation.startBackgroundTask(function (taskId) {

                    window.setTimeout(function () {
                        window.BackgroundGeolocation.destroyLocations(function () { simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "database cleared location"); });

                        BackgroundGeolocation.startGeofences(function () {
                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, '- Geofence-only monitoring started');

                            BackgroundGeolocation.getGeofences(function (geofences) {
                                for (var n = 0, len = geofences.length; n < len; n++) {
                                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, "Geofence: " + geofences[n].identifier + " | " + geofences[n].radius + " | " + geofences[n].latitude + " | " + geofences[n].longitude);
                                }
                            }, function (error) {
                                simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, "Failed to fetch geofences from server");
                            });

                            BackgroundGeolocation.finish(taskId);
                        });
                    }, 10000)
                                        
                });

            });
        }

    }

    this.updateAllLocations = function () {
        var userDb = simpalTek.global.localStorage.get('UserDb');
        var authService = angular.element(document.body).injector().get('authService');

        try {

            simpalTek.helper.log(simpalTek.Enum.errorSeverity.debug, 'Updating all geofence locations');

            if (userDb.devicesAccess.length > 0) {
                var autoOpenLocations = userDb.devicesAccess.filter(function (item) { return item.email.toLowerCase() == authService.authentication.userName.toLowerCase() && item.ownerGrantAutoDoorOpen && item.isMain })

                //remove all existing geofence entry
                window.BackgroundGeolocation.removeGeofences(function () {

                    simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, 'all geofences removed');

                    if (userDb.preference) {

                        if (userDb.preference.proximityMonitorAuto) {
                            //re-add the geofence 
                            if (autoOpenLocations.length > 0) {
                                for (i = 0; i < autoOpenLocations.length; i++) {

                                    deviceGeoNickName = autoOpenLocations[i].deviceId;
                                    deviceGeoLoc = JSON.parse(autoOpenLocations[i].ownerDeviceGPSLoc);
                                    deviceGeoLat = deviceGeoLoc.latitude;
                                    deviceGeoLon = deviceGeoLoc.longitude;

                                    if (autoOpenLocations[i].ownerGrantAutoDoorOpen) {
                                        simpalTek.helper.geo().add(deviceGeoNickName, deviceGeoLat, deviceGeoLon,
                                            function () {

                                            }
                                        );

                                        if (autoOpenLocations[i].userType == 'O')
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for owner - ' + deviceGeoNickName);
                                        else if (autoOpenLocations[i].userType == 'G')
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for guest - ' + deviceGeoNickName);
                                        else
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring added for unknown - ' + deviceGeoNickName);

                                    }
                                    else {
                                        if (autoOpenLocations[i].userType == 'O')
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for owner - ' + deviceGeoNickName);
                                        else if (autoOpenLocations[i].userType == 'G')
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for guest - ' + deviceGeoNickName);
                                        else
                                            simpalTek.helper.log(simpalTek.Enum.errorSeverity.info, ' geo monitoring owner denied access for unknown - ' + deviceGeoNickName);
                                    };

                                }
                            }

                        }
                    }
                });

            }
        } catch (e) {
            simpalTek.helper.log(simpalTek.Enum.errorSeverity.error, 'simpalTek.helper.geoFence.updateAllLocations : ' + e);
        }
    }

    return this;
}